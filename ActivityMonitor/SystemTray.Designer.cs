﻿namespace ActivityMonitor
{
    partial class SystemTray
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contextMenuSysTray = new System.Windows.Forms.MenuStrip();
            this.contextMenuStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keePassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trueCryptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuSysTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuSysTray
            // 
            this.contextMenuSysTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenuStripToolStripMenuItem});
            this.contextMenuSysTray.Location = new System.Drawing.Point(0, 0);
            this.contextMenuSysTray.Name = "contextMenuSysTray";
            this.contextMenuSysTray.Size = new System.Drawing.Size(283, 24);
            this.contextMenuSysTray.TabIndex = 0;
            // 
            // contextMenuStripToolStripMenuItem
            // 
            this.contextMenuStripToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.keePassToolStripMenuItem,
            this.trueCryptToolStripMenuItem,
            this.toolStripSeparator1,
            this.checkForUpdatesToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.contextMenuStripToolStripMenuItem.Name = "contextMenuStripToolStripMenuItem";
            this.contextMenuStripToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.contextMenuStripToolStripMenuItem.Text = "ContextMenuStrip";
            // 
            // keePassToolStripMenuItem
            // 
            this.keePassToolStripMenuItem.Name = "keePassToolStripMenuItem";
            this.keePassToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.keePassToolStripMenuItem.Text = "KeePass";
            // 
            // trueCryptToolStripMenuItem
            // 
            this.trueCryptToolStripMenuItem.Name = "trueCryptToolStripMenuItem";
            this.trueCryptToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.trueCryptToolStripMenuItem.Text = "TrueCrypt";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(168, 6);
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.checkForUpdatesToolStripMenuItem.Text = "&Check for Updates";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(168, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // SystemTray
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 86);
            this.Controls.Add(this.contextMenuSysTray);
            this.MainMenuStrip = this.contextMenuSysTray;
            this.Name = "SystemTray";
            this.Text = "SystemTray Design";
            this.contextMenuSysTray.ResumeLayout(false);
            this.contextMenuSysTray.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip contextMenuSysTray;
        private System.Windows.Forms.ToolStripMenuItem contextMenuStripToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keePassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trueCryptToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}