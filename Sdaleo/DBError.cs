﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections;

namespace Sdaleo
{
    /// <summary>
    /// Our Custom Database Error Class that DB Functions Return to External Callers
    /// </summary>
    public class DBError
    {
        /// <summary>
        /// Used internally to quickly create a custom error object for
        /// default Database Layer Errors
        /// </summary>
        /// <param name="ErrorMsg">The Error Message to Display</param>
        internal const int DATABASE_LAYER_ERROR_NUMBER = 1978; 

        /// <summary>
        /// Error number defaulted to -1
        /// </summary>
        public int nError { get; set; }

        /// <summary>
        /// Error message defaulted to ""
        /// </summary>
        public string ErrorMsg { get; set; }

        /// <summary>
        /// List of Error IDs (defaulted to null)
        /// </summary>
        public ICollection Errors { get; set; }

        #region Construction

        /// <summary>
        /// Create a defaulted Error Object
        /// </summary>
        public DBError()
        {
            nError = -1;
            ErrorMsg = String.Empty;
            Errors = null;
        }

        /// <summary>
        /// Create a custom Error Object
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="ErrorMsg"></param>
        /// <param name="Errors"></param>
        public DBError(int nError, string ErrorMsg, ICollection Errors)
        {
            this.nError = nError;
            this.ErrorMsg = ErrorMsg;
            this.Errors = Errors;
        }

        /// <summary>
        /// Initialize DBError with SDaleo Default Error Info
        /// </summary>
        public DBError(string ErrorMsg)
        {
            this.nError = DATABASE_LAYER_ERROR_NUMBER;
            this.ErrorMsg = ErrorMsg;
            this.Errors = null;
        }

        #endregion

        #region Internal Static

        /// <summary>
        /// Create a Default DatabaseLayer Sdaleo Error
        /// </summary>
        /// <param name="ErrorMsg"></param>
        /// <returns></returns>
        internal static DBError Create(string ErrorMsg)
        {
            return new DBError(DATABASE_LAYER_ERROR_NUMBER, ErrorMsg, null);
        }

        #endregion

        /// <summary>
        /// Did an error occur? nError != -1 and ErrorMsg != ""
        /// </summary>
        public bool ErrorOccured
        { 
            get 
            { 
                return (nError != -1 && !String.IsNullOrEmpty(ErrorMsg)); 
            } 
        }      
    }
}
