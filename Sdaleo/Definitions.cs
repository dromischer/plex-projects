﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo
{
    /// <summary>
    /// DB Management Systems
    /// </summary>
    public enum DBSystem
    {
        SQL_SERVER,
        SQL_CE,
        ADVANTAGE,
        CTREE,
        MYSQL,
        POSTGRES
    }

    /// <summary>
    /// Allows us to pass DBMS Independent Parameters to DB Functions
    /// </summary>
    public struct DBMSIndParameter
    {
        public string parameterName { get; set; }
        public object Value { get; set; }
    }

    /// <summary>
    /// Transaction Delegate, Used to group multiple DB Steps into a 
    /// Single Transaction
    /// </summary>
    /// <param name="prevStepRetVal">The Result of a prior TransactionStep is passed down</param>
    /// <param name="db">the DB(Connection) object that should be used in the next step</param>
    /// <returns></returns>
    public delegate DBRetVal TransactionStep(DBRetVal prevStepRetVal, DB db);
}
