﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo
{
    /// <summary>
    /// This class allows us to Secure a Password from spying eyes
    /// * Only allow specific callers to actually retrieve the password unencrypted *
    /// </summary>
    internal class SecuredPassword
    {
        private string _Password;
        internal string Password
        {
            get
            {
                return _Password;
                //if (SecureDAL.IsInternal)
                //    return _Password;
                //else
                //    return Encryption.EncryptText(_Password);
            }
            set
            {
                _Password = value;
            }
        }
    }
}
