﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo
{
    /// <summary>
    /// Useful Utilities around objects
    /// </summary>
    internal static class ObjTool
    {
        /// <summary>
        /// Returns true if passed in object is valid and is not empty
        /// </summary>
        /// <param name="oToValidate">an object to validate</param>
        /// <returns>true if valid, false otherwise</returns>
        internal static bool IsNotNullAndNotEmpty(object oToValidate)
        {
            if ((oToValidate != null) && (oToValidate.ToString() != String.Empty))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Returns true if passed in object is valid and is empty
        /// </summary>
        /// <param name="oToValidate">an object to validate</param>
        /// <returns>true if valid, false otherwise</returns>
        internal static bool IsNotNullAndIsEmpty(object oToValidate)
        {
            if ((oToValidate != null) && (oToValidate.ToString() == String.Empty))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Convert a string to an Object of Type T
        /// </summary>
        /// <typeparam name="T">Should be a System Type like string, bool, int32, double, decimal, etc...</typeparam>
        /// <param name="strToConvert">String Value to Convert</param>
        /// <returns>an converted Object of Type t, or T Default if error occured</returns>
        internal static T ConvertStringToObj<T>(string strToConvert)
        {

            // Create a Default Type T
            T retVal = default(T);

            try
            {
                #region Conversion

                if (retVal is Char)
                {
                    retVal = (T)((object)strToConvert[0]);
                }
                else if (retVal is String)
                {
                    retVal = (T)((object)strToConvert);
                }
                else if (retVal is Decimal)
                {
                    retVal = (T)((object)Decimal.Parse(strToConvert));
                }
                else if (retVal is Int32)
                {
                    retVal = (T)((object)Int32.Parse(strToConvert));
                }
                else if (retVal is Int64)
                {
                    retVal = (T)((object)Int64.Parse(strToConvert));
                }
                else if (retVal is Single)
                {
                    retVal = (T)((object)Single.Parse(strToConvert));
                }
                else if (retVal is Double)
                {
                    retVal = (T)((object)Double.Parse(strToConvert));
                }
                else if (retVal is Boolean)
                {
                    retVal = (T)((object)Boolean.Parse(strToConvert));
                }
                else if (retVal is DateTime)
                {
                    retVal = (T)((object)DateTime.Parse(strToConvert));
                }
#if NET40   
                else if (retVal is Guid)
                {
                    retVal = (T)((object)Guid.Parse(strToConvert));
                }
#endif
                else if (retVal is IntPtr)
                {
                    retVal = (T)((object)Int32.Parse(strToConvert));
                }
                else if (retVal is UInt32)
                {
                    retVal = (T)((object)UInt32.Parse(strToConvert));
                }
                else if (retVal is UInt64)
                {
                    retVal = (T)((object)UInt64.Parse(strToConvert));
                }
                else
                {
                    retVal = (T)((object)(strToConvert));
                }

                #endregion
            }
            catch (Exception) { /* ignore */ }

            // return T
            return retVal;
        }
    }
}
