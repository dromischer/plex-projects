﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Sdaleo
{
    /// <summary>
    /// Expose the most basic connection string information for various DB Types
    /// </summary>
    public interface IConnectDb
    {
        // Validity & Type        
        DBSystem DBType { get; }
        bool IsValid { get; }

        // Connection Properties
        string ConnectionString { get; }        
        string DataSource { get; }
        string User { get; }      
        
        // Exposes Interfaces, if DB Type Supports it
        bool SupportsDBMS { get; }
        IamDBMS DBMS { get; }
        bool SupportsTimeouts { get; }
        IsupportTimeouts Timeouts { get; }
    }

    /// <summary>
    /// Expose DBMS specific Instance and DB information
    /// </summary>
    public interface IamDBMS
    {        
        // DBMS have instances
        string Server { get; }
        string Instance { get; }

        // DBMS have multiple databases
        string Database { get; }
        bool IsDatabaseSet { get; }
        bool IsDatabaseSetAndNonSystem { get; }
        bool IsDatabaseSetAndNonDefault { get; }
        IConnectDb WithDatabase(string strDatabaseName);
        IConnectDb WithoutDatabase();
    }

    /// <summary>
    /// Expose DB Timeout information
    /// </summary>
    public interface IsupportTimeouts
    {
        uint CommandTimeout { get; set; }
        uint ConnectionTimeout { get; set; }
        IConnectDb WithConnectionTimeout(uint nConnectionTimeout);
        IConnectDb WithCommandTimeout(uint nCommandTimeout);
        IConnectDb WithTimeouts(uint nConnectionTimeout, uint nCommandTimeout);
    }

    /// <summary>
    /// Expose Connection Pool Clearing
    /// </summary>
    public interface IClearConnectionPool
    {
        void ClearConnectionPool(IConnectDb cn);        
    }
}
