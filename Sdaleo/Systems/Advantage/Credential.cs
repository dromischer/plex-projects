﻿#if ADVANTAGE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Advantage.Data.Provider;
using System.Net;

namespace Sdaleo.Systems.Advantage
{
    /// <summary>
    /// Handles Advantage, to be
    /// used by all our Advantage Functions
    /// </summary>
    public class AdvantageCredential : IComparable, ICloneable, IConnectDb, IClearConnectionPool
    {
        public enum ServerType 
        {
            REMOTE,
            LOCAL
        }

        #region IConnectDb

        /// <summary>
        /// Let Callers know this is a Advantage Connection Object
        /// </summary>
        public DBSystem DBType { get { return DBSystem.ADVANTAGE; } }

        /// <summary>
        /// Check to see if the Credential Object consists of valid input
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (!ValidationConsts.IsValidFileNameNPath(DataSource))
                    return false;

                if (!ValidationConsts.Generic.IsValidServerName(User))
                    return false;

                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ConnectionString { get { return _UDL.ConnectionString; } }

        /// <summary>
        /// Advantage the File Name and Path as the Datasource
        /// </summary>
        public string DataSource { get { return _UDL.DataSource; } }

        /// <summary>
        /// Advantage doesn't use User
        /// </summary>
        public string User { get { return _UDL.Username; } }

        /// <summary>
        /// Advantage Server Type
        /// </summary>
        public ServerType Type 
        { 
            get 
            {
                try
                {
                    ServerType type = (ServerType) Enum.Parse(typeof(ServerType), _UDL.ServerType);
                    return type;
                }
                catch (Exception) { /* ignore */ }
                return ServerType.LOCAL;
            } 
        }

        /// <summary>
        /// Advantage doesn't really support DBMS like behaviors
        /// </summary>
        public bool SupportsDBMS { get { return false; } }

        /// <summary>
        /// Advantage doesn't support DBMS like behaviors
        /// </summary>
        public IamDBMS DBMS { get { return null; } }

        /// <summary>
        /// Advantage * not implemented *
        /// </summary>
        public bool SupportsTimeouts { get { return false; } }

        /// <summary>
        /// Advantage * not implemented *
        /// </summary>
        public IsupportTimeouts Timeouts { get { return null; } }

        #endregion

        private UDL _UDL = null;

        #region AdvantageCredential Credential Constructors

        /// <summary>
        /// Create a Advantage Connection from an UDL Object
        /// </summary>
        /// <param name="udl"></param>
        internal AdvantageCredential(UDL udl)
        {
            if (udl != null)
                _UDL = udl;
        }

        /// <summary>
        /// Create a Advantage Connection
        /// </summary>
        public AdvantageCredential(string FileNameNPathInclUNCPath, string UserID, string Password, ServerType type)
        {
            string serverType = type.ToString().ToUpper();
            
            if (!String.IsNullOrEmpty(FileNameNPathInclUNCPath) && FileNameNPathInclUNCPath.StartsWith(@"\\"))
            {
                int n = FileNameNPathInclUNCPath.IndexOf(@"\", @"\\".Length);
                if(n != -1)
                {
                    IPAddress ipaddress;
                    string hostname = FileNameNPathInclUNCPath.Substring(@"\\".Length, n - @"\\".Length);
                    if(IPAddress.TryParse(hostname, out ipaddress))
                    {
                        serverType = ServerType.REMOTE.ToString();
                        if (!hostname.Contains(":"))
                        {
                            // Add Port Number to Ip Address * Required by Advantage * default port is 6262
                            FileNameNPathInclUNCPath = FileNameNPathInclUNCPath.Replace(hostname, hostname + ":6262");
                        }
                    }
                }               
            }                        

            _UDL = new UDL(FileNameNPathInclUNCPath, UserID, Password, serverType);
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            AdvantageCredential credential = new AdvantageCredential((UDL)this._UDL.Clone());
            return credential;
        }

        #endregion

        #region IComparable Members

        public int CompareTo(object obj)
        {
            AdvantageCredential otherCredential = obj as AdvantageCredential;
            if (otherCredential != null)
            {
                int nCompare = _UDL.CompareTo(otherCredential._UDL);
                return nCompare;
            }
            else
            {
                throw new ArgumentException("Object is not a AdvantageCredential");
            }
        }

        #endregion

        #region IClearConnectionPool Members

        public void ClearConnectionPool(IConnectDb credential)
        {
            AdsConnection.FlushConnectionPool(credential.ConnectionString);            
        }

        #endregion
    }
}
#endif