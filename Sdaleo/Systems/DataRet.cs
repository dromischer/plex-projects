﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems
{
    /// <summary>
    /// To Deal with Data Return Type validaton / retrieval
    /// </summary>
    public static class DataRet
    {
        /// <summary>
        /// Quickly retrieve a value for a given type
        /// Will use default value of the type if object is null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oToRetrieve"></param>
        /// <returns></returns>
        public static T Retrieve<T>(object oToRetrieve)
        {
            // Create a Default Type T
            T retVal = default(T);
            if (oToRetrieve != null)
                return ObjTool.ConvertStringToObj<T>(oToRetrieve.ToString());
            
            return retVal;
        }

        /// <summary>
        /// Quickly retrieve a value for a given type
        /// Will use default value passed in, if object is null
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oToRetrieve"></param>
        /// <returns></returns>
        public static T Retrieve<T>(object oToRetrieve, T defaultValue)
        {
            // Create a Default Type T
            T retVal = default(T);
            if (ObjTool.IsNotNullAndNotEmpty(oToRetrieve))
                return ObjTool.ConvertStringToObj<T>(oToRetrieve.ToString());
            else
                retVal = defaultValue;
            return retVal;
        }

        /// <summary>
        /// Quickly retrieve a string value
        /// Will use default value passed in, if string is null
        /// </summary>        
        /// <param name="oToRetrieve"></param>
        /// <param name="bTrim">set to true to Trim() the string</param>
        /// <returns></returns>
        public static string Retrieve(object oToRetrieve, string defaultValue, bool bTrim)
        {
            string retVal = String.Empty;
            if (ObjTool.IsNotNullAndNotEmpty(oToRetrieve))
            {
                retVal = oToRetrieve.ToString();
                if (bTrim)
                    retVal = retVal.Trim();
                return retVal;
            }
            else
            {
                retVal = defaultValue;
            }
            return retVal;
        }

        /// <summary>
        /// Quickly retrieve a string value
        /// Will use "", if string is null.
        /// Will automatically call Trim().
        /// </summary>
        /// <returns></returns>
        public static string Retrieve(object oToRetrieve)
        {
            return Retrieve(oToRetrieve, String.Empty, true);
        }
    }
}
