﻿#if POSTGRES
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems.Postgres.Internal
{
    /// <summary>
    /// Central Location for Postgres Input Validation
    /// </summary>
    internal class Validation
    {
        #region Private Character Validation Declaration

        private const string CharSet_AllowedCharsAlphaNum = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private const string CharSet_AllowedCharsServerName = CharSet_AllowedCharsAlphaNum + ":\\ _!@#$%^&()-+{}[],.;`~";
        private const string CharSet_AllowedCharsInstanceName = CharSet_AllowedCharsAlphaNum + ":\\ _!@#$%^&()-+{}[],.;`~";
        private const string CharSet_AllowedCharsPassword = CharSet_AllowedCharsAlphaNum + "_!@#$%^&()-+{}[],.;`~";
        private const string CharSet_AllowedCharsTableName = CharSet_AllowedCharsAlphaNum + " -_";

        /// <summary>
        /// Generic Function to use with Allowed Character Sets above 
        /// </summary>
        /// <param name="TextToEvaluate">string to evaluate</param>
        /// <param name="TextToEvaluateWith">Pass in one of the legal character consts declared above</param>
        /// <returns>true if valid, false otherwise</returns>
        private static bool ContainsOnlyLegalChars(string TextToEvaluate, string TextToEvaluateWith)
        {
            foreach (char c in TextToEvaluate.ToCharArray())
            {
                bool bFound = (TextToEvaluateWith.IndexOf(c) >= 0);
                if (!bFound)
                    return false;
            }
            return true;
        }

        #endregion

        #region Name Validators

        /// <summary>
        /// Main Validation Function to validate Server Names
        /// </summary>
        /// <param name="strServerName">Name of a Server</param>
        /// <returns>true if valid, false otherise</returns>
        internal static bool IsValidServerName(string strServerName)
        {
            if (String.IsNullOrEmpty(strServerName))
            {
                return false;
            }
            else if (strServerName.Length < 1 || strServerName.Length > 248) // limit server Name length
            {
                return false;
            }
            else if (!ContainsOnlyLegalChars(strServerName, CharSet_AllowedCharsServerName))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Main Validation Function to validate Instance Names
        /// </summary>
        /// <param name="strInstanceName">Name of an Instance</param>
        /// <returns>true if valid, false otherise</returns>
        internal static bool IsValidInstanceName(string strInstanceName)
        {
            if (string.IsNullOrEmpty(strInstanceName))
            {
                return false;
            }
            else if (strInstanceName.Length < 1 || strInstanceName.Length > 18) // limit Instance Name length
            {
                return false;
            }
            else if (!ContainsOnlyLegalChars(strInstanceName, CharSet_AllowedCharsInstanceName))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Main Validation Function to validate Passwords
        /// </summary>
        /// <param name="strPassword">Password</param>
        /// <returns>true if valid, false otherise</returns>
        internal static bool IsValidPassword(string strPassword)
        {
            if (String.IsNullOrEmpty(strPassword))
            {
                return false;
            }
            else if (strPassword.Length < 1 || strPassword.Length > 40) // limit Password Length
            {
                return false;
            }
            else if (!ContainsOnlyLegalChars(strPassword, CharSet_AllowedCharsPassword))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Main Validation Function to validate TableName
        /// </summary>
        /// <param name="strTableName">Name of Table</param>
        /// <returns>true if valid, false otherwise</returns>
        internal static bool IsValidTableName(string strTableName)
        {
            if (string.IsNullOrEmpty(strTableName))
            {
                return false;
            }
            else if (strTableName.Length < 1 || strTableName.Length > 52) // limit Table Name length
            {
                return false;
            }
            else if (!ContainsOnlyLegalChars(strTableName, CharSet_AllowedCharsTableName))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Combination Validators

        /// <summary>
        /// Validates that the passedin string has both a valid server and a valid Instance
        /// </summary>
        /// <param name="strServerNameNInstance">a string to check for server and instance</param>
        /// <returns>true if valid, false otherwise</returns>
        internal static bool IsValidServerNameAndInstanceName(string strServerNameNInstance)
        {
            if (!string.IsNullOrEmpty(strServerNameNInstance) && (strServerNameNInstance.IndexOf('\\') >= 0))
            {

                int nIndexLast = strServerNameNInstance.LastIndexOf('\\');
                string[] values = new String[] { strServerNameNInstance.Substring(0, nIndexLast), strServerNameNInstance.Substring(nIndexLast + 1) };
                if (values.Length == 2)
                    return IsValidServerName(values[0]) && IsValidInstanceName(values[1]);
            }
            return false;
        }

        #endregion
    }
}
#endif