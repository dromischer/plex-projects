﻿#if SQLSERVER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems.SQLServer
{
    /// <summary>
    /// Central Location for SQL Server Input Validation
    /// </summary>
    internal class Validation
    {      
        /// <summary>
        /// Main Validation Function to validate DatabaseName
        /// </summary>
        /// <param name="strDatabaseName">Name of Database</param>
        /// <returns>true if valid, false otherise</returns>
        internal static DBError IsValidDatabaseName(string strDatabaseName)
        {
            if (string.IsNullOrEmpty(strDatabaseName))
            {
                return DBError.Create("DatabaseName is null");
            }
            else if (strDatabaseName.Length < 1 || strDatabaseName.Length > 52) // limit database Name length
            {
                return DBError.Create("DatabaseName length is invalid");
            }            
            else if (!ValidationConsts.ContainsOnlyLegalChars(strDatabaseName, ValidationConsts.CharSet_AllowedDatabaseNames))
            {
                return DBError.Create("DatabaseName contains illegal characters");
            }
            else
            {
                return new DBError();
            }
        }

        /// <summary>
        /// validate SQL Server Table Names
        /// </summary>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        public static DBError IsValidTableName(string strTableName)
        {
            if (string.IsNullOrEmpty(strTableName))
            {
                return DBError.Create("TableName is null");
            }
            else if (strTableName.Length < 1 || strTableName.Length > 52) // limit Table Name length
            {
                return DBError.Create("TableName length is invalid");
            }
            else if (!ValidationConsts.ContainsOnlyLegalChars(strTableName, ValidationConsts.CharSet_AllowedTableNames))
            {
                return DBError.Create("TableName contains illegal characters");
            }
            else
            {
                return new DBError();
            }
        }

        /// <summary>
        /// Validates a DataSource String which can include a Server and Instance, or just be a Server
        /// </summary>
        /// <param name="strDataSource">a DataSource String</param>
        /// <returns>true if valid, false otherise</returns>
        internal static bool IsValidDataSource(string strDataSource)
        {
            if (!string.IsNullOrEmpty(strDataSource) && (strDataSource.IndexOf('\\') >= 0))
            {
                string[] values = strDataSource.Split('\\');
                if (values.Length == 2)
                    return ValidationConsts.Generic.IsValidServerName(values[0]) && ValidationConsts.Generic.IsValidInstanceName(values[1]);
            }
            else if (!string.IsNullOrEmpty(strDataSource) && (strDataSource.IndexOf('\\') == -1))
            {
                return ValidationConsts.Generic.IsValidServerName(strDataSource);
            }
            return false;            
        }

        /// <summary>
        /// Validates that the passedin string has both a valid server and a valid Instance
        /// </summary>
        /// <param name="strServerNameNInstance">a string to check for server and instance</param>
        /// <returns>true if valid, false otherwise</returns>
        internal static bool IsValidServerNameAndInstanceName(string strServerNameNInstance)
        {
            if (!string.IsNullOrEmpty(strServerNameNInstance) && (strServerNameNInstance.IndexOf('\\') >= 0))
            {
                string[] values = strServerNameNInstance.Split('\\');
                if (values.Length == 2)
                    return ValidationConsts.Generic.IsValidServerName(values[0]) && ValidationConsts.Generic.IsValidInstanceName(values[1]);
            }
            return false;
        }

    }
}
#endif