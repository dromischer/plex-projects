﻿#if SQLSERVER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems.SQLServer
{
    /// <summary>
    /// Database Error Constants to Use to handle specific Error
    /// </summary>
    internal static class ErrorConst
    {
        public const int ERROR_DB_ALREADY_EXISTS = 1801;
        public const int ERROR_DB_SOME_FILE_NAMES_COULD_NOT_BE_CREATED = 1802;
        public const int ERROR_DB_CAN_NOT_CREATE_FILE_BECAUSE_IT_ALREADY_EXISTS = 5170;
        public const int ERROR_DB_THERE_ALREADY_IS_AN_OBJECT_IN_THE_DB_WITH_THIS_NAME = 3092;
        public const int ERROR_DB_DEVICE_ACTIVATION_FAILED = 5105;
    }
}
#endif