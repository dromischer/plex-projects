﻿#if SQLSERVER
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems.SQLServer
{
    #region SQLServer Utility Enums

    /// <summary>
    /// Default Databases that are usually part of an SQL Server
    /// </summary>
    internal enum SQLServerDefaultDatabase
    {
        master,
        model,
        msdb,
        tempdb,
        Northwind,
        pubs
    }

    #endregion

    /// <summary>
    /// SQLServer Helper Utilites
    /// </summary>
    public class SQLServerUtilities
    {
        /// <summary>
        /// Splits a DataSource String to it's ServerAddress and InstanceName components
        /// </summary>
        /// <param name="ServerOrServerNInstance">a DataSource string containing either Server or Server And Instance</param>
        /// <param name="Server">Returns ServerAddress, if found</param>
        /// <param name="Instance">Returns Instance name, if found</param>
        /// <returns>True, if successfull, false otherwise</returns>
        public static bool SplitServerOrServerNInstance(string ServerOrServerNInstance, out string Server, out string Instance)
        {
            Server = String.Empty;
            Instance = String.Empty;

            if (!string.IsNullOrEmpty(ServerOrServerNInstance) && (ServerOrServerNInstance.IndexOf('\\') >= 0))
            {
                string[] values = ServerOrServerNInstance.Split('\\');
                if (values.Length == 2)
                {
                    if(ValidationConsts.Generic.IsValidServerName(values[0]))
                       Server = values[0];
                    if(ValidationConsts.Generic.IsValidInstanceName(values[1]))
                       Instance = values[1];
                    return (!String.IsNullOrEmpty(Server) && !String.IsNullOrEmpty(Instance));
                }
                else
                {
                    return false;
                }
            }
            else if (!string.IsNullOrEmpty(ServerOrServerNInstance) && (ServerOrServerNInstance.IndexOf('\\') == 0))
            {
                if (ValidationConsts.Generic.IsValidServerName(ServerOrServerNInstance))
                    Server = ServerOrServerNInstance;
                return (!String.IsNullOrEmpty(Server));
            }
            return false;
        }

        // Easy Access to SQL Master Database
        public static string SQLServerMasterDatabaseName { get { return SQLServerDefaultDatabase.master.ToString(); } }
        public static bool IsSQLServerMasterDatabaseName(string strDatabaseName) { return (SQLServerMasterDatabaseName.ToUpper() == strDatabaseName.ToUpper()); }

        /// <summary>
        ///  Easy Check for Default Database * Any SQL Server Default Database *
        /// </summary>
        /// <param name="strDatabaseName">Name of Database to check for</param>
        /// <returns>true if this is an SQL Server Default Database, false otherwise</returns>
        public static bool IsDefaultDatabaseName(string strDatabaseName)
        {
            foreach (string strName in Enum.GetNames(typeof(SQLServerDefaultDatabase)))
            {
                if (strName.ToUpper() == strDatabaseName.ToUpper())
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Easy Check for System Database * Same as Default Database but does not inclue Northwind, or Pubs *
        /// </summary>
        /// <param name="strDatabaseName">Name of Database to check for</param>
        /// <returns>true if this is an SQL Server System Database, false otherwise</returns>
        public static bool IsSystemDatabaseName(string strDatabaseName)
        {
            foreach (string strName in Enum.GetNames(typeof(SQLServerDefaultDatabase)))
            {
                if (strName.ToUpper() == strDatabaseName.ToUpper() &&
                    (strDatabaseName.ToUpper() != "NORTHWIND") &&
                    (strDatabaseName.ToUpper() != "PUBS"))
                    return true;
            }
            return false;
        }  
    }
}
#endif