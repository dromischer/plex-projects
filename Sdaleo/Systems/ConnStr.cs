﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems
{
    public static class ConnStr
    {
        /// <summary>
        /// Quick Check to see if a connection string is valid * must have DataSource Key and 
        /// a Value * in order to be considered a valid Connection String
        /// </summary>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public static bool IsConnectionString(string ConnectionString)
        {
            if (!String.IsNullOrEmpty(ConnectionString) && 
                ConnectionString.ToUpper().Contains("DATA SOURCE") &&
                ConnectionString.Contains("="))
            {
                String[] tokens = ConnectionString.Split(';');
                foreach (string Pair in tokens)
                {
                    string[] KeyValuePair = Pair.Split('=');
                    try
                    {
                        string left = KeyValuePair[0];
                        string right = KeyValuePair[1];
                        if (String.Compare(KeyValuePair[0].ToUpper().Trim(), "DATA SOURCE", true) == 0)
                            return !String.IsNullOrEmpty(right);
                    }
                    catch (Exception) { /* ignore and continue iteration */ }
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// Check to see if a Connection String Contains the specified Key
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public static bool ContainsKey(string Key, string ConnectionString)
        {
            if (!String.IsNullOrEmpty(Key) && !String.IsNullOrEmpty(ConnectionString))
            {
                String[] tokens = ConnectionString.Split(';');
                foreach (string Pair in tokens)
                {
                    string[] KeyValuePair = Pair.Split('=');
                    try
                    {
                        string left = KeyValuePair[0];
                        string right = KeyValuePair[1];
                        if (String.Compare(KeyValuePair[0].ToUpper().Trim(), Key.ToUpper().Trim(), true) == 0)
                            return true;
                    }
                    catch (Exception) { /* ignore and continue iteration */ }
                }
            }
            return false;
        }

        /// <summary>
        /// Retrieve the value of a Key for the specified Connection String
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public static string RetrieveValue(string Key, string ConnectionString)
        {
            if (!String.IsNullOrEmpty(Key) && !String.IsNullOrEmpty(ConnectionString))
            {
                String[] tokens = ConnectionString.Split(';');
                foreach (string Pair in tokens)
                {
                    string[] KeyValuePair = Pair.Split('=');
                    try
                    {
                        string left = KeyValuePair[0];
                        string right = KeyValuePair[1];
                        string FoundValue = "";
                        if(String.Compare(KeyValuePair[0].ToUpper().Trim(), Key.ToUpper().Trim(), true) == 0)
                        {
                            if (!String.IsNullOrEmpty(right))
                            {
                                FoundValue = right.Trim();
                                return FoundValue;
                            }
                        }
                        
                    }
                    catch (Exception) { /* ignore and continue iteration */ }
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// Tries to retrieve the DataSource Value from a Connection string.
        /// If it can it will return that. If it can't it will return the originial
        /// ConnectionString passed in Trimmed().
        /// </summary>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public static string GetDataSource(string ConnectionString)
        {
            if (IsConnectionString(ConnectionString))
            {
                String[] tokens = ConnectionString.Split(';');
                foreach (string Pair in tokens)
                {
                    string[] KeyValuePair = Pair.Split('=');
                    try
                    {
                        string left = KeyValuePair[0];
                        string right = KeyValuePair[1];
                        string FoundValue = "";
                        if (String.Compare(KeyValuePair[0].ToUpper().Trim(), "DATA SOURCE", true) == 0)
                        {
                            if (!String.IsNullOrEmpty(right))
                            {
                                FoundValue = right.Trim();
                                return FoundValue;
                            }
                        }

                    }
                    catch (Exception) { /* ignore and continue iteration */ }
                }
            }

            // Migth still be a data source just not from a Connection string
            // so just return the value trimmed
            if(!String.IsNullOrEmpty(ConnectionString))
              return ConnectionString.Trim();
            else
              return String.Empty;
        }
    }
}
