﻿#if SQLCOMPACT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems.SQLCE
{
    /// <summary>
    /// General SQL CE Functions
    /// </summary>
    public class SQLCEGeneral
    {
        /// <summary>
        /// Quick Check function to see if the passed in credential allows us to connect to the database,
        /// It also verifies that there are any Tables in the Database
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>        
        /// <param name="CanConnect">Returns true if successfull in connecting to the database</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError SQLCECheckConnection(IConnectDb credential, out bool CanConnect)
        {
            CanConnect = false;
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            // First Verify that the Database Exists
            bool bExists = false;
            dbError = SQLCEDatabase.DatabaseExists(credential, out bExists);
            if (!bExists)
                return dbError;

            ////
            // TODO: Make sure that we can read any tables from the db
            ////

            return null;
        }

    }
}
#endif
