﻿#if SQLCOMPACT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sdaleo.Systems.SQLCE
{
    /// <summary>
    /// Handles SQL CE Credentials, to be
    /// used by all our SQL CE Functions
    /// </summary>
    public class SQLCECredential : IComparable, ICloneable, IConnectDb
    {                
        #region IConnectDb

        /// <summary>
        /// Let Callers know this is a SQLCE Connection Object
        /// </summary>
        public DBSystem DBType { get { return DBSystem.SQL_CE; } }

        /// <summary>
        /// Check to see if the Credential Object consists of valid input
        /// </summary>
        public bool IsValid
        {
            get
            {                
                if (!ValidationConsts.IsValidFileNameNPath(DataSource))
                    return false;

                if (!String.IsNullOrEmpty(_UDL.Password) && !ValidationConsts.Generic.IsValidPassword(_UDL.Password))
                    return false;

                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ConnectionString  {  get { return _UDL.ConnectionString; } }        

        /// <summary>
        /// SQL CE uses the File Name and Path as the Datasource
        /// </summary>
        public string DataSource { get { return _UDL.DataSource; } }
        
        /// <summary>
        /// SQL CE doesn't use User
        /// </summary>
        public string User { get { return _UDL.Username; } }

        /// <summary>
        /// SQL CE doesn't support DBMS like behaviors
        /// </summary>
        public bool SupportsDBMS { get { return false; } }

        /// <summary>
        /// SQL CE doesn't support DBMS like behaviors
        /// </summary>
        public IamDBMS DBMS { get { return null; } }
        
        /// <summary>
        /// SQL CE doesn't support Timeouts
        /// </summary>
        public bool SupportsTimeouts { get { return false; } }
        
        /// <summary>
        /// SQL CE doesn't support Timeouts
        /// </summary>
        public IsupportTimeouts Timeouts { get { return null; } }

        #endregion

        private UDL _UDL = null;

        #region SQLCE Credential Constructors

        /// <summary>
        /// Create a SQL CE Connection from an UDL Object
        /// </summary>
        /// <param name="udl"></param>
        internal SQLCECredential(UDL udl)
        {
            if (udl != null)
                _UDL = udl;
        }

        /// <summary>
        /// Create an Untrusted SQLCE Credential * No Encryption Used *
        /// </summary>
        public SQLCECredential(string FileNameNPath)
        {
            _UDL = new UDL(FileNameNPath, String.Empty);
        }

        /// <summary>
        /// Create a Trusted SQLCE Credential * Encryption Used *
        /// </summary>
        public SQLCECredential(string FileNameNPath, string strPassword)
        {
            _UDL = new UDL(FileNameNPath, strPassword);
        }
       
        #endregion

        #region ICloneable Members

        public object Clone()
        {
            SQLCECredential credential = new SQLCECredential((UDL)this._UDL.Clone());
            return credential;
        }

        #endregion

        #region IComparable Members

        public int CompareTo(object obj)
        {
            SQLCECredential otherCredential = obj as SQLCECredential;
            if (otherCredential != null)
            {
                int nCompare = _UDL.CompareTo(otherCredential._UDL);
                return nCompare;
            }
            else
            {
                throw new ArgumentException("Object is not a SQLCECredential");
            }
        }

        #endregion
    }
}
#endif