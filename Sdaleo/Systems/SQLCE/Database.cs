﻿#if SQLCOMPACT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlServerCe;

namespace Sdaleo.Systems.SQLCE
{
    /// <summary>
    /// Specific actions for SQL CE Databases
    /// </summary>
    public class SQLCEDatabase
    {
        #region Database Create / Exists

        /// <summary>
        /// Checks to see if we can query the specified server for the Database (Existence check)
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>
        /// <param name="bExists">Returns true if the Database Exists, false otherwise</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseExists(IConnectDb credential, out bool bExists)
        {
            bExists = false;
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;
                        
            // Check for File Existence
            if (File.Exists(credential.DataSource))
            {
                bExists = true;
                return dbError;
            }
            else
                return DBError.Create("Database File does not exist"); 
        }

        /// <summary>
        /// Create a new Database with the specified Credentials
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>                
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseCreate(IConnectDb credential)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            //if (dbError.ErrorOccured)
            //    return dbError;
        
            //// First Create the Directory if it doesn't exists
            //string path = Path.GetDirectoryName(credential.DataSource);
            //if (!Directory.Exists(path))
            //    Directory.CreateDirectory(path);

            //// Now try to create the database if it doesn't exists
            //bool bExists = false;
            //dbError = DatabaseExists(credential, out bExists);
            //if (!bExists)
            //{
            //    try
            //    {
            //        using (SqlCeEngine engine = new SqlCeEngine(credential.ConnectionString))
            //        {
            //            engine.CreateDatabase();
            //        }
            //    }
            //    catch (SqlCeException e)
            //    {
            //        dbError = DBMS.CreateErrorDBRetVal(DBSystem.SQL_CE, e);
            //    }
            //}            
            return dbError;
        }

        #endregion

        #region Database Copy / Delete

        /// <summary>
        /// Copies a specified SQL CE Database to a new specified Database
        /// </summary>
        /// <param name="SourceCredential">SQL CE Credentials of Source Database</param>
        /// <param name="DestinationCredential">SQLCE Credential of Destination Database To be copied To</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseCopy(IConnectDb SourceCredential, IConnectDb DestinationCredential)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(SourceCredential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            dbError = ValidationConsts.IsCredentialValid(DestinationCredential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            // Make sure the Source Database exists
            bool bExists = false;
            dbError = DatabaseExists(SourceCredential, out bExists);
            if (!bExists)
                return dbError;

            // Make sure the Destination Database does NOT Exist
            bExists = false;
            dbError = DatabaseExists(DestinationCredential, out bExists);
            if (bExists)
                return DBError.Create("Destination Credential Database already exists");

            try
            {
                File.Copy(SourceCredential.DataSource, DestinationCredential.DataSource);
            }
            catch (Exception ex)
            {
                dbError = DBError.Create(ex.Message);
            }
            return dbError;
        }

        /// <summary>
        /// Delete a specified SQLCE Database
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseDelete(IConnectDb credential)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            // Make sure the Source Database exists
            bool bExists = false;
            dbError = DatabaseExists(credential, out bExists);
            if (!bExists)
                return dbError;

            try
            {
                File.Delete(credential.DataSource);
            }
            catch (Exception ex)
            {
                dbError = DBError.Create(ex.Message);
            }
            return dbError;
        }

        #endregion

        #region Database Integrity

        /// <summary>
        /// Shrinks the specified Database to reclaim wasted space
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>                
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseShrink(IConnectDb credential)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            //if (dbError.ErrorOccured)
            //    return dbError;

            //// Now try to shrink the database if it exists
            //bool bExists = false;
            //dbError = DatabaseExists(credential, out bExists);
            //if (bExists)
            //{
            //    try
            //    {
            //        using (SqlCeEngine engine = new SqlCeEngine(credential.ConnectionString))
            //        {
            //            engine.Shrink();
            //        }
            //    }
            //    catch (SqlCeException e)
            //    {
            //        dbError = DBMS.CreateErrorDBRetVal(DBSystem.SQL_CE, e);
            //    }
            //}
            return dbError;
        }

        /// <summary>
        /// Verifies the specified Database in case it is corrupted
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>                
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseVerify(IConnectDb credential)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            //if (dbError.ErrorOccured)
            //    return dbError;

            //// Now try to repair the database if it exists
            //bool bExists = false;
            //dbError = DatabaseExists(credential, out bExists);
            //if (bExists)
            //{
            //    try
            //    {
            //        using (SqlCeEngine engine = new SqlCeEngine(credential.ConnectionString))
            //        {
            //            engine.Verify();
            //        }
            //    }
            //    catch (SqlCeException e)
            //    {
            //        dbError = DBMS.CreateErrorDBRetVal(DBSystem.SQL_CE, e);
            //    }
            //}
            return dbError;
        }

        /// <summary>
        /// Repairs the specified Database in case it is corrupted
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>                
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError DatabaseRepair(IConnectDb credential)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            //if (dbError.ErrorOccured)
            //    return dbError;

            //// Now try to repair the database if it exists
            //bool bExists = false;
            //dbError = DatabaseExists(credential, out bExists);
            //if (bExists)
            //{
            //    try
            //    {
            //        using (SqlCeEngine engine = new SqlCeEngine(credential.ConnectionString))
            //        {
            //            // Specify null destination connection string for in-place repair
            //            engine.Repair(null, RepairOption.RecoverAllPossibleRows);
            //        }
            //    }
            //    catch (SqlCeException e)
            //    {
            //        dbError = DBMS.CreateErrorDBRetVal(DBSystem.SQL_CE, e);
            //    }
            //}
            return dbError;
        }

        #endregion
    }
}
#endif
