﻿#if SQLCOMPACT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Sdaleo.Systems.SQLCE
{
    /// <summary>
    /// Specific actions for SQL CE Tables in a Database
    /// </summary>
    public class SQLCETable
    {
        /// <summary>
        /// Returns all the Table Names for the specified SQL CE Credential
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>
        /// <param name="Tables">array of table names, or null if error occured</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError Tables(IConnectDb credential, out string[] Tables)
        {
            Tables = null;
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            // Execute the Query
            string sql = "Select [TABLE_NAME] From INFORMATION_SCHEMA.TABLES Order By [TABLE_NAME]";
            DB db = DB.Create(credential);
            DBRetVal retVal = db.FillDataTable(sql);
            if (retVal.IsValid)
            {
                List<String> retList = new List<string>();
                foreach (DataRow row in retVal.GetDataTableRetVal().Rows)
                         retList.Add(row["TABLE_NAME"].ToString());
                Tables = retList.ToArray();
            }
            return retVal;
        }

        /// <summary>
        /// Check to see if the Table Exists on the specified SQL CE Credentials
        /// </summary>
        /// <param name="credential">SQL CE Credentials</param>
        /// <param name="TableName">Specify the TableName to check for (required)</param>
        /// <param name="bExists">true if exists, false otherwise</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError TableExists(IConnectDb credential, string TableName, out bool bExists)
        {
            bExists = false;
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            dbError = Validation.IsValidTableName(TableName);
            if (dbError.ErrorOccured)
                return dbError;

            // Execute the Query
            string sql = String.Format("Select [TABLE_NAME] From INFORMATION_SCHEMA.TABLES Where [TABLE_NAME] = '{0}' Order By [TABLE_NAME]", TableName);
            DB db = DB.Create(credential);
            DBRetVal retVal = db.ExecuteScalar(sql);
            bExists = !retVal.IsValid;
            return retVal;
        }

        /// <summary>
        /// Changes the Table Name for the specified Table
        /// </summary>
        /// <param name="credential">SSQL CE Credentials</param>
        /// <param name="TableName">Name of Table (required)</param>
        /// <param name="TableNameNew">Name of Table New (required)</param>
        /// <returns>DBError Object with ErrorOccured, if error Occured</returns>
        public static DBError TableRename(IConnectDb credential, string TableName, string TableNameNew)
        {
            DBError dbError = ValidationConsts.IsCredentialValid(credential, DBSystem.SQL_CE);
            if (dbError.ErrorOccured)
                return dbError;

            dbError = Validation.IsValidTableName(TableName);
            if (dbError.ErrorOccured)
                return dbError;

            dbError = Validation.IsValidTableName(TableNameNew);
            if (dbError.ErrorOccured)
                return dbError;

            // Execute the Query
            string sql = String.Format("sp_rename @objname = '{0}', @newname = '{1}', @objtype = 'OBJECT'", TableName, TableNameNew);
            DB db = DB.Create(credential);
            DBRetVal retVal = db.ExecuteNonQuery(sql);
            return retVal;
        }
    }
}
#endif