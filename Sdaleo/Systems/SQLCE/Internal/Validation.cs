﻿#if SQLCOMPACT
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sdaleo.Systems;

namespace Sdaleo.Systems.SQLCE
{
    /// <summary>
    /// SQL CE Custom Validation
    /// </summary>
    internal static class Validation
    {
        /// <summary>
        /// validate SQL CE Table Names
        /// </summary>
        /// <param name="strTableName"></param>
        /// <returns></returns>
        public static DBError IsValidTableName(string strTableName)
        {
            if (string.IsNullOrEmpty(strTableName))
            {
                return DBError.Create("TableName is null");
            }
            else if (strTableName.Length < 1 || strTableName.Length > 52) // limit Table Name length
            {
                return DBError.Create("TableName length is invalid");
            }
            else if (!ValidationConsts.ContainsOnlyLegalChars(strTableName, ValidationConsts.CharSet_AllowedTableNames))
            {
                return DBError.Create("TableName contains illegal characters");
            }
            else
            {
                return new DBError();
            }
        }
    }
}
#endif
