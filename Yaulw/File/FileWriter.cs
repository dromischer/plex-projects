﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Yaulw.File
{
    /// <remarks>
    /// Simple Line-By-Line UTF/Ascii File Writer Object
    /// </remarks>
    public class FileWriter
    {
        #region Private Members

        private string _FileName = "";
        private string _FileType = "";
        private string _dirPath = "";
        private bool _OverideExisting = false;
        private bool _FileIsCreated = false;

        #endregion

        #region Construction

        /// <summary>
        /// Create a Simple UTF/Ascii Line FileWriter Object
        /// </summary>
        /// <param name="FileName">The Name of the File (If blank will generate a random file name)</param>
        /// <param name="FileType">The Type of File to write to (Default = "log")</param>
        /// <param name="dirPath">The path where to write the file (If Blank will use Temp Path)</param>
        /// <param name="OverideExisting">true to overide an existing file, false will try to append by default</param>
        public FileWriter(string FileName = "", string FileType = "log", string dirPath = "", bool OverideExisting = true)
        {      
            _FileName = FileName;
            _FileType = FileType;
            _dirPath = dirPath;
            _OverideExisting = OverideExisting;

            // Generate File Name and Path, if not exist
            if(String.IsNullOrEmpty(_FileName))
                _FileName = System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetRandomFileName());
            if(String.IsNullOrEmpty(_dirPath))
                _dirPath = System.IO.Path.GetTempPath();

            // Make Sure Path Exists
            if (!Directory.Exists(_dirPath))
                 Directory.CreateDirectory(_dirPath);           
        }

        #endregion

        #region Write Ascii

        /// <summary>
        /// Write a Line in Ascii to File
        /// </summary>
        /// <param name="line">ascii line to write</param>
        public void WriteLineA(string line)
        {
            using (FileStream fs = CreateFileStream())
            using(StreamWriter sw = new StreamWriter(fs, Encoding.ASCII))
            {                
                sw.WriteLine(line);
                sw.Flush();
            }           
        }

        /// <summary>
        /// Write Lines in Ascii to file
        /// </summary>
        /// <param name="lines">ascii lines to write</param>
        public void WriteLineA(string[] lines)
        {
            using (FileStream fs = CreateFileStream())
            using (StreamWriter sw = new StreamWriter(fs, Encoding.ASCII))
            {
                foreach (string line in lines)
                         sw.WriteLine(line);
                sw.Flush();
            }
        }

        #endregion

        #region Write UTF

        /// <summary>
        /// Write a Line in UTF to File
        /// </summary>
        /// <param name="line">utf line to write</param>
        public void WriteLineUTF8(string line)
        {
            using (FileStream fs = CreateFileStream())
            using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
            {
                sw.WriteLine(line);
                sw.Flush();
            } 
        }

        /// <summary>
        /// Write Lines in UTF to File
        /// </summary>
        /// <param name="lines">utf lines to write</param>
        public void WriteLineUTF8(string[] lines)
        {
            using (FileStream fs = CreateFileStream())
            using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
            {
                foreach (string line in lines)
                         sw.WriteLine(line);
                sw.Flush();
            } 
        }

        #endregion

        #region Public Properties
         
        /// <summary>
        /// Returns the File Name
        /// </summary>
        public string FileName { get { return (this._FileName + "." + this._FileType); } }

        /// <summary>
        /// Returns the Path where the File is located
        /// </summary>
        public string Path { get { return (this._dirPath); } }

        /// <summary>
        /// Returns both Path and FileName
        /// </summary>
        public string FileNameNPath { get { return (this.Path + "\\" + this.FileName); } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Deletes the File, if it exists
        /// </summary>
        /// <returns>true if successful, false otherwise</returns>
        public bool DeleteFile()
        {
            try
            {
                if (System.IO.File.Exists(this.FileNameNPath))
                {
                    System.IO.File.Delete(this.FileNameNPath);
                    _FileIsCreated = false;
                    return true;
                }
            }
            catch (Exception) { /* ignore */ }
            return false;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the File Stream, either in Create or Append Mode
        /// </summary>
        /// <returns>a File Stream to Write to * Must be Closed by Caller *</returns>
        private FileStream CreateFileStream()
        {
            try
            {
                bool bFileExists = System.IO.File.Exists(this.FileNameNPath);
                if (!_FileIsCreated && _OverideExisting && bFileExists)
                {
                    _FileIsCreated = true;
                    return (new FileStream(this.FileNameNPath, FileMode.Create));
                }
                else if (bFileExists)
                {
                    return (new FileStream(this.FileNameNPath, FileMode.Append));
                }
                else
                {
                    _FileIsCreated = true;
                    return (new FileStream(this.FileNameNPath, FileMode.Create));
                }
            }
            catch (Exception) { /* ignore */ }
            return null;
        }

        #endregion
    }
}
