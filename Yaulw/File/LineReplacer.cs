﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IO = System.IO;

namespace Yaulw.File
{
    #region Rule Definitions

    /// <summary>
    /// Comparer Modifier used when looking at the line
    /// </summary>
    public enum LineReplace_ComparerModifier
    {
        None,
        toLower,
        toUpper,
    }

    /// <summary>
    /// Rule Used to determine which line to replace
    /// ~Replace Line with contains the new line to put instead
    /// </summary>
    public struct LineReplace_Rule
    {
        public string Contains;
        public string StartsWith;
        public string EndsWith;
        public LineReplace_ComparerModifier Comparer;
        public string ReplaceLineWith;
    }

    #endregion

    /// <summary>
    /// Useful for replacing lines in a text file.
    /// It reads in a text file line by line and compares the lines.
    /// Using the rules you can replace a line or multiple lines and write out to the text file.
    /// </summary>
    public class LineReplacer
    {
        #region Private Members

        private Dictionary<String, LineReplace_Rule> _RulesDictionary = new Dictionary<string, LineReplace_Rule>();
        private string _fileNameNPath = "";
        private Encoding _encoding = Encoding.UTF8;

        #endregion

        #region Construction

        /// <summary>
        /// Allows for easy line replacing of a text file
        /// </summary>
        /// <param name="FileNameNPath">Full File Name and Path to a file that exists</param>
        /// <exception cref="ArgumentException">Thrown when File does not exist</exception>
        public LineReplacer(string FileNameNPath, Encoding encoding)
        {
            if (String.IsNullOrEmpty(FileNameNPath) || !IO.File.Exists(FileNameNPath))
                throw new ArgumentException("Line Replace makes only sense on Files that exist already");

            _fileNameNPath = FileNameNPath;
            _encoding = encoding;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Add/Update Line Replacing Rules into here BEFORE calling ReplaceLines()
        /// </summary>
        /// <param name="RuleName">unique name of the rule</param>
        /// <param name="rule">Rule Definition</param>
        /// <returns>true if successfully added, false otherwise</returns>
        public bool AddUpdateRule(string RuleName, LineReplace_Rule rule)
        {
            if (!String.IsNullOrEmpty(RuleName))
            {
                // We must be able to match on something
                if (String.IsNullOrEmpty(rule.StartsWith) && String.IsNullOrEmpty(rule.EndsWith) &&
                    String.IsNullOrEmpty(rule.Contains))
                    return false;

                // We must be able to replace with something
                if (String.IsNullOrEmpty(rule.ReplaceLineWith))
                    return false;

                // Rule is good, add it
                _RulesDictionary[RuleName] = rule;
            }
            return false;
        }

        /// <summary>
        /// Call this Function to Replace all the Lines of the Text Files that
        /// match a specific Rule
        /// </summary>
        public void ReplaceLines()
        {
            if (_RulesDictionary.Count <= 0)
                return;

            // # Readin and Process all Lines
            List<string> ReadInLines = new List<string>();
            using (IO.FileStream fs = new IO.FileStream(_fileNameNPath, IO.FileMode.Open))
            using (IO.StreamReader reader = new IO.StreamReader(fs, _encoding))
            {
                // # Iterate thru all the lines in the File
                for (string Line = reader.ReadLine(); Line != null; Line = reader.ReadLine())
                {
                   FoundRuleMathAndPerformedAction(ref Line);
                   ReadInLines.Add(Line);
                }
            }

            // # Write out all the Lines
            using (IO.FileStream fs = new IO.FileStream(_fileNameNPath, IO.FileMode.Create))
            using (IO.StreamWriter writer = new IO.StreamWriter(fs, _encoding))
            {
                foreach (string Line in ReadInLines)
                    writer.WriteLine(Line);
            }
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// Responsible for finding a match between a line and a rule and
        /// performing the action
        /// </summary>
        /// <param name="Line">Line to find a match for</param>       
        /// <returns>true, if match was found/action was done, false otherwise</returns>
        private bool FoundRuleMathAndPerformedAction(ref string Line)
        {
            if (String.IsNullOrEmpty(Line))
                return false;

            bool bFoundMatch = false;
            // Iterate foreach rule for a line            
            foreach (LineReplace_Rule rule in _RulesDictionary.Values)
            {
                // Make sure to use the proper comparer Modifier for the rule
                string line = Line;
                if (rule.Comparer == LineReplace_ComparerModifier.toLower)
                    line = Line.ToLower();
                else if (rule.Comparer == LineReplace_ComparerModifier.toUpper)
                    line = Line.ToUpper();

                // Now let's match on the actual Rule                
                if (!bFoundMatch && !String.IsNullOrEmpty(rule.StartsWith))
                    bFoundMatch = line.StartsWith(rule.StartsWith);
                if (!bFoundMatch && !String.IsNullOrEmpty(rule.EndsWith))
                    bFoundMatch = line.StartsWith(rule.EndsWith);
                if (!bFoundMatch && !String.IsNullOrEmpty(rule.Contains))
                    bFoundMatch = line.StartsWith(rule.Contains);
                if (bFoundMatch)
                {
                    Line = rule.ReplaceLineWith;
                    break;
                }
            }
            return bFoundMatch;
        }

        #endregion

    }
}
