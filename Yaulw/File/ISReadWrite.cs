﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.IO;
using System.Reflection;

namespace Yaulw.File
{
    /// <summary>
    /// 
    /// </summary>
    public enum ISScope
    {
        User,
        Machine,
    }

    /// <summary>
    /// Class allows reading and writing to isolated Storage
    /// </summary>
    public class ISReadWrite
    {
        #region Private Members

        private string _ISOLATED_STORAGE_FILENAME = "";
        private bool _bFileInIsoStoreExists = false;
        private Xml.XSerializer _XSerializer = null;
        private IsolatedStorageScope _scope = IsolatedStorageScope.None;
        private IsolatedStorageFile _isoStore = null;

        #endregion

        #region Construction

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ISFileName"></param>
        public ISReadWrite(string ISFileName) : this(ISFileName, ISScope.Machine) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ISFileName"></param>
        /// <param name="scope"></param>
        public ISReadWrite(string ISFileName, ISScope scope)
        {
            _ISOLATED_STORAGE_FILENAME = ISFileName;

            if (String.IsNullOrEmpty(_ISOLATED_STORAGE_FILENAME))
                throw new ArgumentNullException("ISFileName can not be empty.");            
#if NET4
            if (!IsolatedStorageFile.IsEnabled)
                throw new Exception("Isolated Storage not enabled");
#endif
            _scope = ConvertISScopeToIsolatedStorageScope(scope);
            _isoStore = IsolatedStorageFile.GetStore(_scope, typeof(System.Security.Policy.Url), typeof(System.Security.Policy.Url));
            _bFileInIsoStoreExists = (_isoStore.GetFileNames(ISFileName).Length > 0);
            _XSerializer = new Xml.XSerializer();
        }

        #endregion

        #region Public Read / Write Methods

        public string ReadFromIS()
        {
            using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream(_ISOLATED_STORAGE_FILENAME, FileMode.OpenOrCreate, _isoStore))
            using (StreamReader reader = new StreamReader(fs))
            {
                _bFileInIsoStoreExists = true;
                string strFileContents = reader.ReadToEnd();
                if (!String.IsNullOrEmpty(strFileContents))
                    return strFileContents;
            }
            return String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T ReadFromIS<T>()
        {
            using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream(_ISOLATED_STORAGE_FILENAME, FileMode.OpenOrCreate, _isoStore))
            using (StreamReader reader = new StreamReader(fs))
            {
                _bFileInIsoStoreExists = true;
                string strFileContents = reader.ReadToEnd();
                if (!String.IsNullOrEmpty(strFileContents))
                {
                    T t = _XSerializer.ReadFromString<T>(strFileContents);
                    return t;
                }
            }

            return default(T);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SerializableObject"></param>
        public void WriteToIS<T>(T XMLSerializableObject) where T : new()
        {
            using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream(_ISOLATED_STORAGE_FILENAME, FileMode.Create, _isoStore))
            using (StreamWriter writer = new StreamWriter(fs))
            {
                _bFileInIsoStoreExists = true;
                string strFileContentsToWrite = _XSerializer.WriteToString<T>(XMLSerializableObject);
                if(!String.IsNullOrEmpty(strFileContentsToWrite))
                    writer.Write(strFileContentsToWrite);
            }            
        }

        /// <summary>
        /// 
        /// </summary>                
        public void WriteToIS(string Content)
        {
            using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream(_ISOLATED_STORAGE_FILENAME, FileMode.Create, _isoStore))
            using (StreamWriter writer = new StreamWriter(fs))
            {
                _bFileInIsoStoreExists = true;
                string strFileContentsToWrite = Content;
                if (!String.IsNullOrEmpty(strFileContentsToWrite))
                    writer.Write(strFileContentsToWrite);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        public void Delete()
        {
            if (_bFileInIsoStoreExists)
            {
                _isoStore.DeleteFile(_ISOLATED_STORAGE_FILENAME);
                _bFileInIsoStoreExists = false;
            }
        }

        #endregion

        #region Public Properties

        public bool Exists
        {
            get { return _bFileInIsoStoreExists; }
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        private IsolatedStorageScope ConvertISScopeToIsolatedStorageScope(ISScope scope)
        {
            switch (scope)
            {
                case ISScope.User:
                    return IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly;

                case ISScope.Machine:
                    return IsolatedStorageScope.Machine | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly;
            }
            return IsolatedStorageScope.Application;
        }

        #endregion
    }
 
}
