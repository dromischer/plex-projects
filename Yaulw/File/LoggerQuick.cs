﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Yaulw.Assembly;

namespace Yaulw.File
{
    /// <summary>
    /// Easy Class for quick and dirty logging *that can automatically on a daily basis*
    /// </summary>
    public class LoggerQuick
    {
        #region Private Members

        /// <summary>
        /// FileWriter Object
        /// </summary>
        private Yaulw.File.FileWriter _fileWriter = null;

        /// <summary>
        /// Current Directory
        /// </summary>
        private string _curPath = "";

        /// <summary>
        /// Log Debug Messages
        /// </summary>
        private bool _LogDebug = false;

        /// <summary>
        /// Name of the Log (also serves as RegKey)
        /// </summary>
        private string LogName = "";

        #endregion

        #region Construction

        /// <summary>
        /// Quick Logger (which can clear itself daily and has no dependencies)
        /// quick n' dirty logs in the current directory from which it is called
        /// </summary>
        /// <param name="LogName">Name of the Log file</param>
        /// <param name="bLogDebugMessages">true to spit out DebugMessages, false otherwise</param>
        public LoggerQuick(string LogName, bool bLogDebugMessages)
        {
            if(String.IsNullOrEmpty(LogName))
                throw new ArgumentException("LogName can't be empty");

            // Log Name, which will also server as the registry key name
            this.LogName = Path.GetFileName(LogName);

            // Log Debug Messages?
            _LogDebug = bLogDebugMessages;

            // Get the current running directory
            if (String.IsNullOrEmpty(_curPath))
                _curPath = Path.GetDirectoryName(AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Executing));

            // Set up FileWriter
            if (_fileWriter == null)
                _fileWriter = new Yaulw.File.FileWriter(LogName, "log", _curPath, false);
        }

        /// <summary>
        /// Quick Logger (which can clear itself daily and has no dependencies)
        /// quick n' dirty logs in the current directory from which it is called
        /// </summary>
        /// <param name="LogName">Name of the Log file</param>
        /// <param name="bLogDebugMessages">true to spit out DebugMessages, false otherwise</param>
        /// <param name="path">path to log to</param>
        public LoggerQuick(string LogName, bool bLogDebugMessages, string path)
        {
            if (String.IsNullOrEmpty(LogName))
                throw new ArgumentException("LogName can't be empty");

            // Log Name, which will also server as the registry key name
            this.LogName = Path.GetFileName(LogName);

            // Log Debug Messages?
            _LogDebug = bLogDebugMessages;

            // make sure path is valid
            try
            {
                if (!String.IsNullOrEmpty(path))
                {
                    path = Path.GetDirectoryName(path);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                }
            }
            catch (Exception) { /* ignore */ }

            // Get the current running directory
            if (String.IsNullOrEmpty(_curPath) && String.IsNullOrEmpty(path))
                _curPath = Path.GetDirectoryName(AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Executing));
            else
                _curPath = path;

            // Set up FileWriter
            if (_fileWriter == null)
                _fileWriter = new Yaulw.File.FileWriter(LogName, "log", _curPath, false);
        }

        #endregion

        #region Public Log Methods

        /// <summary>
        /// Debug Logging only logs when _LogDebug is set        
        /// </summary>
        /// <param name="line"></param>
        /// <param name="args"></param>
        public void Debug(string line, params object[] args)
        {
            if (_LogDebug)
                Log("Debug: ", line, args);
        }

        /// <summary>
        /// Info Logging
        /// </summary>
        /// <param name="line"></param>
        /// <param name="args"></param>
        public void Info(string line, params object[] args)
        {            
           Log("Info: ", line, args);
        }

        /// <summary>
        /// Error Logging
        /// </summary>
        /// <param name="line"></param>
        /// <param name="args"></param>
        public void Error(string line, params object[] args)
        {         
            Log("Error: ", line, args);
        }

        #endregion

        #region Private Log Methods

        /// <summary>
        /// Used for logging * Medisoft people * crack me up
        /// </summary>
        /// <param name="line"></param>
        /// <param name="ars"></param>
        private void Log(string prePrend, string line, params object[] args)
        {
            if (_fileWriter != null)
            {
                // Clear the log once a day automatically - yeah!
                DateTime LastLogged = Yaulw.Registry.RegKey.GetKey<DateTime>("LoggerQuick", LogName, DateTime.MinValue);
                if (LastLogged != DateTime.MinValue && LastLogged.Day != DateTime.Now.Day)                
                    _fileWriter.DeleteFile();
                
                // Always set the DT Stamp, for every log
                Yaulw.Registry.RegKey.SetKey<DateTime>("LoggerQuick", LogName, DateTime.Now);

                // Now let's start Logging
                line = prePrend + line;
                if (args != null)
                    _fileWriter.WriteLineA(String.Format(line, args));
                else
                    _fileWriter.WriteLineA(line);
            }
        }

        #endregion

    }
}
