﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yaulw.Tools
{        
    /// <remarks>
    /// Useful Utilities around objects
    /// </remarks>
    public static class ObjTool
    {
        /// <summary>
        /// Returns true if passed in object is valid and is not empty
        /// </summary>
        /// <param name="oToValidate">an object to validate</param>
        /// <returns>true if valid, false otherwise</returns>
        public static bool IsNotNullAndNotEmpty(object oToValidate)
        {
            if ((oToValidate != null) && (oToValidate.ToString() != String.Empty))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Use this to just check if the object can be converted to a string
        /// via ConvertObjToString(). If false, this is most likely an object type that shouldn't be converted to a string.
        /// </summary>
        /// <typeparam name="T">Can be Any type</typeparam>
        /// <param name="objToCheckConvert">an object of type T to check if String Convertible</param>
        /// <returns>true, if the object passed in a String Convertible type like string, bool, int32, double, decimal, etc..., false otherwise</returns>
        public static bool IsOfTypeConvertibleToString<T>(T objToCheckConvert)
        {
            bool retVal = false;
            T objToConvert = objToCheckConvert;

            // Check if String Convertible
            if (objToConvert is System.Char)
            {
                retVal = true;
            }
            else if (objToConvert is System.String)
            {
                retVal = true;
            }
            else if (objToConvert is System.Decimal)
            {
                retVal = true;
            }
            else if (objToConvert is System.Int32)
            {
                retVal = true;
            }
            else if (objToConvert is System.Int64)
            {
                retVal = true;
            }
            else if (objToConvert is System.Single)
            {
                retVal = true;
            }
            else if (objToConvert is System.Double)
            {
                retVal = true;
            }
            else if (objToConvert is System.Boolean)
            {
                retVal = true;
            }
            else if (objToConvert is System.DateTime)
            {
                retVal = true;
            }
#if NET4
            else if (objToConvert is System.Guid)  // Only works in .Net 4.0
            {
                retVal = true;
            }
#endif
            else if (objToConvert is System.IntPtr)
            {
                retVal = true;
            }
            else if (objToConvert is System.UInt32)
            {
                retVal = true;
            }
            else if (objToConvert is System.UInt64)
            {
                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Convert a string to an Object of Type T
        /// </summary>
        /// <typeparam name="T">Should be a System Type like string, bool, int32, double, decimal, etc...</typeparam>
        /// <param name="strToConvert">String Value to Convert</param>
        /// <returns>an converted Object of Type t, or T Default if error occured</returns>
        public static T ConvertStringToObj<T>(string strToConvert)
        {

            // Create a Default Type T
            T retVal = default(T);

            try
            {
                #region Conversion

                if (retVal is System.Char)
                {
                    retVal = (T)((object)strToConvert[0]);
                }
                else if (retVal is System.String)
                {
                    retVal = (T)((object)strToConvert);
                }
                else if (retVal is System.Decimal)
                {
                    retVal = (T)((object)Decimal.Parse(strToConvert));
                }
                else if (retVal is System.Int32)
                {
                    retVal = (T)((object)Int32.Parse(strToConvert));
                }
                else if (retVal is System.Int64)
                {
                    retVal = (T)((object)Int64.Parse(strToConvert));
                }
                else if (retVal is System.Single)
                {
                    retVal = (T)((object)Single.Parse(strToConvert));
                }
                else if (retVal is System.Double)
                {
                    retVal = (T)((object)Double.Parse(strToConvert));
                }
                else if (retVal is System.Boolean)
                {
                    retVal = (T)((object)Boolean.Parse(strToConvert));
                }
                else if (retVal is System.DateTime)
                {
                    retVal = (T)((object)DateTime.Parse(strToConvert));
                }
#if NET4
                else if (retVal is System.Guid)  // Works in .Net 4.0 and up
                {
                    retVal = (T)((object)Guid.Parse(strToConvert));
                }
#endif
                else if (retVal is System.IntPtr)
                {
                    if (IntPtr.Size <= 4)
                    {
                        Int32 i = Int32.Parse(strToConvert);
                        retVal = (T)((object)new IntPtr(i));
                    }
                    else
                    {
                        Int64 i = Int64.Parse(strToConvert);
                        retVal = (T)((object)new IntPtr(i));
                    }
                }
                else if (retVal is System.UInt32)
                {
                    retVal = (T)((object)UInt32.Parse(strToConvert));
                }
                else if (retVal is System.UInt64)
                {
                    retVal = (T)((object)UInt64.Parse(strToConvert));
                }
                else
                {
                    retVal = (T)((object)(strToConvert));
                }

                #endregion
            }
            catch (Exception e) { string Message = e.Message; /* ignore */ }

            // return T
            return retVal;
        }

        /// <summary>
        /// Convert an object of type t to a String
        /// </summary>
        /// <typeparam name="T">Should be a System Type like string, bool, int32, double, decimal, etc...</typeparam>
        /// <param name="objToConvert">Object Value to Convert</param>
        /// <returns>a string that contains the value of the Object</returns>
        public static string ConvertObjToString<T>(T objToConvert)
        {
            String retVal = String.Empty;
            try
            {
                #region Conversion

                if (objToConvert is System.Char)
                {
                    retVal = ((Char)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.String)
                {
                    retVal = ((String)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.Decimal)
                {
                    retVal = ((Decimal)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.Int32)
                {
                    retVal = ((Int32)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.Int64)
                {
                    retVal = ((Int64)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.Single)
                {
                    retVal = ((Single)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.Double)
                {
                    retVal = ((Double)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.Boolean)
                {
                    retVal = ((Boolean)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.DateTime)
                {
                    retVal = ((DateTime)((object)objToConvert)).ToString();
                }
#if NET4
                else if (objToConvert is System.Guid)
                {
                    retVal = ((Guid)((object)objToConvert)).ToString();
                }
#endif
                else if (objToConvert is System.IntPtr)
                {
                    retVal = ((IntPtr)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.UInt32)
                {
                    retVal = ((UInt32)((object)objToConvert)).ToString();
                }
                else if (objToConvert is System.UInt64)
                {
                    retVal = ((UInt64)((object)objToConvert)).ToString();
                }
                else
                {
                    retVal = ((object)objToConvert).ToString();
                }

                #endregion
            }
            catch (Exception) { /* ignore */ }

            // return T
            return retVal;
        }
    }
}
