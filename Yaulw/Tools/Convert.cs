﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yaulw.Tools
{
    /// <remarks>
    /// Common Conversion Operations
    /// </remarks>
    public static class Convert
    {
        /// <summary>
        /// Convert a a String to a UTF Byte Array
        /// </summary>
        /// <param name="str">string to convert</param>
        /// <returns>a Byte Array from a String</returns>
        public static byte[] StrToByteArrayUTF(string str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                return encoding.GetBytes(str);
            }
            return null;
        }

        /// <summary>
        /// Convert a a String to an ASCII Byte Array
        /// </summary>
        /// <param name="str">string to convert</param>
        /// <returns>a Byte Array from a String</returns>
        public static byte[] StrToByteArrayAscii(string str)
        {
            if (!String.IsNullOrEmpty(str))
            {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                return encoding.GetBytes(str);
            }
            return null;
        }


    }
}
