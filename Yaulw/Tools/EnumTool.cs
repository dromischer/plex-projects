﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yaulw.Tools
{
    /// <remarks>
    /// Common Operations on Enumerations
    /// </remarks>
    public static class EnumTool
    {
        #region Common Operations

        /// <summary>
        /// Turns an Enum to a String (Replacing _ with ' ')
        /// </summary>
        /// <param name="_enum">an Enum which could possibly contain an '_'</param>
        /// <returns>a ' ' spaced String of Enum</returns>
        public static string Enum_ToString(Enum _enum)
        {
            return _enum.ToString().Replace('_', ' ');
        }

        /// <summary>
        /// Using the '__' in the Enum Name allows to embed Categories in the Name
        /// </summary>
        /// <param name="_enum">an Enum with '__'</param>
        /// <param name="Category">The String Value before the '__', also replacing '_' with ' '</param>
        /// <param name="Key">The String Value after the '__', also replacing '_' with ' '</param>
        public static void Enum_ToCategoryNKey(Enum _enum, out string Category, out string Key)
        {
            Category = String.Empty;
            Key = String.Empty;

            if (!Enum_HasCategoryNKey(_enum))
                throw new ArgumentException("_enum must contain a __ with category and key values");

            string _enumStr = _enum.ToString();
            int nIndex = _enumStr.IndexOf("__");
            
            // Assign out the Retrived Values
            Category = _enumStr.Substring(0, nIndex).Replace('_', ' ');
            Key = _enumStr.Substring(nIndex + 2).Replace('_', ' ');
        }

        #endregion

        #region HasCategoryNKey

        /// <summary>
        /// Determines if the Passed in Enum has both a Category and Key
        /// </summary>
        /// <param name="_enum">an Enum with '__'</param>
        /// <returns>true if '__' is present with both a category and key value</returns>
        public static bool Enum_HasCategoryNKey(Enum _enum)
        {
            // Check to see there is only one __
            string _enumStr = _enum.ToString();
            int nIndexFirst = _enumStr.IndexOf("__");
            int nIndexLast = _enumStr.LastIndexOf("__");
            if (nIndexFirst == nIndexLast)
            {
                // Check to see that there are values infront and behind the __
                if (nIndexFirst != 0 && (nIndexLast < (_enumStr.Length - 1)))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determines if all the Enums of the Passed in Enum Type have both a Category and Key
        /// </summary>
        /// <param name="_enum">an Enum Type that has all enums with '__'</param>
        /// <returns>true if '__' is present with both a category and key value on all enums</returns>
        public static bool EnumType_HasCategoryNKey(Type _enumType)
        {
            if (_enumType.IsEnum)
            {
                foreach (string enumName in Enum.GetNames(_enumType))
                {
                    bool bIsValid = Enum_HasCategoryNKey((Enum)Enum.Parse(_enumType, enumName));
                    if (!bIsValid)
                        return false;
                }
                return true;
            }
            return false;
        }

        #endregion

        #region Category N' Key Traversals

        /// <summary>
        /// Retrieves all the Categories of an Enum
        /// </summary>
        /// <param name="_enum">an Enum Type that has all enums with '__'</param>
        /// <returns>An Array of all unique Categories, '_' replaced with ' ', or null if error occured</returns>
        public static string[] EnumType_GetCategories(Type _enumType)
        {
            if (_enumType.IsEnum && EnumType_HasCategoryNKey(_enumType))
            {
                List<String> retVal = new List<String>();
                foreach (string enumName in Enum.GetNames(_enumType))
                {
                    string Category;
                    string Key;
                    Enum_ToCategoryNKey((Enum)Enum.Parse(_enumType, enumName), out Category, out Key);
                    if(!retVal.Contains(Category))
                        retVal.Add(Category);
                }
                return retVal.ToArray();
            }
            return null;
        }

        /// <summary>
        /// Retrieves all the Keys of an Enum for a specified Category
        /// </summary>
        /// <param name="_enum">an Enum Type that has all enums with '__'</param>
        /// <returns>An Array of all unique Keys, '_' replaced with ' ', or null if error occured</returns>
        public static string[] EnumType_GetKeys(Type _enumType, string Category)
        {
            if (_enumType.IsEnum && EnumType_HasCategoryNKey(_enumType))
            {
                List<String> retVal = new List<String>();
                foreach (string enumName in Enum.GetNames(_enumType))
                {
                    string CategoryTemp;
                    string Key;
                    Enum_ToCategoryNKey((Enum)Enum.Parse(_enumType, enumName), out CategoryTemp, out Key);
                    if (String.Compare(Category, CategoryTemp, true) == 0)
                        retVal.Add(Key);
                }
                return retVal.ToArray();
            }
            return null;
        }

        #endregion
    }
}
