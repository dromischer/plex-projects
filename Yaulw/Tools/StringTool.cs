﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yaulw.Tools
{
    /// <summary>
    /// Useful String Operations
    /// </summary>
    public static class StringTool
    {       
        /// <summary>
        /// Searches for the 'value' occurences that occur in 'str'
        /// </summary>
        /// <param name="value">string to search occurences for</param>
        /// <param name="str">string to search in</param>
        /// <param name="bIgnoreCase">true to ignore case, false otherwise</param>
        /// <returns>the number of occurences of 'value' in 'str', 0 for none</returns>
        public static uint NumberOfSpecStringFoundInString(string value, string str, bool bIgnoreCase = false)
        {
            uint nCount = 0;
            if (!String.IsNullOrEmpty(str))
            {
                int nIndex = 0;
                StringComparison comparison = bIgnoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture;                
                do
                {
                    nIndex = str.IndexOf(value, nIndex, comparison);
                    if(nIndex != -1)
                       ++nCount;
                } while (nIndex != -1);                
            }
            return nCount;
        }

        /// <summary>
        /// Check to see if string starts with a letter
        /// </summary>
        /// <param name="str">string to check</param>
        /// <returns>true if string starts with a letter</returns>
        public static bool StringStartsWithLetter(string str)
        {
            if (!String.IsNullOrEmpty(str))
                return char.IsLetter(str[0]);
            return false;
        }

        /// <summary>
        /// Check to see if string ends with a letter
        /// </summary>
        /// <param name="str">string to check</param>
        /// <returns>true if string ends with a letter</returns>
        public static bool StringEndsWithLetter(string str)
        {
            if (!String.IsNullOrEmpty(str))
                return char.IsLetter(str[str.Length - 1]);
            return false;
        }

        /// <summary>
        /// Returns the last word of a string (string must end with a letter)
        /// </summary>
        /// <param name="str">string to go thru</param>
        /// <returns>the last word of a string</returns>
        public static string StringFetchLastWord(string str)
        {
            if (!String.IsNullOrEmpty(str) && StringEndsWithLetter(str))
            {
                // Find the last word
                StringBuilder sb = new StringBuilder();
                for (int i = str.Length - 1; i >= 0; --i)
                {
                    if (char.IsLetter(str[i]))
                        sb.Append(str[i]);
                    else
                        break;
                }

                // Reverse the String
                string strRet = sb.ToString();
#if NET4
                sb.Clear();
#else
                sb = new StringBuilder();
#endif
                for (int i = strRet.Length - 1; i >=0; --i)
                     sb.Append(strRet[i]);

                // return the string
                return sb.ToString();
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the first word of a string (string must start with a letter)
        /// </summary>
        /// <param name="str">string to go thru</param>
        /// <returns>the first word of a string</returns>
        public static string StringFetchFirstWord(string str)
        {
            if (!String.IsNullOrEmpty(str) && StringStartsWithLetter(str))
            {
                // Find the last word
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < str.Length; ++i)
                {
                    if (char.IsLetter(str[i]))
                        sb.Append(str[i]);
                    else
                        break;
                }

                // return the string
                return sb.ToString();
            }
            return String.Empty;
        }

        /// <summary>
        /// Stripes the last word from the referenced string (string must end with a letter)
        /// </summary>
        /// <param name="str">last word stripped from string</param>
        public static void StringStripeLastWord(ref string str)
        {
            if (!String.IsNullOrEmpty(str) && StringEndsWithLetter(str))
            {
                // Find the last word
                int i = 0;
                for (i = str.Length - 1; i >= 0; --i)
                {
                    if (!char.IsLetter(str[i]))
                        break;
                }

                // Return string without last word
                str = str.Substring(0, i + 1);
            }
        }

        /// <summary>
        /// Stripes the first word from the referenced string (string must start with a letter)
        /// </summary>
        /// <param name="str">first word stripped from string</param>
        public static void StringStripeFirstWord(ref string str)
        {
            if (!String.IsNullOrEmpty(str) && StringStartsWithLetter(str))
            {
                // Find the last word
                int i = 0;
                for (i = 0; i < str.Length; ++i)
                {
                    if (!char.IsLetter(str[i]))
                        break;
                }

                // Return string without first word
                str = str.Substring(i);
            }
        }

    }
   
}
