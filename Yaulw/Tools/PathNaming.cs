﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Yaulw.Tools
{
    /// <remarks>
    /// Tools For File/Directory Paths
    /// </remarks>
    public static class PathNaming
    {
        /// <summary>
        /// Get the Root Path for a given path string
        /// </summary>
        /// <param name="path">path</param>
        /// <returns>the root folder, with '\'</returns>
        public static string GetPathRoot(string path)
        {
            if(!String.IsNullOrEmpty(path))
                return PathEndsWithSlash(System.IO.Path.GetPathRoot(path));
            return "";
        }

        /// <summary>
        /// Does Path contain file information
        /// </summary>
        /// <returns>returns true if the specified path information contains file information</returns>
        public static bool PathContainsFile(string path)
        {
            if (!String.IsNullOrEmpty(path) && (path.LastIndexOf('.') > path.LastIndexOf('\\')))
                return true;
            return false;
        }

        /// <summary>
        /// Makes sure that the specified path begins with a '\'
        /// </summary>
        /// <returns>a path string that begins with \</returns>
        public static string PathBeginsWithSlash(string path)
        {
            if (!String.IsNullOrEmpty(path) && (path[0] != '\\'))
                return '\\' + path;
            return path;
        }

        /// <summary>
        /// Makes sure that the specified path does NOT begin with a '\'
        /// </summary>
        /// <returns>a path string that does NOT begin with \</returns>
        public static string PathBeginsWithNoSlash(string path)
        {
            if (!String.IsNullOrEmpty(path) && (path[0] == '\\'))
                return path.Substring(1);
            return path;
        }

        /// <summary>
        /// Makes sure that the specified path ends with a '\'
        /// </summary>
        /// <returns>a path string that ends with \</returns>
        public static string PathEndsWithSlash(string path)
        {
            if (!String.IsNullOrEmpty(path) && (path[path.Length - 1] != '\\'))
                return path + '\\';
            return path;
        }

        /// <summary>
        /// Makes sure that the specified path does NOT end with a '\'
        /// </summary>
        /// <returns>a path string that does NOT end with \</returns>
        public static string PathEndsWithNoSlash(string path)
        {
            if (!String.IsNullOrEmpty(path) && (path[path.Length - 1] == '\\'))
                return path.Substring(0, path.Length - 1);
            return path;
        }

        /// <summary>
        /// Returns True if the fileName is valid * Filename chars are More restrictive than Path * should always use this
        /// </summary>
        /// <param name="FileName">filename to check for</param>
        /// <returns>true, if valid, false otherwise</returns>
        public static bool FileNameIsValid(string FileName)
        {
            if (!String.IsNullOrEmpty(FileName))
            {
                // stripe out the filename, if possible, if not, just use what is
                // passed into us
                string file = Path.GetFileName(FileName);
                if (!String.IsNullOrEmpty(file))
                    FileName = file;                

                char[] invalidChars = Path.GetInvalidFileNameChars();
                foreach (char c in invalidChars)
                {
                    if (FileName.Contains(c))
                        return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns True if the Directory Path is valid * Path chars are not as restrictive as File * should always use File Chars (better safe than sorry)
        /// Use FileNameIsValid()
        /// </summary>
        /// <param name="DirectoryPath">DirectoryPath to check for</param>
        /// <returns>true, if valid, false otherwise</returns>
        public static bool IsDirectoryPathValid(string DirectoryPath)
        {
            if (!String.IsNullOrEmpty(DirectoryPath))
            {
                // stripe the trailing '\\', if possible, if not, just use what is
                // passed into us
                string path = Path.GetDirectoryName(DirectoryPath);
                if (!String.IsNullOrEmpty(path))
                    DirectoryPath = path;                

                // FileName Char restrictions contain everything we need,
                // except for a path we want to allow '\\'
                char[] invalidChars = Path.GetInvalidPathChars();

                foreach (char c in invalidChars)
                {
                    // allow '\\' for paths
                    if (c != '\\' && DirectoryPath.Contains(c))
                        return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// return a valid directory path, a path that is invalid and contains invalid
        /// characters is automatically converted, (illegal characters are skipped)
        /// </summary>
        /// <param name="DirectoryPath">DirectoryPath to change</param>
        /// <returns>the 'corrected' path string</returns>
        public static string MakeDirectoryPathValid(string DirectoryPath)
        {
            return MakeDirectoryPathValid(DirectoryPath, String.Empty);
        }

        /// <summary>
        /// return a valid directory path, a path that is invalid and contains invalid
        /// characters is automatically converted, (illegal characters are skipped)
        /// </summary>
        /// <param name="DirectoryPath">DirectoryPath to change</param>
        /// <param name="replacementStr">the string to put in place instead of the invalid char found,
        /// it itself can't be an invalid char</param>
        /// <returns>the 'corrected' path string</returns>
        public static string MakeDirectoryPathValid(string DirectoryPath, string replacementStr)
        {
            if(!String.IsNullOrEmpty(DirectoryPath))
            {
                // stripe the trailing '\\', if possible, if not, just use what is
                // passed into us
                string path = Path.GetDirectoryName(DirectoryPath);
                if (!String.IsNullOrEmpty(path))
                    DirectoryPath = path;
                
                // FileName Char restrictions contain everything we need,
                // except for a path we want to allow '\\'
                char[] invalidChars = Path.GetInvalidFileNameChars();

                // Make sure replacementStr is valid also, otherwise it somewhat defeats the purpose
                // of this whole function
                string strToUseForReplacement = "";
                bool replacementStrIsValid = false;
                if (replacementStr != null)
                {
                    replacementStrIsValid = true;
                    if (replacementStr != "")
                    {
                        foreach (char c in replacementStr)
                        {
                            invalidChars.Contains(c);
                            replacementStrIsValid = false;
                            break;
                        }
                    }
                }
                if (replacementStrIsValid)
                    strToUseForReplacement = replacementStr;

                // Construct new String Path
                StringBuilder sb = new StringBuilder();
                foreach (char c in DirectoryPath)
                {
                    // allow '\\' for paths
                    if (c != '\\' && invalidChars.Contains(c))
                    {
                        sb.Append(strToUseForReplacement);
                    }
                    else
                    {
                        sb.Append(c);
                    }
                }
                return sb.ToString();
            }
            return DirectoryPath;
        }

        /// <summary>
        /// Retrieve the last directory name from a path, can be a UNC path like \\{server}\Directory
        /// or a file and system path like C:\{directory}\{directory}, will always return the name of the
        /// last directory in the path. if it is not a path, i.e just a name is passed in without '\'s then
        /// it will just stripe out all '\' that maybe there and return the name
        /// </summary>
        /// <param name="DirectoryPath"></param>
        /// <returns>the name of the last directory in the path</returns>
        public static string RetrieveLastDirectoryNameInPath(string DirectoryPath)
        {
            if (!String.IsNullOrEmpty(DirectoryPath))
            {
                // Find the last folder and return that information
                // This is what this function was created for                
                string dp = Path.GetDirectoryName(DirectoryPath);
                int nLast = dp.LastIndexOf('\\');
                if (nLast != -1 && dp.Length > 3)
                {
                    dp = dp.Substring(nLast + 1);
                    return dp;
                }                

                // Return what was passed into us, probably doesn't have a '\\',
                // but it could be that it just has one. like someone passed in \\{server},
                // then we'll just return the {server}, if someone passes in c:\directory, then,
                // we should pass back just the directory
                if (DirectoryPath.Length > 3 && DirectoryPath[1] == ':')
                {
                    return DirectoryPath.Substring(3);
                }
                else
                {
                    // try finding the last \\, again without using GetDirectoryName which
                    // stripes it, return the last stuff found, otherwise just return what was passed to us
                    nLast = DirectoryPath.LastIndexOf('\\');
                    if (nLast != -1 && DirectoryPath.Length > 3)
                    {
                        return DirectoryPath.Substring(nLast + 1);
                    }
                    else
                    {
                        return DirectoryPath;
                    }
                }                
            }
            return String.Empty;
        }

    }
}
