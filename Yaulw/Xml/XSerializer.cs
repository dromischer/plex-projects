﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Yaulw.Xml
{
    /// <remarks>
    /// Wrapper class around XMLReading/Writing (Serialization)
    /// </remarks>
    public class XSerializer
    {
        #region Private Members

        private XmlSerializer _Serializer = null;
        private Type _SerializerAssignedToType = null;

        #endregion

        #region * Private Initialization * Called at the begining of every function call

        /// <summary>
        /// Pass in a valid XML Serializable Object in order to initialize the XML Serializer
        /// ~Only newes a NEW Serializer object if the underlying Type for this class changed
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if an invalid XmlObject is passed in</exception>
        /// <exception cref="OutOfMemoryExceptoin">Thrown if by the end of this function, no Serializer Object Exists</exception>
        private void InitializeSerializer<T>()
        {
            // Create the Serializer Object, if Needed
            bool bCreateSerializer = false;
            if (_Serializer == null)
                bCreateSerializer = true;
            else if (_SerializerAssignedToType != typeof(T))
                bCreateSerializer = true;

            if (bCreateSerializer)
            {
                _Serializer = null;
                _SerializerAssignedToType = null;

                // Create Serializer Obj
                _SerializerAssignedToType = typeof(T);
                _Serializer = new XmlSerializer(typeof(T));
            }

            // Something is wrong, if Serializer, is not created at this point
            if (_Serializer == null)
                throw new OutOfMemoryException();
        }

        #endregion

        #region Construction

        public XSerializer() { }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reads in an XML Object from a String
        /// </summary>
        /// <typeparam name="T">XMLSerializable Type</typeparam>
        /// <param name="xml">an xml string to read</param>
        /// <returns>Deserialized Object</returns>
        public T ReadFromString<T>(string xml)
        {
            InitializeSerializer<T>();

            // Deserialize the Resource and return the Object
            using (StringReader reader = new StringReader(xml))
            {
                T retVal = (T)_Serializer.Deserialize(reader);
                return retVal;
            }
        }

        /// <summary>
        /// Reads in an XML Object from a String
        /// </summary>
        /// <typeparam name="T">XMLSerializable Type</typeparam>
        /// <param name="xml">an xml stream to read</param>
        /// <returns>Deserialized Object</returns>
        public T ReadFromStream<T>(Stream xml)
        {
            InitializeSerializer<T>();

            // Deserialize the Resource and return the Object
            using (StreamReader reader = new StreamReader(xml))
            {
                T retVal = (T)_Serializer.Deserialize(reader);
                return retVal;
            }
        }

        /// <summary>
        /// Reads in an XML Object from a Stream (Resource)
        /// </summary>
        /// <typeparam name="T">XMLSerializable Type</typeparam>
        /// <param name="ResourceStream">A Stream to read from</param>
        /// <returns>Deserialized Object</returns>
        public T ReadFromResource<T>(Stream ResourceStream)
        {
            InitializeSerializer<T>();

            // Deserialize the Resource and return the Object
            using (TextReader reader = new StreamReader(ResourceStream))
            {
                T retVal = (T)_Serializer.Deserialize(reader);
                return retVal;
            }
        }

        /// <summary>
        /// Reads in an XML Object from a File
        /// </summary>
        /// <typeparam name="T">XMLSerializable Type</typeparam>
        /// <param name="PathNFileName">Valid Path to XML File to Deserialize</param>
        /// <returns>Deserialized Object, or null if file not found</returns>
        public T ReadFromFile<T>(string PathNFileName)
        {
            InitializeSerializer<T>();

            // Check File Existence
            if (!System.IO.File.Exists(PathNFileName))
                return default(T);

            // Deserialize the File and return the Object
            using (TextReader reader = new StreamReader(PathNFileName))
            {
                T retVal = (T)_Serializer.Deserialize(reader);
                return retVal;
            }
        }

        /// <summary>
        /// Writes an XML Object to a String
        /// </summary>
        /// <typeparam name="T">XMLSerializable Type</typeparam>
        /// <param name="XmlObject">An XML Serializable Object, can be null to write Blank Object</param>
        /// <returns>a serialized XML Object as a string</returns>
        public string WriteToString<T>(T XmlObject) where T : new()
        {
            InitializeSerializer<T>();

            // Determine 'Blank' or Running Object
            T objectToSerialize = default(T);
            if (XmlObject == null)
                objectToSerialize = new T();
            else
                objectToSerialize = XmlObject;

            using (StringWriter writer = new StringWriter())
            {
                _Serializer.Serialize(writer, objectToSerialize);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Writes an XML Object to a File
        /// </summary>
        /// <typeparam name="T">XMLSerializable Type</typeparam>
        /// <param name="XmlObject">An XML Serializable Object, can be null to write Blank Object</param>
        /// <param name="PathNFileName">Valid Path to XML File to Serialize</param>
        public void WriteToFile<T>(T XmlObject, string PathNFileName) where T : new()
        {
            InitializeSerializer<T>();

            // Create output directory, if not exists
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(PathNFileName)))
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(PathNFileName));

            // Determine 'Blank' or Running Object
            T objectToSerialize = default(T);
            if (XmlObject == null)
                objectToSerialize = new T();
            else
                objectToSerialize = XmlObject;

            // Write File;
            using (TextWriter writer = new StreamWriter(PathNFileName))
            {
                _Serializer.Serialize(writer, objectToSerialize);
            }
        }

        #endregion
    }
}
