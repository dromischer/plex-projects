﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Yaulw.Process;

namespace Yaulw.Other
{
    public static class Installer
    {

        /// <summary>
        /// Run a command on the commandline * Hidden *
        /// </summary>
        /// <param name="cmdline">cmd to run</param>
        public static string RunCmdLine(string cmdline)
        {
            string result = PStarter.RunDosCommand(cmdline);
            return result;
        }

        /// <summary>
        /// To grant the specified User or group Full Control permissions to the folder and its contents
        /// </summary>
        /// <param name="FolderNPath">full path to folder/directory</param>
        /// <param name="UserOrGroup">domainname\administrator, any windows user or group</param>
        /// <returns></returns>
        public static bool GrantFullPermissionToFolderForUserOrGroup(string FolderNPath, string UserOrGroup)
        {
            if (Directory.Exists(FolderNPath))
            {
                string command = String.Format("cacls \"{0}\" /t /e /g {1}:f", FolderNPath, UserOrGroup);
                string strResult = RunCmdLine(command);
                if (strResult.Contains("Invalid arguments."))
                    return false;
                else
                    return true;
            }
            return false;
        }
    }
}
