﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using Yaulw.Tools;

namespace Yaulw.Other
{
    /// <remarks>
    /// Easy StateManager to store and retrieve various Application States using an Enum
    /// <example>
    /// <code>
    /// internal enum State
    /// {
    ///    App_Started_bool,
    ///    App_ErrorsOccured_bool,     
    ///    SpecialMode_CommandLine_Mode_bool,
    /// }
    ///
    /// internal static StateM AppState = new StateM(typeof(State));
    /// </code>
    /// </example>
    /// </remarks>
    public class StateM : IDisposable
    {
        #region Private Members

        // Private Parsing Enums
        private Type _StateKeys = null;
        private StringDictionary _sdCurrentState = new StringDictionary(); // For State String Convertible Types
        private Dictionary<String, Object> _sdCurrentStateObj = new Dictionary<string, object>(); // For State Object Types
        private bool _disposed = false;
        private object _lock = new object();

        #endregion

        #region Construction

        /// <summary>
        /// The StateM needs to know what Keys to Get/Set
        /// </summary>
        /// <param name="Enum_StateKeys">Pass an Enum used to determine StateKeys</param>
        public StateM(Type Enum_StateKeys)
        {
            if (Enum_StateKeys.IsEnum)
            {
                _StateKeys = Enum_StateKeys;
            }
            else
                throw new ArgumentException("StateKeys must be an Enum");
        }

        /// <summary>
        /// Finalizer
        /// </summary>
        ~StateM()
        {
            Dispose(true);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// True if User Has passed in State Values
        /// </summary>
        public bool HasStates { get { return (_sdCurrentState.Count >= 1 || _sdCurrentStateObj.Count >= 1); } }

        #endregion

        #region Public Methods

        /// <summary>
        /// Main Entry Function to Retrieve an State Value
        /// </summary>
        /// <typeparam name="T">Should be a System Type like string, bool, int32, double, decimal, etc...</typeparam>
        /// <param name="stateKey">State Value you want to retrieve</param>
        /// <param name="DefaultValue">Default value to use if nothing was retrieved * Error occured *</param>
        /// <returns>value or default value, if not found</returns>
        public T GetStateValue<T>(Enum stateKey, T DefaultValue)
        {
            lock (_lock)
            {
                T RetVal = DefaultValue;
                if (ObjTool.IsOfTypeConvertibleToString<T>(DefaultValue))
                {
                    string Value = String.Empty;
                    if (GetStateValue(stateKey, out Value) && !String.IsNullOrEmpty(Value))
                        RetVal = ObjTool.ConvertStringToObj<T>(Value);
                }
                else
                {
                    object o = null;
                    if (GetStateValue(stateKey, out o) && (o != null))
                        RetVal = (T)o;
                }
                return RetVal;
            }
        }

        /// <summary>
        /// Main Entry Function to Set a State Value
        /// </summary>
        /// <typeparam name="T">Should be a System Type like string, bool, int32, double, decimal, etc...</typeparam>
        /// <param name="stateKey">State Value you want to set</param>
        /// <param name="Value">Value you want to set</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool SetStateValue<T>(Enum stateKey, T Value)
        {
            lock (_lock)
            {
                bool bSuccess = false;
                if (ObjTool.IsOfTypeConvertibleToString<T>(Value))
                {
                    string strValue = ObjTool.ConvertObjToString<T>(Value);
                    bSuccess = SetStateValue(stateKey, strValue);
                }
                else
                {
                    bSuccess = SetStateValue(stateKey, (object)Value);
                }
                return bSuccess;
            }
        }

        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Private GetStateValue Getter Function
        /// </summary>
        /// <param name="stateKey">pass in the State to look for</param>
        /// <param name="Value">Returns the value or String.Empty, if none</param>
        /// <returns>true if the State Value Exists, false otherwise</returns>
        private bool GetStateValue(Enum stateKey, out string Value)
        {
            lock (_lock)
            {
                Value = String.Empty;
                if (_sdCurrentState.ContainsKey(stateKey.ToString()))
                {
                    Value = _sdCurrentState[stateKey.ToString()];
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Private GetStateValue Getter Function
        /// </summary>
        /// <param name="stateKey">pass in the State to look for</param>
        /// <param name="o">Returns the object or null, if noone</param>
        /// <returns>true if the State Value Exists, false otherwise</returns>
        private bool GetStateValue(Enum stateKey, out object o)
        {
            lock (_lock)
            {
                o = null;
                if (_sdCurrentStateObj.ContainsKey(stateKey.ToString()))
                {
                    o = _sdCurrentStateObj[stateKey.ToString()];
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Private SetStateValue Setter Function
        /// </summary>
        /// <param name="stateKey">pass in the State to Set</param>
        /// <param name="Value">The Value to Set</param>
        /// <returns>true if the State Value was set, false otherwise</returns>
        private bool SetStateValue(Enum stateKey, string Value)
        {
            lock (_lock)
            {
                try
                {
                    _sdCurrentState[stateKey.ToString()] = Value;
                    return true;
                }
                catch (Exception) { /* ignore */ }
                return false;
            }
        }

        /// <summary>
        /// Private SetStateValue Setter Function
        /// </summary>
        /// <param name="stateKey">pass in the State to Set</param>
        /// <param name="o">The Object to Set</param>
        /// <returns>true if the State Value was set, false otherwise</returns>
        private bool SetStateValue(Enum stateKey, object o)
        {
            lock (_lock)
            {
                try
                {
                    _sdCurrentStateObj[stateKey.ToString()] = o;
                    return true;
                }
                catch (Exception) { /* ignore */ }
                return false;
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose the State Object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass
            // of this type implements a finalizer
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose the State Object
        /// </summary>
        /// <param name="disposing">true, if called from within</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    foreach (object o in _sdCurrentStateObj)
                    {
                        if (o is IDisposable)
                            ((IDisposable)o).Dispose();
                    }
                    _sdCurrentStateObj.Clear();
                }

                // Indicate that the instance has been disposed.                                
                _sdCurrentState = null;
                _sdCurrentStateObj = null;
                _disposed = true;
            }
        }

        #endregion
    }
}
