﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Yaulw.Other
{
    /// <remarks>
    /// Common Delegates * Useful for Dispatching *
    /// </remarks>
    public static class DelegateCollection
    {
        // 0 Params Functions
        public delegate void Void_Func();
        public delegate bool Bool_Func();
        public delegate int Int_Func();        
        public delegate object Obj_Func();
        public delegate DateTime DateTime_Func();

        // Void Ret - 1 Params Functions
        public delegate void Void_Param1_String_Func(string str1);
        public delegate void Void_Param1_Int_Func(int int1);
        public delegate void Void_Param1_DateTime_Func(DateTime dt1);
        public delegate void Void_Param1_Exception_Func(Exception ex1);

        // Bool Ret - 1 Params Functions
        public delegate bool Bool_Param1_String_Func(string str1);
        public delegate bool Bool_Param1_Window_Func(Window window);

        // String Ret - 1 Params Functions
        public delegate string String_Param1_Bool_Func(bool bool1);
        public delegate string String_Param1_String_Func(string str1);

    }
}
