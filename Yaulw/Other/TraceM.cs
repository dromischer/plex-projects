﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Threading = System.Threading;
using System.Diagnostics;

namespace Yaulw.Other
{
    /// <remarks>
    /// Wrapper Class arround Tracing, designed to measure performance.
    /// Use this class to measure performance arround your calls.
    /// <example>
    /// TraceM.TraceBegin()
    /// ... Some Code....
    /// ... Some More Code...
    /// TraceM.TraceEnd("Custom Message");
    /// </example>
    /// </remarks>
    public static class TraceM
    {
        #region Private Statics

        private static Dictionary<uint, Stack<DateTime>> s_ThreadTraceBeginTSMap = new Dictionary<uint, Stack<DateTime>>();
        public static bool EnableTracing { get; set; }

        #endregion

        #region Construction

        /// <summary>
        /// Construction * Tracing by default is disabled *
        /// </summary>
        static TraceM()
        {
            EnableTracing = false;
        }

        #endregion

        #region Public Statics

        /// <summary>
        /// Call this to Start the Performance Trace
        /// </summary>   
        public static void TraceBegin()
        {
            if (EnableTracing)
            {
                uint curThreadId = (uint)Threading.Thread.CurrentThread.ManagedThreadId;
                if (!s_ThreadTraceBeginTSMap.ContainsKey(curThreadId))
                    s_ThreadTraceBeginTSMap[curThreadId] = new Stack<DateTime>();

                s_ThreadTraceBeginTSMap[curThreadId].Push(DateTime.Now);
            }
        }

        /// <summary>
        /// Us this to End the Performance Trace
        /// </summary>        
        /// <param name="CustomMessage">Custom Message you want displayed in the Trace Window</param>
        public static void TraceEnd(string CustomMessage = "")
        {
            if (EnableTracing)
            {
                uint curThreadId = (uint)Threading.Thread.CurrentThread.ManagedThreadId;
                if (s_ThreadTraceBeginTSMap.ContainsKey(curThreadId))
                {
                    DateTime orgTime = s_ThreadTraceBeginTSMap[curThreadId].Pop();
                    TimeSpan ts = DateTime.Now - orgTime;
                    Trace.WriteLine((CustomMessage + " (Time Taken in ms " + ts.TotalMilliseconds.ToString() + ")"));
                }
            }
        }

        #endregion
    }
}
