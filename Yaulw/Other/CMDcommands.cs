﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yaulw.Other
{
    /// <remarks>
    /// Commonly used Batch/CMD command Line Command
    /// </remarks>
    public static class CMDcommands
    {
        #region CMDCommands

        /// <summary>
        /// map a credential to a network path
        /// </summary>
        /// <param name="path">network path</param>
        /// <param name="user">user</param>
        /// <param name="pwd">password</param>
        /// <returns>Command-Line Command</returns>
        static public string net_use(string path, string user, string pwd)
        {
            return ("net use " + path + " /User:" + user + " " + pwd);
        }

        /// <summary>
        /// map a credential and drive letter to a network path
        /// </summary>
        /// <param name="path">network path</param>
        /// <param name="driveletter">drive letter to map</param>
        /// <param name="user">user</param>
        /// <param name="pwd">password</param>
        /// <param name="path"></param>       
        /// <returns>Command-Line Command</returns>
        static public string net_use_persist(string path, string driveletter, string user, string pwd)
        {
            return ("net use " + driveletter + ": " + path + " /User:" + user + " " + pwd + " PERSISTENT:YES");
        }

        /// <summary>
        /// Delete a Network Credential from a network path
        /// </summary>
        /// <param name="path">network path</param>
        /// <returns>Command-Line Command</returns>
        static public string net_delete(string path)
        {
            return ("net use /delete " + path);
        }

        /// <summary>
        /// Delete a Drive Letter and Network Credential from a network path
        /// </summary>
        /// <param name="driveletter"></param>
        /// <returns>Command-Line Command</returns>
        static public string net_delete_persist(string driveletter)
        {
            return ("net use /delete " + driveletter + ":");
        }

        #endregion

        #region PSTools

        /// <remarks>
        /// Commands for System Internals PSExec Tools
        /// </remarks>
        public static class PSTools
        {
            /// <summary>
            /// PSExec is a System Internals Remote Execution CommandLine tool.
            /// Use this to execute a .bat/.cmd file on the other computer
            /// </summary>
            /// <param name="computer">Name of computer</param>
            /// <param name="file">.bat or .cmd file to execute</param>
            /// <param name="user">user</param>
            /// <param name="pwd">password</param>
            /// <param name="interactive">true to allow remote process to interact with desktop</param>
            /// <returns>Command-Line Command</returns>
            static public string ps_exec(string computer, string file, string user, string pwd, bool interactive)
            {
                string Show = "";
                if (interactive)
                    Show = "-i ";

                if (file.ToLower().Contains(".bat") || file.ToLower().Contains(".cmd"))
                {
                    // copy .bat or .cmd file over
                    return (@"psexec.exe \\" + computer + " -u " + user + " -p " + pwd + " -c -f -e -d " + Show + "/accepteula " + '\"' + file + '\"');
                }
                else
                {
                    // doesn't copy over, executes command from location, don't wait for process to terminate
                    return (@"psexec.exe \\" + computer + " -u " + user + " -p " + pwd + " -d -e " + Show + "/accepteula " + '\"' + file + '\"');
                }

            }
        }

        #endregion
    }
}
