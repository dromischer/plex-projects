﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Yaulw.Win32;

namespace Yaulw.Other
{
    #region OSInfo Enums

    /// <summary>
    /// Windows OS Types
    /// </summary>
    public enum OStype
    {
        WIN_2000 = 4,
        WIN_2003 = 5,
        WIN_VISTA = 6,
        WIN_7 = 7,
        WIN_8 = 8,
        WIN_UNKNOWN = -1,
    }

    #endregion

    /// <remarks>
    /// This class provides OS Specific and Version information to the caller
    /// </remarks>
    public static class OSInfo
    {
        #region Public Properties

        /// <summary>
        /// Get OSType and Version Information 
        /// </summary>
        public static int Major { get; private set; }
        public static int Minor { get; private set; }
        public static OStype OS { get; private set; }

        /// <summary>
        /// Set Supported Version for an Application
        /// </summary>
        public static OStype MinVersionSupported { get; set; }
        public static OStype MaxVersionSupported { get; set; }

        /// <summary>
        /// Is this OS Version Supported * Min/Max Should be set by Application *
        /// </summary>
        public static bool IsSupportedOS
        { 
            get 
            {
                if (MinVersionSupported != OStype.WIN_UNKNOWN && MaxVersionSupported != OStype.WIN_UNKNOWN)
                    return ((int) OS >= (int)MinVersionSupported) && ((int) OS <= (int)MaxVersionSupported);
                else
                    return true;
            } 
        }

        /// <summary>
        /// Is Vista/Win7's Aero Enabled
        /// </summary>
        public static bool IsAero
        {
            get
            {
                if (((int)OS >= (int)OStype.WIN_VISTA) || ((int)OS <= (int)OStype.WIN_7))
                {
                    bool IsAero = false;
                    if (uxDwm.DwmIsCompositionEnabled(ref IsAero) < 0)
                        return false;
                    else
                        return IsAero;
                }
                return false;
            }
        }

        /// <summary>
        /// Is WindowsXP/2003 Composition Enabled
        /// </summary>
        public static bool IsXPComposition
        {
            get
            {
                if ((int)OS == (int)OStype.WIN_2003)
                    return uxDwm.IsAppThemed();
                return false;
            }
        }

        #endregion

        #region Construction

        /// <summary>
        /// Constructor
        /// </summary>
        static OSInfo()
        {
            // Get the OS Version
            Major = System.Environment.OSVersion.Version.Major;
            Minor = System.Environment.OSVersion.Version.Minor;

            // Parse the OS Version
            try { OSInfo.OS = (OStype)Enum.Parse(typeof(OStype), Major.ToString()); }
            catch (Exception) { OSInfo.OS = OStype.WIN_UNKNOWN; }

            // Set Min/Max to Unknown
            MinVersionSupported = OStype.WIN_UNKNOWN;
            MaxVersionSupported = OStype.WIN_UNKNOWN;
        }

        #endregion
    }
}
