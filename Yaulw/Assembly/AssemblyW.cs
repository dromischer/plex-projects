﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Refl = System.Reflection;
using System.Diagnostics;
using System.Reflection;

namespace Yaulw.Assembly
{
    /// <remarks>
    /// Useful Functions for Retrieving General Assembly Information
    /// </remarks>
    public static class AssemblyW
    {
        #region Assembly State/Type

        /// <summary>
        /// Assembly State/Type
        /// </summary>
        public enum AssemblyST
        {
            Calling,
            Entry,
            Executing
        }

        /// <summary>
        /// Internal Helper Function to Get the Appropriate Assembly State/Type
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve</param>
        /// <exception cref="ArgumentException">Thrown if invalist AssemblyST is passed in</exception>
        /// <returns>Returns the Specified State/Type Assembly</returns>
        private static Refl.Assembly GetAssembly(AssemblyST st)
        {
            switch (st)
            {
                case AssemblyST.Calling:
                    return Refl.Assembly.GetCallingAssembly();
                case AssemblyST.Entry:
                    return Refl.Assembly.GetEntryAssembly();
                case AssemblyST.Executing:
                    return Refl.Assembly.GetExecutingAssembly();
            }
            throw (new ArgumentException("Invalid AssemblyST"));
        }

        #endregion

        #region Common Assembly Properties

        /// <summary>
        /// Returns the Name of the Assembly
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
        /// <returns>Returns 'just the Name' of the Assembly</returns>
        public static string GetAssemblyName(AssemblyST st)
        {
            string[] curAsmName = GetAssembly(st).FullName.Split(',');
            return curAsmName[0];
        }

        /// <summary>
        /// Returns the Name of the Assembly
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
        /// <returns>Returns 'just the Name' of the Assembly</returns>
        public static string GetAssemblyName(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            string[] curAsmName = asm.FullName.Split(',');
            return curAsmName[0];
        }

        /// <summary>
        /// Returns the Assembly Title
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
        /// <returns>Returns the Assembly Title, as specified in AssemblyInfo</returns>
        public static string GetAssemblyTitle(AssemblyST st)
        {            
            object[] attributes = GetAssembly(st).GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                return titleAttribute.Title;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Assembly Title
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
        /// <returns>Returns the Assembly Title, as specified in AssemblyInfo</returns>
        public static string GetAssemblyTitle(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            object[] attributes = asm.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                return titleAttribute.Title;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Description of the Assembly
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
        /// <returns>Returns the Assembly Description, as specified in AssemblyInfo</returns>
        public static string GetAssemblyDescription(AssemblyST st)
        {
            object[] attributes = GetAssembly(st).GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyDescriptionAttribute descriptionAttribute = (AssemblyDescriptionAttribute)attributes[0];
                return descriptionAttribute.Description;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Description of the Assembly
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
        /// <returns>Returns the Assembly Description, as specified in AssemblyInfo</returns>
        public static string GetAssemblyDescription(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            object[] attributes = asm.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyDescriptionAttribute descriptionAttribute = (AssemblyDescriptionAttribute)attributes[0];
                return descriptionAttribute.Description;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Company Name of the Assembly
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
        /// <returns>Returns the Assembly Company, as specified in AssemblyInfo</returns>
        public static string GetAssemblyCompany(AssemblyST st)
        {
            object[] attributes = GetAssembly(st).GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyCompanyAttribute companyAttribute = (AssemblyCompanyAttribute)attributes[0];
                return companyAttribute.Company;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Company Name of the Assembly
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
        /// <returns>Returns the Assembly Company, as specified in AssemblyInfo</returns>
        public static string GetAssemblyCompany(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            object[] attributes = asm.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyCompanyAttribute companyAttribute = (AssemblyCompanyAttribute)attributes[0];
                return companyAttribute.Company;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Product Name of the Assembly
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
        /// <returns>Returns the Assembly Product, as specified in AssemblyInfo</returns>
        public static string GetAssemblyProductName(AssemblyST st)
        {
            object[] attributes = GetAssembly(st).GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyProductAttribute productAttribute = (AssemblyProductAttribute)attributes[0];
                return productAttribute.Product;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Product Name of the Assembly
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
        /// <returns>Returns the Assembly Product, as specified in AssemblyInfo</returns>
        public static string GetAssemblyProductName(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            object[] attributes = asm.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyProductAttribute productAttribute = (AssemblyProductAttribute)attributes[0];
                return productAttribute.Product;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Copyright Information of the Assembly
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
        /// <returns>Returns the Assembly Copyright, as specified in AssemblyInfo</returns>
        public static string GetAssemblyCopyright(AssemblyST st)
        {
            object[] attributes = GetAssembly(st).GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyCopyrightAttribute copyrightAttribute = (AssemblyCopyrightAttribute)attributes[0];
                return copyrightAttribute.Copyright;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Copyright Information of the Assembly
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
        /// <returns>Returns the Assembly Copyright, as specified in AssemblyInfo</returns>
        public static string GetAssemblyCopyright(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            object[] attributes = asm.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            if (attributes.Length > 0)
            {
                AssemblyCopyrightAttribute copyrightAttribute = (AssemblyCopyrightAttribute)attributes[0];
                return copyrightAttribute.Copyright;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns the Assembly Version
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve</param>
        /// <exception cref="ArgumentException">Thrown if invalist AssemblyST is passed in</exception>
        /// <returns>Returns the Assembly Version, as specified in AssemblyInfo</returns>
        public static Version GetAssemblyVersion(AssemblyST st)
        {
            return SpecializedAssemblyInfo.GetAssemblyNameObj(st).Version;
        }

        /// <summary>
        /// Returns the Assembly Version
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalist Assembly is passed in</exception>
        /// <returns>Returns the Assembly Version, as specified in AssemblyInfo</returns>
        public static Version GetAssemblyVersion(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            return SpecializedAssemblyInfo.GetAssemblyNameObj(asm).Version;
        }

        /// <summary>
        /// Returns the Assembly File Version
        /// </summary>
        /// <param name="st">State/Type of Assembly to Retrieve</param>
        /// <exception cref="ArgumentException">Thrown if invalist AssemblyST is passed in</exception>
        /// <returns>Returns the File Version, as specified by the File</returns>
        public static FileVersionInfo GetAssemblyFileVersion(AssemblyST st)
        {
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(SpecializedAssemblyInfo.GetAssemblyFileNameNPath(st));
            return fvi;
        }

        /// <summary>
        /// Returns the Assembly File Version
        /// </summary>
        /// <param name="asm">Assembly to Retrieve Info for</param>
        /// <exception cref="ArgumentException">Thrown if invalist Assembly is passed in</exception>
        /// <returns>Returns the File Version, as specified by the File</returns>
        public static FileVersionInfo GetAssemblyFileVersion(Refl.Assembly asm)
        {
            if (asm == null)
                throw new ArgumentException("Invalid Assembly");

            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(SpecializedAssemblyInfo.GetAssemblyFileNameNPath(asm));
            return fvi;
        }

        #endregion

        #region Specialized Assembly Information

        /// <remarks>
        /// Useful Functions for Retrieving Specialized Assembly Information
        /// </remarks>
        public class SpecializedAssemblyInfo
        {
            /// <summary>
            /// Returns the Assembly Name Object from the Assembly
            /// </summary>
            /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
            /// <returns>AssemblyName object for the specified assembly</returns>
            public static Refl.AssemblyName GetAssemblyNameObj(AssemblyST st)
            {
                return GetAssembly(st).GetName();
            }

            /// <summary>
            /// Returns the Assembly Name Object from the Assembly
            /// </summary>
            /// <param name="asm">Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
            /// <returns>AssemblyName object for the specified assembly</returns>
            public static Refl.AssemblyName GetAssemblyNameObj(Refl.Assembly asm)
            {
                if (asm == null)
                    throw new ArgumentException("Invalid Assembly");

                return asm.GetName();
            }

            /// <summary>
            /// Returns the Exact File Name and Path of the Assembly
            /// </summary>
            /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
            /// <returns>Full File Name and Path</returns>
            public static string GetAssemblyFileNameNPath(AssemblyST st)
            {
                Refl.Assembly asm = GetAssembly(st);
                string curAsmName = asm.Location;
                return curAsmName;
            }

            /// <summary>
            /// Returns the Exact File Name and Path of the Assembly
            /// </summary>
            /// <param name="asm">Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
            /// <returns>Full File Name and Path</returns>
            public static string GetAssemblyFileNameNPath(Refl.Assembly asm)
            {
                if (asm == null)
                    throw new ArgumentException("Invalid Assembly");

                string curAsmName = asm.Location;
                return curAsmName;
            }

            /// <summary>
            /// Returns the File Name (without Extension) of the Assembly
            /// </summary>
            /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
            /// <returns>File Name without extension</returns>
            public static string GetAssemblyFileName(AssemblyST st)
            {
                string curAsmName = Path.GetFileNameWithoutExtension(GetAssembly(st).ManifestModule.Name);
                return curAsmName;
            }

            /// <summary>
            /// Returns the File Name (without Extension) of the Assembly
            /// </summary>
            /// <param name="asm">Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
            /// <returns>File Name without extension</returns>
            public static string GetAssemblyFileName(Refl.Assembly asm)
            {
                if (asm == null)
                    throw new ArgumentException("Invalid Assembly");

                string curAsmName = Path.GetFileNameWithoutExtension(asm.ManifestModule.Name);
                return curAsmName;
            }

            /// <summary>
            /// Returns the Path of the Assembly
            /// </summary>
            /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
            /// <returns>Path</returns>
            public static string GetAssemblyPath(AssemblyST st)
            {
                Refl.Assembly asm = GetAssembly(st);
                string curAsmName = Path.GetDirectoryName(asm.Location);                
                return curAsmName;
            }

            /// <summary>
            /// Returns the Path of the Assembly
            /// </summary>
            /// <param name="asm">Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
            /// <returns>Path</returns>
            public static string GetAssemblyPath(Refl.Assembly asm)
            {
                if (asm == null)
                    throw new ArgumentException("Invalid Assembly");

                string curAsmName = Path.GetDirectoryName(asm.Location);
                return curAsmName;
            }

            /// <summary>
            /// Returns the File Name (with Extension) of the Assembly
            /// </summary>
            /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
            /// <returns>File Name with extension</returns>
            public static string GetAssemblyFileNameWithExtension(AssemblyST st)
            {
                string curAsmName = Path.GetFileName(GetAssembly(st).ManifestModule.Name);
                return curAsmName;
            }

            /// <summary>
            /// Returns the File Name (with Extension) of the Assembly
            /// </summary>
            /// <param name="asm">Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
            /// <returns>File Name with extension</returns>
            public static string GetAssemblyFileNameWithExtension(Refl.Assembly asm)
            {
                if (asm == null)
                    throw new ArgumentException("Invalid Assembly");

                string curAsmName = Path.GetFileName(asm.ManifestModule.Name);
                return curAsmName;
            }

            /// <summary>
            /// Returns all the Resource names inside the assembly
            /// </summary>
            /// <param name="st">State/Type of Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid AssemblyST is passed in</exception>
            /// <returns>Returns all resource names defined in the assembly</returns>
            public static string[] GetAssemblyResourceNames(AssemblyST st)
            {
                string[] resources = GetAssembly(st).GetManifestResourceNames();
                return resources;
            }

            /// <summary>
            /// Returns all the Resource names inside the assembly
            /// </summary>
            /// <param name="asm">Assembly to Retrieve Info for</param>
            /// <exception cref="ArgumentException">Thrown if invalid Assembly is passed in</exception>
            /// <returns>Returns all resource names defined in the assembly</returns>
            public static string[] GetAssemblyResourceNames(Refl.Assembly asm)
            {
                if (asm == null)
                    throw new ArgumentException("Invalid Assembly");

                string[] resources = asm.GetManifestResourceNames();
                return resources;
            }
        }

        #endregion
    }
}
