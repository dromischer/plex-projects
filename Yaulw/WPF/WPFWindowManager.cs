﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using Yaulw.Thread;
using System.Windows;
using Yaulw.Other;
using System.Collections;
using System.Windows.Interop;

namespace Yaulw.WPF
{

    /// <summary>
    /// Responsible for Managing WPFWindows
    /// </summary>
    public class WPFWindowManager : WindowManager
    {
        #region Construction

        /// <summary>
        /// WPFWindow Manager Contructor
        /// </summary>
        /// <param name="dispatcher">Thread Dispatcher Object</param>
        /// <param name="windowTypes">Window Types this WindowManager is managing</param>
        /// <param name="PerformanceLoad">true, to load performance enabled windows upon construction</param>
        public WPFWindowManager(Dispatcher dispatcher, WindowType[] windowTypes, bool PerformanceLoad)
            : base(dispatcher, windowTypes, PerformanceLoad)
        {
            // Here i can load Show/hide all windows that are performance loaded * To further load stuff WPF related *
            if (PerformanceLoad)
            {
                foreach (WindowType type in windowTypes)
                {
                    List<object> objects = GetObjects(type);
                    if (objects.Count == 1 && type.IsPerformanceLoaded)
                    {
                        // Show/Hide Window!
                    }
                }
            }
        }

        #endregion

        #region Public Overriden Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="windowKey">Window to run action on</param>
        /// <param name="attributes">Window Attributes to use</param>
        /// <param name="tag">object to pass into Window</param>
        /// <returns>the tag object from the window</returns>
        public override object ShowWindow(Enum windowKey, WindowAttributes attributes, object tag)
        {
            DelegateCollection.Obj_Func func = delegate()
            {
                object o = CreateInstance(windowKey);
                if (AddObject(windowKey, o))
                {
                    Window wpfWin = (Window)o;
                    wpfWin.Tag = tag;

                    // Set Attributes
                    SetAttributesOnWindow(wpfWin, attributes);

                    // Show
                    wpfWin.Show();
                    object retVal = wpfWin.Tag;
                    wpfWin = null;
                    return retVal;
                }
                return null;
            };

            if (Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                return func();
            }
            else
            {
                object[] parameters = new object[] { windowKey, attributes, tag };
                return Dispatcher.Invoke((ShowDelegate)ShowWindow, DispatcherPriority.Normal, parameters);
            }
        }

        /// <summary>
        /// Show the WPF Window as a Dialog
        /// </summary>
        /// <param name="windowKey">Window to run action on</param>
        /// <param name="attributes">Window Attributes to use</param>
        /// <param name="tag">object to pass into Window</param>
        /// <returns>the tag object from the window</returns>
        public override object ShowDialog(Enum windowKey, WindowAttributes attributes, object tag)
        {
            DelegateCollection.Obj_Func func = delegate()
            {
                object o = CreateInstance(windowKey);
                if (AddObject(windowKey, o))
                {
                    Window wpfWin = (Window)o;
                    wpfWin.Tag = tag;

                    // Set Attributes
                    SetAttributesOnWindow(wpfWin, attributes);

                    // ShowDialog()
                    wpfWin.ShowDialog();
                    object retVal = wpfWin.Tag;
                    DeleteObject(windowKey);
                    wpfWin = null;
                    return retVal;
                }
                return null;
            };

            if (Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                return func();
            }
            else
            {
                object[] parameters = new object[] { windowKey, attributes, tag };
                return Dispatcher.Invoke((ShowDelegate)ShowDialog, DispatcherPriority.Normal, parameters);
            }
        }

        /// <summary>
        /// Run any generic action on a window
        /// </summary>
        /// <param name="windowKey">Window to run action on</param>
        /// <param name="action">action to perform on the windo</param>
        public override bool RunAction(Enum windowKey, DelegateCollection.Bool_Param1_Window_Func action)
        {
            if (Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                List<object> objects = GetObjects(windowKey);
                object o = null;
                if (objects != null)
                    o = objects.Last();
                if (o != null && o is Window && action != null)
                    return action((Window)o);
                else
                    return action(null);
            }
            else
            {
                object[] parameters = new object[] { windowKey, action };
                return (bool)Dispatcher.Invoke((ActionDelegate)RunAction, DispatcherPriority.Normal, parameters);
            }
        }

        /// <summary>
        /// Hide the Specified Window
        /// </summary>
        /// <param name="windowKey">Window to run action on</param>
        /// <returns>true, if successful, false otherwise</returns>
        public override bool HideWindow(Enum windowKey)
        {
            DelegateCollection.Bool_Param1_Window_Func func = delegate(Window wpfWin)
            {
                wpfWin.Hide();
                return true;
            };
            return RunAction(windowKey, func);
        }

        /// <summary>
        /// Close the Specified Window
        /// </summary>
        /// <param name="windowKey">Window to run action on</param>
        /// <returns>true, if successful, false otherwise</returns>
        public override bool CloseWindow(Enum windowKey)
        {
            DelegateCollection.Bool_Param1_Window_Func func = delegate(Window wpfWin)
            {
                DeleteObject(windowKey);
                return true;
            };
            return RunAction(windowKey, func);
        }

        /// <summary>
        /// Dispose of this manager Properly
        /// </summary>
        public override void Dispose()
        {
            // We must call the base's ClearAllObjects()
            DelegateCollection.Bool_Param1_Window_Func func = delegate(Window wpfWin)
            {
                ClearAllObjects();
                return true;
            };
            RunAction(null, func);
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// Set the Window Attributes as specifed by the WindowAttributes type
        /// </summary>
        /// <param name="attributes">Attributes to set</param>
        /// <param name="wpfWin">Window to set them on</param>
        private void SetAttributesOnWindow(Window wpfWin, WindowAttributes attributes)
        {
            // Set Attributes
            if (attributes != null && wpfWin != null)
            {
                WindowInteropHelper InteropHelper = new WindowInteropHelper(wpfWin);
                if (attributes.hWndOwner != IntPtr.Zero)
                    InteropHelper.Owner = (IntPtr)attributes.hWndOwner;
                if (attributes.Height != 0)
                    wpfWin.Height = attributes.Height;
                if (attributes.Width != 0)
                    wpfWin.Width = attributes.Width;
                if (attributes.Left != int.MinValue)
                    wpfWin.Left = attributes.Left;
                if (attributes.Top != int.MinValue)
                    wpfWin.Top = attributes.Top;
                if (!String.IsNullOrEmpty(attributes.Title))
                    wpfWin.Title = attributes.Title;
            }
        }

        #endregion
    }

}
