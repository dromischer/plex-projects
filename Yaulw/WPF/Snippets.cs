﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Interop;

namespace Yaulw.WPF
{
    /// <remarks>
    /// Useful WPF Snippets
    /// </remarks>
    static public class Snippets
    {
        /// <summary>
        /// Useful to create a simple wrapping tooltip for a wpf control
        /// </summary>
        /// <param name="strToolTipContent"></param>
        /// <param name="height">Set to 0 to leave default, else set to 0> to define max height</param>
        /// <param name="width">Set to 0 to leave default, else set to 0> to define max width</param>
        /// <returns>a ToolTip Object to assign to a control</returns>
        public static ToolTip CreateNewStringToolTip(string strToolTipContent, int height, int width)
        {
            // Create the TextBlock
            TextBlock tx = new TextBlock();
            if (height > 0)
                tx.Height = height;
            if (width > 0)
                tx.Width = width;
            tx.TextWrapping = TextWrapping.Wrap;
            tx.Text = strToolTipContent;

            // Create the ToolTip with the TextBlock inside
            ToolTip tt = new ToolTip();
            tt.Content = strToolTipContent;
            return tt;
        }

        /// <summary>
        /// Useful if you want to know if the Mouse cursor is over the specified Wpf Window
        /// (Uses windowsforms to do the calculations)
        /// </summary>
        /// <returns>true if the mouse cursor is over the specified form window, false otherwise</returns>
        public static bool IsMouseCursorOverWPFormWindow(Window wpfWindow)
        {
            if (wpfWindow != null)
            {
                // Get the Point location of the mouse cursor
                System.Drawing.Point point = System.Windows.Forms.Cursor.Position;

                // Get the rect for the Navigation Form
                Point wpfFormLeftTopPoint = new Point(wpfWindow.Left, wpfWindow.Top);
                Size wpfFormSize = new Size(wpfWindow.Width, wpfWindow.Height);
                Rect rect = new Rect(wpfFormLeftTopPoint, wpfFormSize);

                // Return if the Mouse is over the Navigation Form
                return (rect.Contains((double)point.X, (double)point.Y));
            }
            return false;
        }

        /// <summary>
        /// For Dynamically Creating Text ComboBox Items
        /// </summary>
        /// <param name="strContent">Text Content to Display</param>
        /// <param name="tag">object to tag to the menu item</param>
        /// <returns></returns>
        public static ComboBoxItem CreateAComboBoxItem(string strContent, object tag)
        {
            if (!String.IsNullOrEmpty(strContent) && (tag != null))
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = strContent;
                item.Tag = tag;
                return item;
            }
            return null;
        }

        /// <summary>
        /// For dynamically creating Image sources, this function helps you to create an image out of
        /// a Resource Uri. You can then assign this image to a 'Source' property on an image wpf object
        /// </summary>
        /// <param name="strRelativeResourceUri">a valid relative Resource Uri String to an image</param>
        /// <param name="ImageHeight">the output image's desired height</param>
        /// <param name="ImageWidth">the output image's desired width</param>
        /// <returns></returns>
        public static Image CreateWPFImageFromRelativeResourceUri(string strRelativeResourceUri, int ImageHeight, int ImageWidth)
        {
            if (!String.IsNullOrEmpty(strRelativeResourceUri))
            {
                Image myImage = new Image();
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(strRelativeResourceUri, UriKind.Relative);
                image.EndInit();
                myImage.Stretch = Stretch.Fill;
                myImage.Height = ImageHeight;
                myImage.Width = ImageWidth;
                myImage.Source = image;
                return myImage;
            }
            return null;
        }

        /// <summary>
        /// Uses the InteropHelper to get the Handle of a WPF Window
        /// </summary>
        /// <param name="window">a WPF Window</param>
        /// <returns>an hWnd for the specified WPF Window</returns>
        public static IntPtr GetHandleForWPFWindow(Window window)
        {            
            WindowInteropHelper InteropHelper = new WindowInteropHelper(window);
            if (InteropHelper.Handle != IntPtr.Zero)
                return InteropHelper.Handle;
#if NET4
            else
                return InteropHelper.EnsureHandle();
#else
            else
                return IntPtr.Zero;
#endif
        }

        /// <summary>
        /// Uses the InteropHelper to set the Owner Property of a WPF Window
        /// </summary>
        /// <param name="window">a WPF Window</param>
        /// <param name="hOwnerWnd">Owner hWnd Handle to set for WPF Window</param>
        public static void SetOwnerForWPFWindow(Window window, IntPtr hOwnerWnd)
        {
            WindowInteropHelper InteropHelper = new WindowInteropHelper(window);
            InteropHelper.Owner = hOwnerWnd;            
        }

        /// <summary>
        /// Adds the Specified Message Hook the the specified WPF Window.
        /// Ensures that the WPF Window exists (must be loaded with valid hWnd).
        /// </summary>
        /// <param name="window">a WPF Window</param>
        /// <param name="hook">a message hook function</param>
        public static void SetMessageHookForWPFWindow(Window window, HwndSourceHook hook)
        {            
            WindowInteropHelper interop = new WindowInteropHelper(window);
#if NET4
            HwndSource source = HwndSource.FromHwnd(interop.EnsureHandle());
#else
            HwndSource source = HwndSource.FromHwnd(interop.Handle);
#endif
            source.AddHook(hook);            
        }
    }
}
