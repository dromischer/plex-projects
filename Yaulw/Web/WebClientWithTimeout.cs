﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Yaulw.Web
{
    /// <remarks>
    /// 
    /// </remarks>
    public class WebClientWithTimeout : WebClient
    {
        /// <summary>
        /// 
        /// </summary>
        public int Timeout { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeout"></param>
        public WebClientWithTimeout(int timeout)
        {
            this.Timeout = timeout;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri address)
        {
            var result = base.GetWebRequest(address);
            result.Timeout = this.Timeout;
            return result;
        }



    }
}
