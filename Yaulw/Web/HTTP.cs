﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Yaulw.Web
{
    public class HTTP
    {


        /// <summary>
        ///  urlCreated.Text = Url;
        ///  var toPost = Url.Split('?');
        ///  var result = HttpPost(toPost[0], toPost[1]);
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="parameters"></param>
        /// <returns>String.Empty if error occured, non-empty string otherwise</returns>
        string HttpPost(string uri, string parameters)
        {
            var cookies = new CookieContainer();
            var webRequest = (HttpWebRequest)WebRequest.Create(uri);

            webRequest.CookieContainer = cookies;
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(parameters);
            Stream os = null;
            try
            {
                webRequest.ContentLength = bytes.Length;   //Count bytes to send
                os = webRequest.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);         //Send it
            }
            catch (Exception) { /* ignore */ }
            finally
            {
                if (os != null)
                {
                    os.Close();
                }
            }

            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch (Exception) { /* ignore */ }
            return String.Empty;
        } 
    }
}
