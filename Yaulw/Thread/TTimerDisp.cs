﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Threading;
using WinThread = System.Threading;

namespace Yaulw.Thread
{
    /// <remarks>
    /// Wrapper class around Timer Object * Uses Dispatcher *
    /// </remarks>
    public class TTimerDisp : IDisposable
    {
        #region Private Members

        private Timer _Timer = new Timer();
        private bool _disposed = false;
        private Dispatcher _Dispatcher = null;
        private ElapsedEventHandler _DispatchedElapsedEvent = null;

        #endregion

        #region Construction

        /// <summary>
        /// Creates a new Multi-threaded System.Timer that uses the Dispatcher to Fire the Event
        /// </summary>
        /// <param name="ElapsedHandler">Event Handler for Timer</param>
        /// <param name="IntervalMiliseconds">Interval in Miliseconds</param>
        /// <param name="StartEnabled">True to start the timer upon creation, false otherwise</param>
        /// <returns>A Timer Object, which should be Disposed by Caller</returns>
        public TTimerDisp(ElapsedEventHandler ElapsedHandler, int IntervalMiliseconds = 1000, bool StartEnabled = false)        
        {
            if (ElapsedHandler != null)
            {                
                _Timer = new System.Timers.Timer();

                // The Primary Dispatcher thread is the thread that called us
                _Dispatcher = Dispatcher.CurrentDispatcher;
                _DispatchedElapsedEvent = ElapsedHandler;
                _Timer.Elapsed += new ElapsedEventHandler(_Timer_Elapsed);
                
                // Set the Interval / start
                _Timer.Interval = IntervalMiliseconds;
                _Timer.Enabled = StartEnabled;
                if (StartEnabled)
                    _Timer.Start();

                // Keep the timer alive 
                GC.KeepAlive(_Timer);
            }
        }

        /// <summary>
        /// Finalizer
        /// </summary>
        ~TTimerDisp()
        {
            Dispose(true);
        }

        #endregion

        #region Private Helpers

        /// <summary>
        ///  For Dispatching the Event to the Primary Dispatcher Thread
        /// </summary>        
        private void _Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            object[] param_s = new object[] { sender, e };
            _Dispatcher.Invoke((ElapsedEventHandler)_DispatchedElapsedEvent, param_s);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Manually Start the Timer
        /// </summary>
        public void Start()
        {
            Stop(); // First Stop(), an existing Timer
            _Timer.Enabled = true;
            _Timer.Start();
        }

        /// <summary>
        /// Manually Start the Timer at a new Interval
        /// </summary>
        /// <param name="IntervalMiliseconds">Interval in Miliseconds</param>
        public void Start(uint IntervalMiliseconds)
        {
            Stop(); // First Stop(), an existing Timer
            _Timer.Interval = IntervalMiliseconds;
            _Timer.Enabled = true;
            _Timer.Start(); 
        }

        /// <summary>
        /// Manually Start the Timer at a new Interval
        /// </summary>
        /// <param name="tsInterval">Interval as a TimeSpan</param>
        public void Start(TimeSpan tsInterval)
        {
            Stop(); // First Stop(), an existing Timer
            _Timer.Interval = tsInterval.TotalMilliseconds;
            _Timer.Enabled = true;
            _Timer.Start();
        }

        /// <summary>
        /// Manually Stop the Timer
        /// </summary>
        public void Stop()
        {
            _Timer.Enabled = false;
            _Timer.Stop();
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose the Timer Object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass
            // of this type implements a finalizer
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose the Timer Object
        /// </summary>
        /// <param name="disposing">true, if called from within</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_Timer != null)
                        _Timer.Dispose();
                }

                // Indicate that the instance has been disposed.
                _Timer = null;
                _disposed = true;
            }
        }

        #endregion
    }
}
