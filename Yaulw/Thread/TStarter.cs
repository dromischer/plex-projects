﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Specialized;

namespace Yaulw.Thread
{
    /// <remarks>
    /// Used to Manage/Launch Threads In an Application
    /// </remarks>
    public static class TStarter
    {
        #region Public Delegates

        /// <summary>
        /// Thread Delegate with a single Object as a parameter
        /// </summary>
        /// <param name="o">object to be passed to Delegate</param>
        public delegate void ParameterizedThreadMethod(object o);

        /// <summary>
        /// No Parameter Thread Delegate
        /// </summary>
        public delegate void ThreadMethod();

        #endregion

        #region Private Static Members

        private static Dictionary<string, System.Threading.Thread> _StartedNamedThreads = new Dictionary<string, System.Threading.Thread>();

        #endregion

        #region Public Statics

        /// <summary>
        /// Starts a Parameterized thread
        /// </summary>
        /// <param name="method">specifcy a method to invoke</param>
        /// <param name="parameter">specify a parameter to pass to the method</param>
        /// <param name="threadName">If you specify a ThreadName, it must be Unique to the Application</param>
        /// <param name="Apartment">ApartmentState</param>
        /// <param name="IsBackground">Is Background Thread</param>
        /// <param name="priority">set thread priority</param>
        /// <returns>a thread object if successful, null otherwise</returns>
        public static System.Threading.Thread StartParameterizedThread(ParameterizedThreadMethod method, object parameter, string threadName = "", ApartmentState Apartment = ApartmentState.MTA, bool IsBackground = false, ThreadPriority priority = ThreadPriority.Normal)
        {
            // If a threadName is defined, make sure that it is unique to the application
            if (!String.IsNullOrEmpty(threadName) && _StartedNamedThreads.ContainsKey(threadName))
                 return null;

            System.Threading.Thread thread = null;
            if (method != null && parameter != null)
            {
                thread = new System.Threading.Thread(new ParameterizedThreadStart(method));
                if (!String.IsNullOrEmpty(threadName))
                    thread.Name = threadName;

                // Start the Thread                
                thread.SetApartmentState(Apartment);
                thread.Priority = priority;
                thread.IsBackground = IsBackground;
                thread.Start(parameter);                

                // Save the Thread in the Dictionary
                if (!String.IsNullOrEmpty(threadName))
                    _StartedNamedThreads[threadName] = thread;
            }
            return thread;
        }

        /// <summary>
        /// Starts a thread without Parameters
        /// </summary>
        /// <param name="method">specifcy a method to invoke</param>
        /// <param name="threadName">If you specify a ThreadName, it must be Unique to the Application</param>
        /// <param name="Apartment">ApartmentState</param>
        /// <param name="IsBackground">Is Background Thread</param>
        /// <param name="priority">set thread priority</param>
        /// <returns>a thread object if successful, null otherwise</returns>
        public static System.Threading.Thread StartThread(ThreadMethod method, string threadName = "", ApartmentState Apartment = ApartmentState.MTA, bool IsBackground = false, ThreadPriority priority = ThreadPriority.Normal)
        {
            // If a threadName is defined, make sure that it is unique to the system
            if (!String.IsNullOrEmpty(threadName) && _StartedNamedThreads.ContainsKey(threadName))
                return null;

            System.Threading.Thread thread = null;
            if (method != null)
            {
                thread = new System.Threading.Thread(new ThreadStart(method));
                if (!String.IsNullOrEmpty(threadName))
                    thread.Name = threadName;

                // Start the Thread
                thread.SetApartmentState(Apartment);
                thread.Priority = priority;
                thread.IsBackground = IsBackground;
                thread.Start();

                // Save the Thread in the Dictionary
                if (!String.IsNullOrEmpty(threadName))
                    _StartedNamedThreads[threadName] = thread;
            }
            return thread;

        }

        /// <summary>
        /// Retrieve all Named threads that were started thru here that are still running
        /// </summary>
        /// <returns>an array of running named threads, null if none</returns>
        public static System.Threading.Thread[] GetStartedNamedThreadsThatAreStillRunning()
        {
            List<System.Threading.Thread> threads = new List<System.Threading.Thread>();
            List<string> threadNamesThatAreNoLongerAlive = new List<string>();
            
            // Get Named Alive Threads
            foreach (System.Threading.Thread thread in _StartedNamedThreads.Values)
            {
                if (thread.IsAlive)
                    threads.Add(thread);
                else
                    threadNamesThatAreNoLongerAlive.Add(thread.Name);
            }

            // Perform Cleanup
            foreach (string threadName in threadNamesThatAreNoLongerAlive)
            {
                _StartedNamedThreads[threadName] = null;
                _StartedNamedThreads.Remove(threadName);
            }

            return threads.ToArray();
        }

        #endregion
    }
}
