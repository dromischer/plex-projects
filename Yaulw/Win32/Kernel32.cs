﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Yaulw.Win32
{
    /// <summary>
    /// Kernel32.dll Entry Points * http://pinvoke.net/ *
    /// </summary>
    public static class Kernel32
    {
        [DllImport("kernel32.dll")]
        extern public static bool AttachConsole(int dwProcessId);

        [DllImport("kernel32.dll")]
        extern public static IntPtr CreateEvent(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, [In] StringBuilder lpName);

        [DllImport("kernel32.dll")]
        extern public static bool CloseHandle(IntPtr hHandle);

        [DllImport("kernel32.dll")]
        extern public static bool FreeConsole();

        [DllImport("kernel32.dll")]
        extern public static int GetLastError();

        [DllImport("kernel32.dll")]
        extern public static uint GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder retVal, uint nSize, string lpFileName);

        [DllImport("kernel32.dll")]
        extern public static IntPtr GetProcAddress(IntPtr hModule, String procname);

        [DllImport("kernel32.dll")]
        extern public static int GetProcessId(IntPtr hProcess);

        [DllImport("kernel32.dll")]
        extern public static uint GlobalAddAtom(string lpString);

        [DllImport("kernel32.dll")]
        extern public static uint GlobalGetAtomName(uint nAtom, [Out] StringBuilder lpBuffer, int nSize);

        [DllImport("kernel32.dll")]
        extern public static uint GlobalDeleteAtom(uint nAtom);

        [DllImport("kernel32.dll")]
        extern public static IntPtr LoadLibrary(String dllname);

        [DllImport("kernel32.dll")]
        extern public static IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        extern public static bool SetEvent(IntPtr hEvent);

        [DllImport("kernel32.dll")]
        extern public static bool SetProcessDEPPolicy(int dwFlags);

        [DllImport("kernel32.dll")]
        extern public static int WaitForSingleObject(IntPtr hHandle, uint dwMilliseconds);

        [DllImport("kernel32.dll")]
        extern public static bool WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);

        [DllImport("kernel32.dll", SetLastError = true)]
        extern public static int GetShortPathName(string path, [Out] StringBuilder shortPath, int shortPathLength);

        [DllImport("kernel32.dll", SetLastError = true)]
        extern public static int GetLongPathName(string path, [Out] StringBuilder longPath, int longPathLength);
    }
}
