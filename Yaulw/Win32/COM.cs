﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace Yaulw.Win32
{
    /// <remarks>
    /// COM Win32 Helper Functions
    /// </remarks>
    public static class COM
    {
        /// <summary>
        /// Returns a pointer to an implementation of IBindCtx (a bind context object). This object stores information about a particular moniker-binding operation
        /// </summary>
        /// <param name="reserved">This parameter is reserved and must be 0</param>
        /// <param name="ppbc">Address of an IBindCtx* pointer variable that receives the interface pointer to the new bind context object. </param>
        /// <returns>This function can return the standard return values E_OUTOFMEMORY and S_OK</returns>
        [DllImport("ole32.dll")]
        public static extern int CreateBindCtx(int reserved, out IBindCtx ppbc);

        /// <summary>
        /// Use this to retrieve the Actice COM Object from the ROT, for the specified progId
        /// </summary>
        /// <param name="progId"></param>
        /// <returns>a valid com object, or null if error occured</returns>
        public static Object GetActiceCOMObject(string progId)
        {
            Object app = null;
            try
            {
                app = Marshal.GetActiveObject(progId);
            }
            catch (SystemException) { /* ignore */ }
            return app;
        }

        /// <summary>
        /// ROT Object Entry
        /// </summary>
        public struct RunningObject
        {
            public string name;
            public object o;
        }

        /// <summary>
        /// Use this to Get All Running Objects in the ROT
        /// </summary>
        /// <returns>list of all Runnint Objects in the ROT</returns>
        public static List<RunningObject> GetRunningObjects()
        {
            // Get the table.
            var res = new List<RunningObject>();
            IBindCtx bc;

            CreateBindCtx(0, out bc);
            IRunningObjectTable runningObjectTable;

            bc.GetRunningObjectTable(out runningObjectTable);
            IEnumMoniker monikerEnumerator;
            runningObjectTable.EnumRunning(out monikerEnumerator);
            monikerEnumerator.Reset();

            // Enumerate and fill our nice dictionary.
            IMoniker[] monikers = new IMoniker[1];
            IntPtr numFetched = IntPtr.Zero;

            while (monikerEnumerator.Next(1, monikers, numFetched) == 0)
            {
                RunningObject running;
                monikers[0].GetDisplayName(bc, null, out running.name);
                runningObjectTable.GetObject(monikers[0], out running.o);
                res.Add(running);
            }
            return res;
        }

        /// <summary>
        /// Use this to Get A specific type of Object from the ROT
        /// </summary>
        /// <returns>List of a specific type of Object in the ROT</returns>
        public static List<T> GetRunningObjectsOfType<T>()
        {
            // Get the table.
            var res = new List<T>();
            IBindCtx bc;

            CreateBindCtx(0, out bc);
            IRunningObjectTable runningObjectTable;

            bc.GetRunningObjectTable(out runningObjectTable);
            IEnumMoniker monikerEnumerator;
            runningObjectTable.EnumRunning(out monikerEnumerator);
            monikerEnumerator.Reset();

            // Enumerate and fill our nice dictionary.
            IMoniker[] monikers = new IMoniker[1];
            IntPtr numFetched = IntPtr.Zero;

            while (monikerEnumerator.Next(1, monikers, numFetched) == 0)
            {
                object o;
                runningObjectTable.GetObject(monikers[0], out o);

                if (o is T)
                    res.Add((T)o);
                o = null;
            }
            return res;
        }
    }

}
