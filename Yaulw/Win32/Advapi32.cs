﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;


namespace Yaulw.Win32
{
    /// <remarks>
    /// Advapi.dll Entry Points * http://pinvoke.net/ *
    /// </remarks>
    public static class Advapi32
    {
        /// <summary>
        /// Retrieves the current status of the specified service based on the specified information level
        /// </summary>
        /// <param name="hService">A handle to the service</param>
        /// <param name="InfoLevel">The service attributes to be returned</param>
        /// <param name="lpBuffer">A pointer to the buffer that receives the status information</param>
        /// <param name="cbBufSize">he size of the buffer pointed to by the lpBuffer</param>
        /// <param name="pcbBytesNeeded">A pointer to a variable that receives the number of bytes needed to store all status information</param>
        /// <returns>If the function succeeds, the return value is nonzero</returns>
        [DllImport("Advapi32.dll")]
        extern public static bool QueryServiceStatusEx(IntPtr hService, int InfoLevel, ref Structures.SERVICE_STATUS_PROCESS lpBuffer, int cbBufSize, out int pcbBytesNeeded);
    }
}
