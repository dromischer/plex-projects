﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Yaulw.Win32
{
    /// <remarks>
    /// Helper class to Convert Win32 Structures to DotNet and vice versa
    /// </remarks>
    public static class Convert
    {
        /// <summary>
        /// Use this to Convert to Win32 Window Message Enum from a C# Message
        /// </summary>
        /// <param name="m">a C# Message</param>
        /// <returns>a Win32 Message Enum</returns>
        public static Definitions.WM MessageToWin32WM(Message m)
        {
            Definitions.WM wm = Definitions.WM.WM_NULL;
            try { wm = (Definitions.WM)m.Msg; }
            catch (Exception) { wm = Definitions.WM.WM_NULL; }
            return wm;
        }

        /// <summary>
        /// Use this to Convert to a C# Rectangle from a Win32 RECT
        /// </summary>
        /// <param name="rect">a Win32 Rect</param>
        /// <returns>a C# Rectancle</returns>
        public static Rectangle RECTToRectangle(Structures.RECT rect)
        {
            return new Rectangle(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
        }

        /// <summary>
        /// Use this to Convert a Win32 RECT to a C# Rectangle
        /// </summary>
        /// <param name="rect">a C# Rectangle</param>
        /// <returns>a Win32 Rect</returns>
        public static Structures.RECT RectangleToRECT(Rectangle rect)
        {
            return Structures.RECT.FromXYWH(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        #region Win32Owner

        /// <remarks>
        /// a IWin32Window Object commonly used by WinForms
        /// </remarks>
        public class Win32Owner : IWin32Window
        {
            public IntPtr Handle { get; set; }
        }

        /// <summary>
        /// Convert a Handle to a Win32Owner Object
        /// </summary>
        /// <param name="hWnd">pass in a Window Handle</param>
        /// <returns>a newly created Win32Owner Object</returns>
        public static IWin32Window ConverthWndToIWin32Window(IntPtr hWnd)
        {
            return new Win32Owner() { Handle = hWnd };
        }

        #endregion
    }
}
