﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Yaulw.Win32
{
    /// <remark>
    /// User32.dll Entry Points * http://pinvoke.net/ *
    /// </remark>
    public static class User32
    {
        // Arg for SetWindowsHookEx()
        public delegate int WindowsHookProc(int nCode, IntPtr wParam, IntPtr lParam);

        // Arg for EnumWindows (EnumWindows Callback)
        public delegate bool EnumWindowsProc(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        extern public static IntPtr BeginPaint(IntPtr hWnd, ref Structures.PAINTSTRUCT paintStruct);

        [DllImport("user32.dll")]
        extern public static int CallNextHookEx(int idHook, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        extern public static bool EmptyClipboard();

        [DllImport("user32.dll")]
        extern public static bool EndPaint(IntPtr hWnd, ref Structures.PAINTSTRUCT paintStruct);

        [DllImport("User32.dll")]
        extern public static bool EnumChildWindows(IntPtr hParent, Delegate lpEnumFunc, IntPtr lParam);

        [DllImport("User32.dll")]
        extern public static bool EnumWindows(Delegate lpEnumFunc, IntPtr lParam);
        
        [DllImport("User32.dll")]
        extern public static IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        extern public static IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        /// <summary>
        /// Helper FindWindowEx Function * Useful *
        /// </summary>
        public static IntPtr FW(IntPtr hwndParent, string lpszClass)
        {
            return FindWindowEx(hwndParent, IntPtr.Zero, lpszClass, String.Empty);
        }

        [DllImport("user32.dll")]
        extern public static int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll")]
        extern public static bool GetClientRect(IntPtr hWnd, out Structures.RECT rect);

        [DllImport("user32.dll")]
        extern public static int GetClipboardSequenceNumber();

        [DllImport("user32.dll")]
        extern public static IntPtr GetDC(IntPtr ptr);

        [DllImport("user32.dll")]
        extern public static IntPtr GetDCEx(IntPtr hWnd, IntPtr hrgnClip, int flags);

        [DllImport("user32.dll")]
        extern public static IntPtr GetDesktopWindow();

        [DllImport("user32.dll")]
        extern public static IntPtr GetDlgItem(IntPtr hDlg, int nIDDlgItem);

        [DllImport("user32.dll")]
        extern public static uint GetDlgItemText(IntPtr hDlg, int nIDDlgItem, [Out] StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll")]
        extern public static IntPtr GetFocus();

        [DllImport("user32.dll")]
        extern public static IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        extern public static short GetKeyState(Definitions.VK vKey);

        [DllImport("user32.dll")]
        extern public static bool GetMonitorInfo(IntPtr hMonitor, ref Structures.MONITORINFOEX monitorinfo);

        [DllImport("user32.dll")]
        extern public static IntPtr GetParent(IntPtr hWnd);

        [DllImport("user32.dll")]
        extern public static int GetSystemMetrics(int index);

        [DllImport("user32.dll")]
        extern public static IntPtr GetTopWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        extern public static IntPtr GetWindowDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        extern public static long GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        extern public static bool GetWindowRect(IntPtr hWnd, out Structures.RECT rect);

        [DllImport("user32.dll")]
        extern public static int GetWindowText(IntPtr hWnd, StringBuilder text, int count);

        [DllImport("user32.dll")]
        extern public static int GetWindowTextLength(IntPtr hWnd);

        [DllImport("User32.dll")]
        extern public static int GetWindowThreadProcessId(IntPtr hWnd, ref int lpdwProcessId);

        [DllImport("user32.dll")]
        extern public static IntPtr MonitorFromRect(ref Structures.RECT rect, int dwFlags);

        [DllImport("user32.dll")]
        extern public static void mouse_event(Definitions.MouseEvent dwMouseEventFlags, long dx, long dy, long cButtons, long dwExtraInfo);

        [DllImport("user32.dll")]
        extern public static bool IsWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        extern public static bool IsWindowEnabled(IntPtr hwnd);

        [DllImport("User32.dll")]
        extern public static bool IsWindowVisible(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern bool LockWorkStation();

        [DllImport("User32.dll")]
        extern public static bool PostMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        extern public static uint RegisterWindowMessage(string lpString);

        [DllImport("user32.dll")]
        extern public static bool ReleaseCapture();

        [DllImport("user32.dll")]
        extern public static IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);

        [DllImport("user32.dll")]
        extern public static uint SendInput(uint nInputs, ref Structures.INPUT pInputs, int cbSize);

        [DllImport("User32.dll")]
        extern public static int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        extern public static IntPtr SetActiveWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        extern public static IntPtr SetCapture(IntPtr hWnd);

        [DllImport("user32.dll")]
        extern public static IntPtr SetFocus(IntPtr hwnd);

        [DllImport("user32.dll")]
        extern public static int SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        extern public static bool SetLayeredWindowAttributes(IntPtr hwnd, Int32 crKey, byte bAlpha, uint dwFlags);

        [DllImport("user32.dll")]
        extern public static IntPtr SetParent(IntPtr child, IntPtr newParent);

        [DllImport("user32.dll")]
        extern public static int SetWindowsHookEx(int idHook, WindowsHookProc lpfn, IntPtr hInstance, int threadId);

        [DllImport("user32.dll")]
        extern public static long SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

        [DllImport("user32.dll")]
        extern public static bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll")]
        extern public static int ShowScrollBar(IntPtr hWnd, int wBar, int bShow);

        [DllImport("user32.dll")]
        extern public static int ShowWindow(IntPtr hwnd, int nCmdShow);

        [DllImport("user32.dll")]
        extern public static bool UnhookWindowsHookEx(int idHook);
    }
}
