﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Yaulw.Win32
{
    /// <remark>
    /// User32.dll Entry Points * http://pinvoke.net/ *
    /// </remark>
    public static class Wininet
    {
        [DllImport("wininet", CharSet = CharSet.Auto)]
        extern public static bool InternetGetConnectedState(ref Definitions.ConnectionStatusEnum flags, int dw);


    }
}
