﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Yaulw.Win32
{
    /// <remarks>
    /// Gdi32.dll Entry Points * http://pinvoke.net/ *
    /// </remarks>
    public static class Gdi32
    {
        /// <summary>
        /// The BitBlt function performs a bit-block transfer of the color data corresponding to a rectangle of pixels from
        /// the specified source device context into a destination device context.
        /// </summary>
        /// <param name="hdcDest">A handle to the destination device context</param>
        /// <param name="xDest">The x-coordinate, in logical units, of the upper-left corner of the destination rectangle</param>
        /// <param name="yDest">The y-coordinate, in logical units, of the upper-left corner of the destination rectangle</param>
        /// <param name="wDest">he width, in logical units, of the source and destination rectangles</param>
        /// <param name="hDest">The height, in logical units, of the source and the destination rectangles</param>
        /// <param name="hdcSource">A handle to the source device context</param>
        /// <param name="xSrc">The x-coordinate, in logical units, of the upper-left corner of the source rectangle</param>
        /// <param name="ySrc">The y-coordinate, in logical units, of the upper-left corner of the source rectangle</param>
        /// <param name="RasterOp">A raster-operation code. These codes define how the color data for the source rectangle is to be combined with the color data for the destination rectangle to achieve the final color</param>
        /// <returns>If the function succeeds, the return value is nonzero</returns>
        [DllImport("gdi32.dll")]
        extern public static bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, int RasterOp);

        /// <summary>
        /// This function creates a bitmap compatible with the device associated with the specified device context
        /// </summary>
        /// <param name="hdc">Handle to a device context</param>
        /// <param name="nWidth">Specifies the bitmap width, in pixels</param>
        /// <param name="nHeight">Specifies the bitmap height, in pixels</param>
        /// <returns>A handle to the bitmap indicates success. NULL indicates failure.</returns>
        [DllImport("gdi32.dll")]
        extern public static IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

        /// <summary>
        /// This function creates a memory device context (DC) compatible with the specified device
        /// </summary>
        /// <param name="hdc">Handle to an existing device context</param>
        /// <returns>The handle to a memory device context indicates success. NULL indicates failure.</returns>
        [DllImport("gdi32.dll")]
        extern public static IntPtr CreateCompatibleDC(IntPtr hdc);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpszDriver"></param>
        /// <param name="passNULL"></param>
        /// <param name="passNULL2"></param>
        /// <param name="passNULL3"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        extern public static IntPtr CreateDC(string lpszDriver, IntPtr passNULL, IntPtr passNULL2, IntPtr passNULL3);        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hDc"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        extern public static IntPtr DeleteDC(IntPtr hDc);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hDc"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        extern public static IntPtr DeleteObject(IntPtr hDc);              

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hdc"></param>
        /// <param name="bmp"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        extern public static IntPtr SelectObject(IntPtr hdc, IntPtr bmp);       
    }
}
