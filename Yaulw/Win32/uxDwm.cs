﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Yaulw.Win32
{
    /// <remarks>
    /// uxTheme.dll and dwmapi.dll Entry Points * http://pinvoke.net/ *
    /// </remarks>
    public static class uxDwm
    {
        /// <summary>
        /// Obtains a value that indicates whether Desktop Window Manager (DWM) composition is enabled
        /// </summary>
        /// <param name="pfEnabled">TRUE if DWM composition is enabled; otherwise, FALSE</param>
        /// <returns>If this function succeeds, it returns S_OK. Otherwise, it returns an HRESULT error code</returns>
        [DllImport("dwmapi.dll")]
        extern public static long DwmIsCompositionEnabled(ref bool pfEnabled);

        /// <summary>
        /// Reports whether the current application's user interface displays using visual styles
        /// </summary>
        /// <returns>TRUE, if the application has a visual style applied. FALSE otherwise</returns>
        [DllImport("uxTheme.dll")]
        extern public static bool IsAppThemed();           
    }
}
