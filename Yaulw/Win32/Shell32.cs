﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Yaulw.Win32
{
    /// <summary>
    /// Shell32.dll Entry Points * http://pinvoke.net/ *
    /// </summary>
    public static class Shell32
    {
        [DllImport("Shlwapi.dll")]
        extern public static uint AssocQueryString(Definitions.AssocF flags, Definitions.AssocStr str, string pszAssoc, string pszExtra, [Out] StringBuilder pszOut, [In][Out] ref uint pcchOut);

        [DllImport("shell32.dll")]
        extern public static IntPtr FindExecutable(string lfFile, string lpDirectory, [Out] StringBuilder lpResult);
        
        [DllImport("shell32.dll")]
        extern public static IntPtr ShellExecute(IntPtr hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);

        [DllImport("shell32.dll")]
        extern public static bool ShellExecuteEx(ref Structures.ShellExecuteInfo lpExecInfo);

        [DllImport("shell32.dll")]
        extern public static long SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken, int dwFlags, [Out] StringBuilder pszPath);
    }
}
