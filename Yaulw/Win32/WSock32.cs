﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net.Sockets;

namespace Yaulw.Win32
{
    /// <remarks>
    /// WSock32.dll Entry Points * http://pinvoke.net/ *
    /// </remarks>
    public static class WSock32
    {

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, BestFitMapping = false, ThrowOnUnmappableChar = true, SetLastError = true)]
        extern public static SocketError WSAStartup([In] short wVersionRequested, [Out] out Structures.WSAData lpWSAData);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int WSACleanup();

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In, Out] ref Structures.FileDescriptorSet readfds,
                                        [In, Out] ref Structures.FileDescriptorSet writefds,
                                        [In, Out] ref Structures.FileDescriptorSet exceptfds,
                                        [In] ref Structures.TimeValue timeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In, Out] ref Structures.FileDescriptorSet readfds,
                                        [In, Out] ref Structures.FileDescriptorSet writefds,
                                        [In, Out] ref Structures.FileDescriptorSet exceptfds,
                                        [In] IntPtr nullTimeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In, Out] ref Structures.FileDescriptorSet readfds,
                                        [In] IntPtr ignoredA,
                                        [In] IntPtr ignoredB,
                                        [In] ref Structures.TimeValue timeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In, Out] ref Structures.FileDescriptorSet readfds,
                                        [In] IntPtr ignoredA,
                                        [In] IntPtr ignoredB,
                                        [In] IntPtr nullTimeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In] IntPtr ignoredA,
                                        [In, Out] ref Structures.FileDescriptorSet writefds,
                                        [In] IntPtr ignoredB,
                                        [In] ref Structures.TimeValue timeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In] IntPtr ignoredA,
                                        [In, Out] ref Structures.FileDescriptorSet writefds,
                                        [In] IntPtr ignoredB,
                                        [In] IntPtr nullTimeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In] IntPtr ignoredA,
                                        [In] IntPtr ignoredB,
                                        [In, Out] ref Structures.FileDescriptorSet exceptfds,
                                        [In] ref Structures.TimeValue timeout);

        [DllImport("wsock32.dll", CharSet = CharSet.Ansi, SetLastError = true)]
        extern public static int select([In] int ignoredParameter,
                                        [In] IntPtr ignoredA,
                                        [In] IntPtr ignoredB,
                                        [In, Out] ref Structures.FileDescriptorSet exceptfds,
                                        [In] IntPtr nullTimeout);

    }
}
