﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using WatchdogLib.Thread;
using System.Timers;

namespace WatchdogLib.WinForms
{       
    /// <summary>
    /// Wrapper Class around .Net NotifyIcon, to make it easier to work with an System Tray Icon.
    /// Instantiate the object and set ContextMenu or ContextMenuStrip in order to have a right click menu.
    /// Subscripe to the MouseLeftClick, and MouseLeftDoubleClick event to get accurate events to handle,
    /// Call Show()/Hide() to show/hide the System Tray Icon, respectively.
    /// </summary>
    public class SysTray
    {
        #region Public Properties
        
        public NotifyIcon trayNotify { get; private set; }
        public Icon Icon { get { return trayNotify.Icon; } set { trayNotify.Icon = value; } }
        public ContextMenu trayMenu { get { return trayNotify.ContextMenu; } set { trayNotify.ContextMenu = value; } }
        public ContextMenuStrip trayMenuStrip { get { return trayNotify.ContextMenuStrip; } set { trayNotify.ContextMenuStrip = value; } }
        public string toolTip { get { return trayNotify.Text; } set { trayNotify.Text = value; } }
        public bool Visible { get { return trayNotify.Visible; } } 

        #endregion

        #region Public Events

        public delegate void SingleLeftMouseClick(MouseEventArgs e);
        /// <summary>
        /// Subscribe to get the Left Mouse Single Click Event
        /// </summary>
        public event SingleLeftMouseClick LeftMouseClick;

        public delegate void DoubleLeftMouseClick(MouseEventArgs e);
        /// <summary>
        /// Subscribe to get the Left Mouse Double Click Event
        /// </summary>
        public event DoubleLeftMouseClick LeftMouseDoubleClick;

        public delegate void RightMouseClick(MouseEventArgs e);
        /// <summary>
        /// Subscribe to get any Right Mouse Fired Event, use to redo context menu, if needed
        /// </summary>
        public event RightMouseClick RightMouseFired;

        #endregion

        #region Private Members

        private TTimer SingleClickDetectTimer = null;
        private TimeSpan _LastFiredEvent = new TimeSpan(DateTime.Now.Ticks);
        private const int _MILISECONDS_FOR_SINGLEMOUSE_CLICKEVENT_TOCOUNT = 500;
        private const int _N_SECONDS_TOIGNORE_NEXT_SIGNLEMOUSE_CLICKEVENT = 2; // to avoid tripple clicks, etc... (only sends one double click)

        /// <summary>
        /// Returns true if enough time since _LastFiredEvent has passed
        /// </summary>
        private bool EnoughTimeSinceLastEventHasElapsed
        {
            get
            {
                // 1 second is 10 Million ticks, one Milisecond is 10K
                return (DateTime.Now.Ticks - _LastFiredEvent.Ticks) >= (_N_SECONDS_TOIGNORE_NEXT_SIGNLEMOUSE_CLICKEVENT * 1000 * 10000);
            }
        }

        #endregion

        #region Construction

        /// <summary>
        /// Construct a System Tray Icon (use public properties like ContextMenu,Icon,toolTip to customize further)
        /// </summary>
        /// <param name="toolTip">pass in an initial ToolTip to display, defaults to ""</param>
        /// <param name="icon">if null, defaults to systemIcons.Application</param>
        //public SysTray(string toolTip = "", Icon icon = null)
        public SysTray(string toolTip, Icon icon)
        {
            // Create internal objects
            this.trayNotify = new NotifyIcon();
            this.SingleClickDetectTimer = new TTimer(new ElapsedEventHandler(RealSingleClickDetectTimer_ElapsedEventHandler), _MILISECONDS_FOR_SINGLEMOUSE_CLICKEVENT_TOCOUNT, false, true);

            // Add Single / Double-Click Event Handlers
            trayNotify.Click += new EventHandler(trayNotify_Click);
            trayNotify.DoubleClick += new EventHandler(trayNotify_DoubleClick);
            trayNotify.MouseDown += new MouseEventHandler(trayNotify_MouseDown);
            
            // Set ToolTip
            if (!String.IsNullOrEmpty(toolTip))
                this.toolTip = toolTip;
            
            // Set Icon
            if (icon == null)
                this.Icon = new Icon(SystemIcons.Application, 40, 40);
            else
                this.Icon = icon;
        }

        #endregion

        #region Click Event Handlers

        /// <summary>
        /// Called by NotifyIcon DoubleClick Event, We filter for only the left mouse double-click,
        /// event and fire event when neccessary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trayNotify_DoubleClick(object sender, EventArgs e)
        {
            MouseEventArgs args = (MouseEventArgs)e;
            if (args.Button == MouseButtons.Left)
            {
                SingleClickDetectTimer.Stop();
                if (LeftMouseDoubleClick != null && EnoughTimeSinceLastEventHasElapsed)
                {
                    _LastFiredEvent = new TimeSpan(DateTime.Now.Ticks);
                    LeftMouseDoubleClick(new MouseEventArgs(MouseButtons.Left, 2, 0, 0, 0));
                }
            }
        }

        /// <summary>
        // Called by NotifyIcon Click Event, We filter for only the left mouse click,
        /// event and fire event when neccessary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void trayNotify_Click(object sender, EventArgs e)
        {
            MouseEventArgs args = (MouseEventArgs) e;
            if (args.Button == MouseButtons.Left && EnoughTimeSinceLastEventHasElapsed)
                SingleClickDetectTimer.Start(); // Start Single Click Detect Timer              
        }

        /// <summary>
        /// In order to accurately re-do a context menu, we handle MouseDown for the 
        /// Right-Mouse click. Mouse Down comes in before the click event, which gives
        /// the caller an opportunity to handle/recreate the context menu dynamically, if needed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void trayNotify_MouseDown(object sender, MouseEventArgs e)
        {
            MouseEventArgs args = (MouseEventArgs)e;
            if (args.Button == MouseButtons.Right && (RightMouseFired != null))
                RightMouseFired(args);
        }

        /// <summary>
        /// Used to detect ONLY Single Clicks, since a single-click and then a double-click fires,
        /// we want to ignore the first click,and first see if a double-click comes in, if so, ignore
        /// the single click, otherwise send it. (this is done by trayNotify_Click & transNotify_DoubleClick)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RealSingleClickDetectTimer_ElapsedEventHandler(object sender, ElapsedEventArgs e)
        {
            SingleClickDetectTimer.Stop();
            if (LeftMouseClick != null)
            {
                _LastFiredEvent = new TimeSpan(DateTime.Now.Ticks);
                LeftMouseClick(new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            }
        }

        #endregion

        #region Show N' Hide

        /// <summary>
        /// Show the System Tray Icon
        /// </summary>
        public void Show()
        {
            trayNotify.Visible = true;
        }

        /// <summary>
        /// Hide the System Tray Icon
        /// </summary>
        public void Hide()
        {
            trayNotify.Visible = false;
        }

        #endregion

        #region Public ShowBallon

        /// <summary>
        /// Pops up a Ballon over the System Tray Icon
        /// </summary>
        /// <param name="nTimeoutInSeconds">Specify the Timeout in Seconds</param>
        //public void ShowBallon_Default(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds = 10)
        public void ShowBallon_Default(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds)
        {
            trayNotify.ShowBalloonTip((nTimeoutInSeconds * 1000), BallonTipTitle, BallonTipText, ToolTipIcon.None);
        }

        /// <summary>
        /// Pops up a Error Ballon over the System Tray Icon
        /// </summary>
        /// <param name="nTimeoutInSeconds">Specify the Timeout in Seconds</param>
        //public void ShowBallon_Error(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds = 10)
        public void ShowBallon_Error(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds)
        {
            trayNotify.ShowBalloonTip((nTimeoutInSeconds * 1000), BallonTipTitle, BallonTipText, ToolTipIcon.Error);
        }

        /// <summary>
        /// Pops up a Warning Ballon over the System Tray Icon
        /// </summary>
        /// <param name="nTimeoutInSeconds">Specify the Timeout in Seconds</param>
        //public void ShowBallon_Warning(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds = 10)
        public void ShowBallon_Warning(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds)
        {
            trayNotify.ShowBalloonTip((nTimeoutInSeconds * 1000), BallonTipTitle, BallonTipText, ToolTipIcon.Warning);
        }

        /// <summary>
        /// Pops up a Info Ballon over the System Tray Icon
        /// </summary>
        /// <param name="nTimeoutInSeconds">Specify the Timeout in Seconds</param>
        //public void ShowBallon_Info(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds = 10)
        public void ShowBallon_Info(string BallonTipTitle, string BallonTipText, int nTimeoutInSeconds)
        {
            trayNotify.ShowBalloonTip((nTimeoutInSeconds * 1000), BallonTipTitle, BallonTipText, ToolTipIcon.Info);
        }

        #endregion
    }
}
