﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Component.All;
using BridgeConnector.Lib.File;

namespace Component.Binary.Mirth
{
    public class InstallComponent : IInstallComponent , ISetup
    {
        public const string MIRTH_SERVICE_NAME = Common_MediLytec.MediLytecPoundDef.MIRTH_SERVICE_NAME;
        private const string POSTGRE_SERVICE_NAME = Common_MediLytec.MediLytecPoundDef.POSTGRE_SERVICE_NAME;

        #region IInstallComponent Members

        public ComponentConfig.Component GetComponent()
        {
            return Common.EmbeddedConfig.BinaryComponents.GetComponent("Mirth");
        }

        public bool BEFORE_INSTALLING_COMPONENT()
        {
            bool bSuccess = Common.StopService(MIRTH_SERVICE_NAME);
            if (!bSuccess)
            {
                Common.Log.Error(String.Format("Failed to stop {0}", MIRTH_SERVICE_NAME));
                return false;
            }
            
            string result = Common.RunCmdLine("netsh firewall set portopening tcp 8080 MirthConnectAdministrator ENABLE ALL");
            bSuccess = result.Contains("successfully") || result.Contains("Ok.") || result.Contains("The service has not been started");
            if (bSuccess)
            {
                result = Common.RunCmdLine("netsh firewall set portopening tcp 8443 MirthConnectServer ENABLE ALL");
                bSuccess = result.Contains("successfully") || result.Contains("Ok.") || result.Contains("The service has not been started");
            }
            if (bSuccess)
            {
                result = Common.RunCmdLine("netsh firewall set portopening tcp 1099 MirthConnectJMX ENABLE ALL");
                bSuccess = result.Contains("successfully") || result.Contains("Ok.") || result.Contains("The service has not been started");
            }
            if (!bSuccess)
            {
                Common.Log.Error(String.Format("Errors occured opening up ports for '{0}'", MIRTH_SERVICE_NAME));
            }
            return bSuccess;
        }

        public bool INSTALL_COMPONENT()
        {
            Common.PSetupSpwan(GetComponent().TempFileNamesNPath[0], "-q -overwrite");            
            return true;
        }

        public bool AFTER_INSTALLING_COMPONENT()
        {
            if (!Common.ServiceExists(MIRTH_SERVICE_NAME))
            {
                Common.Log.Error(String.Format("Service '{0}' does not exist. Something went wrong with Setup", MIRTH_SERVICE_NAME));
                return false;
            }
            // Make sure Service is stopped
            Common.StopService(MIRTH_SERVICE_NAME);

            // # Configure Mirth to work with PostgreSQL, if possible
            if (Common.ServiceExists(POSTGRE_SERVICE_NAME))
            {
                //SetupMirthToUsePostgreSQL("mirthdb", "postgres", "Clinical$1");
            }
            return true;
        }

        public bool SUPPORTS_UNINSTALL()
        {
            return false;
        }

        public bool BEFORE_UNINSTALLING_COMPONENT()
        {
            return false;
        }

        public bool UNINSTALL_COMPONENT()
        {
            return false;
        }

        public bool AFTER_UNINSTALLING_COMPONENT()
        {
            return false;
        }

        #endregion

        #region ISetup Members

        public void ComponentLoaded(ref SetupEvents subscribeToDesiredEvents)
        {
            subscribeToDesiredEvents.Before_Install += new SetupEvents.SetupEvent(subscribeToDesiredEvents_Before_Install);
            subscribeToDesiredEvents.After_Install += new SetupEvents.SetupEvent(subscribeToDesiredEvents_After_Install);
            subscribeToDesiredEvents.Ending_Setup += new SetupEvents.SetupEvent(subscribeToDesiredEvents_Ending_Setup);
        }

        void subscribeToDesiredEvents_Ending_Setup()
        {
            // Make Sure at the end that both Mirth and Postgresql are started
            Common.StartService(POSTGRE_SERVICE_NAME);
            Common.StartService(MIRTH_SERVICE_NAME);
        }

        void subscribeToDesiredEvents_Before_Install()
        {
        }

        void subscribeToDesiredEvents_After_Install()
        {
        }        

        #endregion

        #region Private Helpers

        /// <summary>
        /// Function is responsible fore writing Postgresql Information to the Mirth Configuration
        /// * The Mirth Service needs to be stopped/started for this configuration to take affect,
        /// ideally this should only be called with the mirth service stopped *
        /// </summary>
        private void SetupMirthToUsePostgreSQL(string dbname, string user, string password)
        {
            // Open Mirth Configuration
            LineReplacer replacer = new LineReplacer((Common.GetProgramFilesPathOnSystemWithEndSlash() + "Mirth Connect\\conf\\mirth.properties"), Encoding.ASCII);

            LineReplace_Rule DBurl = new LineReplace_Rule();
            DBurl.StartsWith = "database.url =";
            DBurl.ReplaceLineWith = String.Format("database.url = jdbc:postgresql://localhost:5432/{0}", dbname);
            DBurl.Comparer = LineReplace_ComparerModifier.None;
            replacer.AddUpdateRule("DBurl", DBurl);

            LineReplace_Rule DBuser = new LineReplace_Rule();
            DBuser.StartsWith = "database.username =";
            DBuser.ReplaceLineWith = String.Format("database.username = {0}", user);
            DBuser.Comparer = LineReplace_ComparerModifier.None;
            replacer.AddUpdateRule("DBuser", DBuser);

            LineReplace_Rule DBpass = new LineReplace_Rule();
            DBpass.StartsWith = "database.password =";
            DBpass.ReplaceLineWith = String.Format("database.password = {0}", password);
            DBpass.Comparer = LineReplace_ComparerModifier.None;
            replacer.AddUpdateRule("DBpass", DBpass);

            // Replace Lines in Mirth Configuration
            replacer.ReplaceLines();
        }

        #endregion
    }
}
