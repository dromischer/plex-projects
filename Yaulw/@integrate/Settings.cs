﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Watchdog.WatchdogLib.Registry;
using Watchdog.WatchdogLib.Assembly;
using Watchdog.WatchdogLib.Monitor;
using Watchdog.WatchdogLib.Process;
using System.Diagnostics;
using WatchdogLib.Tools;

namespace Watchdog
{
    internal class Settings
    {
        /// <summary>
        /// Critical Section
        /// </summary>
        private object _lock = new object();

        #region Construction

        /// <summary>
        /// Construction
        /// </summary>
        internal Settings()
        {
        }

        #endregion

        #region Scheduler N' Scheduler Backup Settings

        /// <summary>
        /// Minimum Minute Interval for Schedulers
        /// </summary>
        public const uint MINIMUM_MINUTE_INTERVAL_SCHEDULERS = 10;

        /// <summary>
        /// True if there are Settings specified for the Scheduler
        /// </summary>
        internal bool SchedulerSettingsExist
        {
            get
            {
                DateTime dtStart = DateTime.MinValue;
                DateTime dtLastRun = DateTime.MinValue;
                uint nHoursOrMinutes = 0;
                bool bIsHours = true;
                return GetSchedulerSettings(out dtStart, out dtLastRun, out nHoursOrMinutes, out bIsHours);
            }
        }

        /// <summary>
        /// True if there are Settings specified for the Scheduler Backup
        /// </summary>
        internal bool SchedulerBackupSettingsExist
        {
            get
            {
                DateTime dtStart = DateTime.MinValue;
                DateTime dtLastRun = DateTime.MinValue;
                uint nHoursOrMinutes = 0;
                bool bIsHours = true;
                return GetSchedulerBackupSettings(out dtStart, out dtLastRun, out nHoursOrMinutes, out bIsHours);
            }
        }

        /// <summary>
        /// Use this to retrieve the Scheduler Settings 
        /// </summary>
        /// <param name="dtStart">Start Time</param>
        /// <param name="dtLastRun">Last Run Time</param>
        /// <param name="nHoursOrMinutes">number of Hours or Minutes</param>
        /// <param name="bIsHours">is hours</param>
        /// <returns>Returns true if there is Scheduler Information in the Settings, also returns the settings, false otherwise</returns>        
        internal bool GetSchedulerSettings(out DateTime dtStart, out DateTime dtLastRun, out uint nHoursOrMinutes, out bool bIsHours)
        {
            lock (_lock)
            {
                dtStart = DateTime.MinValue;
                dtLastRun = DateTime.MinValue;
                nHoursOrMinutes = 0;
                bIsHours = true;

                if (uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatEveryNHoursOrMinutes, "0"), out nHoursOrMinutes) &&
                    DateTime.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_StartDateTime, DateTime.MinValue.ToString()), out dtStart) &&
                    nHoursOrMinutes > 0 &&
                    dtStart != DateTime.MinValue
                    )
                {
                    // Get the Last time this scheduler was run
                    dtLastRun = DateTime.MinValue;
                    DateTime.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_LastRun_DateTime, DateTime.MinValue.ToString()), out dtLastRun);

                    // Are we on an hourly or minute schedule?
                    bIsHours = App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatIsHour, true);

                    // Minimum Interval for Minutes
                    if (!bIsHours && nHoursOrMinutes < MINIMUM_MINUTE_INTERVAL_SCHEDULERS)
                        nHoursOrMinutes = MINIMUM_MINUTE_INTERVAL_SCHEDULERS;

                    // return only true if nHoursOrMinutes is bigger 0 and dtStart is NOT equal to MinVal
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Use this to retrieve the Scheduler Backup Settings 
        /// </summary>
        /// <param name="dtStart">Start Time</param>
        /// <param name="dtLastRun">Last Run Time</param>
        /// <param name="nHoursOrMinutes">number of Hours or Minutes</param>
        /// <param name="bIsHours">is hours</param>
        /// <returns>Returns true if there is Scheduler Backup Information in the Settings, also returns the settings, false otherwise</returns>        
        internal bool GetSchedulerBackupSettings(out DateTime dtStart, out DateTime dtLastRun, out uint nHoursOrMinutes, out bool bIsHours)
        {
            lock (_lock)
            {
                dtStart = DateTime.MinValue;
                dtLastRun = DateTime.MinValue;
                nHoursOrMinutes = 0;
                bIsHours = true;

                if (uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_ForDurationNHoursOrMinutes, "0"), out nHoursOrMinutes) &&
                    DateTime.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StartDateTime, DateTime.MinValue.ToString()), out dtStart) &&
                    nHoursOrMinutes > 0 &&
                    dtStart != DateTime.MinValue
                    )
                {
                    // Get the Last time this scheduler was run
                    dtLastRun = DateTime.MinValue;
                    DateTime.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_LastRun_DateTime, DateTime.MinValue.ToString()), out dtLastRun);

                    // Are we on an hourly or minute schedule?
                    bIsHours = App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_DurationIsHour, true);

                    // Minimum Interval for Minutes
                    if (!bIsHours && nHoursOrMinutes < MINIMUM_MINUTE_INTERVAL_SCHEDULERS)
                        nHoursOrMinutes = MINIMUM_MINUTE_INTERVAL_SCHEDULERS;

                    // return only true if nHoursOrMinutes is bigger 0 and dtStart is NOT equal to MinVal
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Set/Get the Scheduler Start DT
        /// </summary>
        internal DateTime SchedulerStartDateTime
        {
            get
            {
                string strDateTime = App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_StartDateTime, DateTime.MinValue.ToString());
                DateTime dt;
                if (DateTime.TryParse(strDateTime, out dt) && dt != DateTime.MinValue)
                    return dt;
                else
                    return DateTime.Now;
            }
            set
            {
                if (value != DateTime.MinValue)
                    App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_StartDateTime, value.ToString());
            }
        }

        /// <summary>
        /// Set/Get the Scheduler Backup Start DT
        /// </summary>
        internal DateTime SchedulerBackupStartDateTime
        {
            get
            {
                string strDateTime = App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StartDateTime, DateTime.MinValue.ToString());
                DateTime dt;
                if (DateTime.TryParse(strDateTime, out dt) && dt != DateTime.MinValue)
                    return dt;
                else
                    return DateTime.Now;
            }
            set
            {
                if (value != DateTime.MinValue)
                    App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StartDateTime, value.ToString());
            }
        }

        /// <summary>
        /// Set/Get the Scheduler Repeat Hours or Minutes
        /// </summary>
        internal uint SchedulerRepeatEveryHoursOrMinutes
        {
            get
            {
                uint nRepeatEveryNHoursOrMinutes = 0;
                if (uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatEveryNHoursOrMinutes, "0"), out nRepeatEveryNHoursOrMinutes) && (nRepeatEveryNHoursOrMinutes >= 1 && nRepeatEveryNHoursOrMinutes <= 8760))
                {
                    if ((nRepeatEveryNHoursOrMinutes > 0) && (nRepeatEveryNHoursOrMinutes < MINIMUM_MINUTE_INTERVAL_SCHEDULERS) && !SchedulerRepeatIsHour)
                        return MINIMUM_MINUTE_INTERVAL_SCHEDULERS;
                    else
                        return nRepeatEveryNHoursOrMinutes;
                }
                else
                {
                    App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatEveryNHoursOrMinutes, "0");
                    return 0;
                }
            }
            set
            {
                if (value >= 0 && value <= 8760)
                {
                    if ((value > 0) && (value < MINIMUM_MINUTE_INTERVAL_SCHEDULERS) && !SchedulerRepeatIsHour)
                        App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatEveryNHoursOrMinutes, MINIMUM_MINUTE_INTERVAL_SCHEDULERS.ToString());
                    else
                        App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatEveryNHoursOrMinutes, value.ToString());
                }
            }
        }

        /// <summary>
        /// Set/Get the Scheduler Backup Duration Hours or Minutes
        /// </summary>
        internal uint SchedulerBackupForDurationHoursOrMinutes
        {
            get
            {
                uint nForDurationNHoursOrMinutes = 0;
                if (uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_ForDurationNHoursOrMinutes, "0"), out nForDurationNHoursOrMinutes) && (nForDurationNHoursOrMinutes >= 1 && nForDurationNHoursOrMinutes <= 8760))
                {
                    if ((nForDurationNHoursOrMinutes > 0) && (nForDurationNHoursOrMinutes < MINIMUM_MINUTE_INTERVAL_SCHEDULERS) && !SchedulerBackupDurationIsHour)
                        return MINIMUM_MINUTE_INTERVAL_SCHEDULERS;
                    else
                        return nForDurationNHoursOrMinutes;
                }
                else
                {
                     App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_ForDurationNHoursOrMinutes, "0");
                     return 0;
                }
            }
            set
            {
                if (value >= 0 && value <= 8760)
                {
                    if ((value > 0) && (value < MINIMUM_MINUTE_INTERVAL_SCHEDULERS) && !SchedulerBackupDurationIsHour)
                        App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_ForDurationNHoursOrMinutes, MINIMUM_MINUTE_INTERVAL_SCHEDULERS.ToString());
                    else
                        App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_ForDurationNHoursOrMinutes, value.ToString());
                }
            }
        }

        /// <summary>
        /// Set/Get the Scheduler Repeat Is Hour Flag
        /// </summary>
        internal bool SchedulerRepeatIsHour
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatIsHour, true);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_RepeatIsHour, value);
            }
        }

        /// <summary>
        /// Set/Get the Scheduler Backup Duration Is Hour Flag
        /// </summary>
        internal bool SchedulerBackupDurationIsHour
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_DurationIsHour, true);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_DurationIsHour, value);
            }
        }

        /// <summary>
        /// Boolean Flag indicating whether the Scheduler Restarts Services
        /// </summary>
        internal bool SchedulerRestartsServices
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Scheduler_Restarts_Services, false);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Scheduler_Restarts_Services, value);
            }
        }

        /// <summary>
        /// DT stamp when the Scheduler was last run
        /// </summary>
        internal DateTime LastSchedulerRun
        {
            get
            {
                // Get the Last time this scheduler was run
                DateTime dtLastRun = DateTime.MinValue;
                DateTime.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_LastRun_DateTime, DateTime.MinValue.ToString()), out dtLastRun);
                return dtLastRun;
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Sheduler_LastRun_DateTime, value.ToString());
            }

        }

        /// <summary>
        /// DT stamp when the Scheduler Backup was last run
        /// </summary>
        internal DateTime LastSchedulerBackupRun
        {
            get
            {
                // Get the Last time this scheduler was run
                DateTime dtLastRun = DateTime.MinValue;
                DateTime.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_LastRun_DateTime, DateTime.MinValue.ToString()), out dtLastRun);
                return dtLastRun;
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_LastRun_DateTime, value.ToString());
            }

        }

        /// <summary>
        /// Boolean Flag indicating whether the Scheduler Backup Stops Instead of Pauses
        /// </summary>
        internal bool SchedulerBackupStopInsteadOfPause
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StopInsteadOfPause, true);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StopInsteadOfPause, value);
            }
        }

        /// <summary>
        /// Boolean Flag indicating whether the Scheduler Backup Stops Servies
        /// </summary>
        internal bool SchedulerBackupStopServices
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StopServices, true);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__ShedulerBackup_StopServices, value);
            }
        }

        #endregion

        #region Program Settings
  
        /// <summary>
        /// Boolean flag indicates whether upon program start it should start monitoring right away
        /// </summary>
        internal bool StartMonitorUponStart
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Monitor_On_Start, true);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Monitor_On_Start, value);
            }
        }

        /// <summary>
        /// Indicates the max failure count that can occur per process/service within a given MaxFailureCountHour Timespan
        /// * Important Setting for Process Monitor * Value can be between 1 and 99
        /// </summary>
        internal uint MaxFailureCount
        {
            get
            {
                // Re-read MaxFailureCount Setting * Default is specified in AppResx *
                uint nMaxFailureCount = uint.Parse(AppResx.GetString("DEFAULT_VALUE_MAX_PROCESS_START_FAILURE_COUNT"));
                bool bCanParseIni = uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Max_Fail_Count, AppResx.GetString("DEFAULT_VALUE_MAX_PROCESS_START_FAILURE_COUNT")), out nMaxFailureCount);
                if (!bCanParseIni)
                    App.log.Error(AppResx.GetStringFormated("ERROR_INI_SETTINGS_PARSE", App.Ini_Setting.MonitorSettings__Max_Fail_Count.ToString()));
                if (nMaxFailureCount < 1)
                    nMaxFailureCount = 1;
                else if (nMaxFailureCount > 99)
                    nMaxFailureCount = 99;
                return nMaxFailureCount;
            }
            set
            {
                if (value >= 1 && value <= 99)
                    App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Max_Fail_Count, value.ToString());
            }
        }

        /// <summary>
        /// Indicates the Hour Timespan in which MaxFailureCount can occur
        /// * Important Setting for Process Monitor * Value can be between 1 and 8760
        /// </summary>
        internal uint MaxFailureCountHour
        {
            get
            {
                // Re-read MaxFailureCountHour Setting * Default is specified in AppResx
                uint nMaxFailureCountHour = uint.Parse(AppResx.GetString("DEFAULT_VALUE_MAX_PROCESS_START_FAILURE_COUNT_HOUR"));
                bool bCanParseIni = uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Max_Fail_Count_Per_Hour, AppResx.GetString("DEFAULT_VALUE_MAX_PROCESS_START_FAILURE_COUNT_HOUR")), out nMaxFailureCountHour);
                if (!bCanParseIni)
                    App.log.Error(AppResx.GetStringFormated("ERROR_INI_SETTINGS_PARSE", App.Ini_Setting.MonitorSettings__Max_Fail_Count_Per_Hour.ToString()));
                if (nMaxFailureCountHour < 1)
                    nMaxFailureCountHour = 1;
                else if (nMaxFailureCountHour > 8760)
                    nMaxFailureCountHour = 8760;
                return nMaxFailureCountHour;
            }
            set
            {
                if (value >= 1 && value <= 8760)
                    App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Max_Fail_Count_Per_Hour, value.ToString());
            }
        }

        /// <summary>
        /// Get/Set the Last Program Version in the Registry * Used to check if an upgrade occured *
        /// </summary>
        internal string LastProgramVersion
        {
            get
            {
               string LastVersion = RegKey.GetKey<string>(App.APPLICATION_NAME_SHORT, "LastProgramVersion", String.Empty);
               return LastVersion;
            }
            set
            {
                RegKey.SetKey<string>(App.APPLICATION_NAME_SHORT, "LastProgramVersion", value);
            }
        }

        #endregion

        #region Email Settings

        /// <summary>
        /// Boolean flag indicates whether Email Notifications are enabled
        /// </summary>
        internal bool EmailNotificationEnabled
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__EnableEmailNotifications, false);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__EnableEmailNotifications, value);
            }
        }

        /// <summary>
        /// True if the settings for Email are valid
        /// </summary>
        internal bool EmailSettingsValid
        {
            get
            {
                // Check reg
                bool NotValid = String.IsNullOrEmpty(EmailStmpServer) || String.IsNullOrEmpty(EmailSenderEmail) ||
                                String.IsNullOrEmpty(EmailReceiverEmail);
                if (NotValid)
                    return false;

                // Check auth
                if (EmailStmpServerRequiresAuth)
                    NotValid = String.IsNullOrEmpty(EmailSmtpUsername) || String.IsNullOrEmpty(EmailSmtpPassword);
                return !NotValid;
            }
        }

        /// <summary>
        /// Boolean flag indicates whether Email Smtp Server Requires Authentication
        /// </summary>
        internal bool EmailStmpServerRequiresAuth
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SmtpRequiresAuth, false);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpRequiresAuth, value);
            }
        }

        /// <summary>
        /// Boolean flag indicates whether Email Smtp Server Requires SSL
        /// </summary>
        internal bool EmailStmpServerRequiresSSL
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SmtpRequiresSSL, false);
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpRequiresSSL, value);
            }
        }

        /// <summary>
        /// Email Smtp Server
        /// </summary>
        internal string EmailStmpServer
        {
            get
            {
                string strFound = App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SmtpServer, "");
                if (!String.IsNullOrEmpty(strFound) && strFound.Contains('.'))
                    return strFound;
                else
                    return String.Empty;
            }
            set
            {
                if(!String.IsNullOrEmpty(value) && value.Contains('.'))
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpServer, value);
            }
        }

        /// <summary>
        /// Email Sender Email
        /// </summary>
        internal string EmailSenderEmail
        {
            get
            {
                string strFound = App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SenderEmail, "");
                if (!String.IsNullOrEmpty(strFound) && strFound.Contains('.') && strFound.Contains('@'))
                    return strFound;
                else
                    return String.Empty;
            }
            set
            {
                if (!String.IsNullOrEmpty(value) && value.Contains('.') && value.Contains('@'))
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SenderEmail, value);
            }
        }

        /// <summary>
        /// Email Receiver Email
        /// </summary>
        internal string EmailReceiverEmail
        {
            get
            {
                string strFound = App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__ReveiverEmail, "");
                if (!String.IsNullOrEmpty(strFound) && strFound.Contains('.') && strFound.Contains('@'))
                    return strFound;
                else
                    return String.Empty;
            }
            set
            {
                if (!String.IsNullOrEmpty(value) && value.Contains('.') && value.Contains('@'))
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__ReveiverEmail, value);
            }
        }

        /// <summary>
        /// Email Smtp Port * Default is 25 *
        /// </summary>
        internal uint EmailSmtpPort
        {
            get
            {
                uint nPort = 25;
                bool bCanParseIni = uint.TryParse(App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SmtpPort, "25"), out nPort);
                if (!bCanParseIni)
                    App.log.Error(AppResx.GetStringFormated("ERROR_INI_SETTINGS_PARSE", App.Ini_Setting.EmailSettings__SmtpPort.ToString()));
                if (nPort < 1)
                    nPort = 1;
                else if (nPort > 65536)
                    nPort = 65536;
                return nPort;
            }
            set
            {
                if (value >= 1 && value <= 65536)
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpPort, value.ToString());
            }
        }

        /// <summary>
        /// Email Smtp Username
        /// </summary>
        internal string EmailSmtpUsername
        {
            get
            {
                string strFound = App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SmtpUsername, "");
                if (!String.IsNullOrEmpty(strFound))
                    return strFound;
                else
                    return String.Empty;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpUsername, value);
                else
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpUsername, "");
            }
        }

        /// <summary>
        /// Email Smtp Password
        /// </summary>
        internal string EmailSmtpPassword
        {
            get
            {
                string strFound = App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__SmtpPassword, "");
                if (!String.IsNullOrEmpty(strFound))
                    return strFound;
                else
                    return String.Empty;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpPassword, value);
                else
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__SmtpPassword, "");
            }
        }

        /// <summary>
        /// Email Custom Message Text
        /// </summary>
        internal string EmailCustomMessageText
        {
            get
            {
                string strFound = App.inifile.GetKeyValue(App.Ini_Setting.EmailSettings__Custom_Message_Text, "");
                if (!String.IsNullOrEmpty(strFound))
                    return strFound;
                else
                    return String.Empty;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__Custom_Message_Text, value);
                else
                    App.inifile.SetKeyValue(App.Ini_Setting.EmailSettings__Custom_Message_Text, "");
            }
        }

        #endregion

        #region Windows Settings

        /// <summary>
        /// Boolean Flag indicates whether this app should start with windows
        /// </summary>
        internal bool StartWithWindows
        {
            get
            {
                bool bStartWithWindows = App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Start_With_Windows, true);
                bool bSetOk = true;
                if (bStartWithWindows)
                    bSetOk = RegKey.SetKey<string>("Microsoft\\Windows\\CurrentVersion\\Run", App.APPLICATION_NAME_SHORT, AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Entry));
                else
                    bSetOk = RegKey.DeleteKey("Microsoft\\Windows\\CurrentVersion\\Run", App.APPLICATION_NAME_SHORT);
                if (!bSetOk)
                    App.log.Error("Failed to write to Microsoft\\Windows\\CurrentVersion\\Run. Check Permissions.");

                // * Special Circumstance *
                if (bStartWithWindows)
                    DeleteOffendingHL7KeyIfApplicable();

                return bStartWithWindows;
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Start_With_Windows, value);
                bool bSetOk = true;
                if (value)
                    bSetOk = RegKey.SetKey<string>("Microsoft\\Windows\\CurrentVersion\\Run", App.APPLICATION_NAME_SHORT, AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Entry));
                else
                    bSetOk = RegKey.DeleteKey("Microsoft\\Windows\\CurrentVersion\\Run", App.APPLICATION_NAME_SHORT);

                // * Special Circumstance *
                if (value)
                    DeleteOffendingHL7KeyIfApplicable();

                if (!bSetOk)
                    App.log.Error("Failed to write to HKCU\\Microsoft\\Windows\\CurrentVersion\\Run. Check Permissions.");
            }
        }

        /// <summary>
        /// Boolean Flag indicates whether LockWorkstation should be called upon CU login
        /// </summary>
        internal bool LockWorkstationWithWindows
        {
            get
            {
                bool bLockWorkstationWithWindows = App.inifile.GetKeyValue(App.Ini_Setting.WindowsSettings__LockWorkstation_With_Windows, false);
                bool bSetOk = true;
                if (bLockWorkstationWithWindows)
                    bSetOk = RegKey.SetKey<string>("Microsoft\\Windows\\CurrentVersion\\Run", "ALockWorkstation", "rundll32.exe user32.dll LockWorkStation");
                else
                    bSetOk = RegKey.DeleteKey("Microsoft\\Windows\\CurrentVersion\\Run", "ALockWorkstation");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKCU\\Microsoft\\Windows\\CurrentVersion\\Run. Check Permissions.");
                return bLockWorkstationWithWindows;
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.WindowsSettings__LockWorkstation_With_Windows, value);
                bool bSetOk = true;
                if (value)
                    bSetOk = RegKey.SetKey<string>("Microsoft\\Windows\\CurrentVersion\\Run", "ALockWorkstation", "rundll32.exe user32.dll LockWorkStation");
                else
                    bSetOk = RegKey.DeleteKey("Microsoft\\Windows\\CurrentVersion\\Run", "ALockWorkstation");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKCU\\Microsoft\\Windows\\CurrentVersion\\Run. Check Permissions.");
            }
        }

        /// <summary>
        /// Boolean Flag indicates whether Windows should automatically Login
        /// </summary>
        internal bool EnableAutomaticLoginWithWindows
        {
            get
            {
                bool bAutoLogin = App.inifile.GetKeyValue(App.Ini_Setting.WindowsSettings__AutoLogin_With_Windows, false);
                bool bSetOk = true;
                if (bAutoLogin)
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "AutoAdminLogon", "1");
                else
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "AutoAdminLogon", "0");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon. Check Permissions.");
                return bAutoLogin;
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.WindowsSettings__AutoLogin_With_Windows, value);
                bool bSetOk = true;
                if (value)
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "AutoAdminLogon", "1");
                else
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "AutoAdminLogon", "0");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon. Check Permissions.");
            }
        }

        /// <summary>
        /// AutoLogin Default Domain
        /// </summary>
        internal string AutomaticLoginDefaultDomain
        {
            get
            {
                return RegKey.GetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultDomainName", Environment.UserDomainName);
            }
            set
            {                
                bool bSetOk = true;
                if (!String.IsNullOrEmpty(value))
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultDomainName", value);
                else
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultDomainName", "");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon. Check Permissions.");
            }
        }

        /// <summary>
        /// AutoLogin Default Username
        /// </summary>
        internal string AutomaticLoginDefaultUsername
        {
            get
            {
                return RegKey.GetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultUserName", Environment.UserName);
            }
            set
            {
                bool bSetOk = true;
                if (!String.IsNullOrEmpty(value))
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultUserName", value);
                else
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultUserName", "");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon. Check Permissions.");
            }
        }

        /// <summary>
        /// AutoLogin Default Password
        /// </summary>
        internal string AutomaticLoginDefaultPassword
        {
            get
            {
                return RegKey.GetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultPassword", "");
            }
            set
            {
                bool bSetOk = true;
                if (!String.IsNullOrEmpty(value))
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultPassword", value);
                else
                    bSetOk = RegKey.SetKey<string>(HKEYRoot.LocalMachine, "Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon", "DefaultPassword", "");
                if (!bSetOk)
                    App.log.Error("Failed to write to HKLM\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon. Check Permissions.");
            }
        }

        /// <summary>
        /// Creates an Exe HardLink to the Application .exe at the Rooth Path
        /// </summary>
        internal bool CreateExeHardLinkInRootPath
        {
            get
            {
                //fsutil hardlink create c:\foo.txt c:\bar.txt
                bool bCreateHardLinkInRootPath = App.inifile.GetKeyValue(App.Ini_Setting.WindowsSettings__CreateExeHardLink_In_RootPath, true);
                String FileNameWithExtension = AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameWithExtension(AssemblyW.AssemblyST.Entry);
                String RootPath = PathNaming.GetPathRoot(AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Entry));
                String FileNameFullPath = AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Entry);
                if (bCreateHardLinkInRootPath && (String.Compare(FileNameFullPath, String.Format("{0}{1}", RootPath, FileNameWithExtension), true) != 0))
                {
                    Process.Start(PStartInfo.CreateCMDDosCommandProcess(String.Format("del {0}{1}", RootPath, FileNameWithExtension), false));
                    Process.Start(PStartInfo.CreateCMDDosCommandProcess(String.Format("fsutil hardlink create {0}{1} \"{2}\"", RootPath, FileNameWithExtension, FileNameFullPath), false));
                }
                else if (String.Compare(FileNameFullPath, String.Format("{0}{1}", RootPath, FileNameWithExtension), true) != 0)
                {
                    Process.Start(PStartInfo.CreateCMDDosCommandProcess(String.Format("del {0}{1}", "", ""), false));
                }
                return bCreateHardLinkInRootPath;
            }
            set
            {
                App.inifile.SetKeyValue(App.Ini_Setting.WindowsSettings__CreateExeHardLink_In_RootPath, value);
                String FileNameWithExtension = AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameWithExtension(AssemblyW.AssemblyST.Entry);
                String RootPath = PathNaming.GetPathRoot(AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Entry));
                String FileNameFullPath = AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Entry);
                if (value && (String.Compare(FileNameFullPath, String.Format("{0}{1}", RootPath, FileNameWithExtension), true) != 0))
                {
                    Process.Start(PStartInfo.CreateCMDDosCommandProcess(String.Format("del {0}\\{1}", RootPath, FileNameWithExtension), false));
                    Process.Start(PStartInfo.CreateCMDDosCommandProcess(String.Format("fsutil hardlink create {0}\\{1} \"{2}\"", RootPath, FileNameWithExtension, FileNameFullPath), false));
                }
                else if (String.Compare(FileNameFullPath, String.Format("{0}{1}", RootPath, FileNameWithExtension), true) != 0)
                {
                    Process.Start(PStartInfo.CreateCMDDosCommandProcess(String.Format("del {0}\\{1}", "", ""), false));
                }
            }
        }

        #endregion

        #region Prefill Settings

        /// <summary>
        /// Prefill's the default exes for the Process Monitor (GUI Prefill Feature)
        /// </summary>
        internal string DefaultExeRunningFilter
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Default_Exe_Running_Filter, AppResx.GetString("DEFAULT_EXE_RUNNING_FILTER"));
            }
            set
            {
                if(value != null)
                   App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Default_Exe_Running_Filter, value);
            }
        }

        /// <summary>
        /// Prefill's the default services for the Service Monitor (GUI Prefill Feature)
        /// </summary>
        internal string DefaultServicesNamesRunningFilter
        {
            get
            {
                return App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Default_Services_Names_Running_Filter, AppResx.GetString("DEFAULT_SERVICE_NAMES_RUNNING_FILTER"));
            }
            set
            {
                if(value != null)
                   App.inifile.SetKeyValue(App.Ini_Setting.MonitorSettings__Default_Services_Names_Running_Filter, value);
            }
        }

        #endregion

        #region Window Settings

        /// <summary>
        /// Retrieve the Top N' Left for the Settings Window
        /// </summary>
        /// <param name="Top">Top Double</param>
        /// <param name="Left">Left Double</param>        
        internal void GetWindowSetting_TopLeft_SettingsWindow(ref double Top, ref double Left)
        {
            // Load the Window at the correct position
            try
            {
                string TopLeft = App.inifile.GetKeyValue(App.Ini_Setting.WindowSettings__SettingsWindow_TopLeft, String.Empty);
                if (!String.IsNullOrEmpty(TopLeft))
                {
                    string[] tl = TopLeft.Split(';');
                    Top = double.Parse(tl[0]);
                    Left = double.Parse(tl[1]);

                    // Check Screen ! * To Do * Check Screen Dimensions *
                }
            }
            catch (Exception ex) { App.log.Error("Error Loading Settings Window Settings", ex); }       
        }

        /// <summary>
        /// Set the Top N' Left for the Settings Window
        /// </summary>
        /// <param name="Top">Top Double</param>
        /// <param name="Left">Left Double</param>
        internal void SetWindowSetting_TopLeft_SettingsWindow(double Top, double Left)
        {
            if (!Double.IsNaN(Top) && !Double.IsNaN(Left))
                App.inifile.SetKeyValue(App.Ini_Setting.WindowSettings__SettingsWindow_TopLeft, (Top.ToString() + ";" + Left.ToString()));
        }

        /// <summary>
        /// Retrieve the Top N' Left for the Settings Window
        /// </summary>
        /// <param name="Top">Top Double</param>
        /// <param name="Left">Left Double</param>        
        internal void GetWindowSetting_TopLeft_LogViewerWindow(ref double Top, ref double Left)
        {
            // Load the Window at the correct position
            try
            {
                string TopLeft = App.inifile.GetKeyValue(App.Ini_Setting.WindowSettings__LogViewerWindow_TopLeft, String.Empty);
                if (!String.IsNullOrEmpty(TopLeft))
                {
                    string[] tl = TopLeft.Split(';');
                    Top = double.Parse(tl[0]);
                    Left = double.Parse(tl[1]);

                    // Check Screen ! * To Do * Check Screen Dimensions *
                }
            }
            catch (Exception ex) { App.log.Error("Error Loading Settings Window Settings", ex); }
        }

        /// <summary>
        /// Set the Top N' Left for the Settings Window
        /// </summary>
        /// <param name="Top">Top Double</param>
        /// <param name="Left">Left Double</param>
        internal void SetWindowSetting_TopLeft_LogViewerWindow(double Top, double Left)
        {
            if (!Double.IsNaN(Top) && !Double.IsNaN(Left))
                App.inifile.SetKeyValue(App.Ini_Setting.WindowSettings__LogViewerWindow_TopLeft, (Top.ToString() + ";" + Left.ToString()));
        }

        /// <summary>
        /// Retrieve the Top N' Left for the Settings Window
        /// </summary>
        /// <param name="Top">Top Double</param>
        /// <param name="Left">Left Double</param>        
        internal void GetWindowSetting_TopLeft_AboutWindow(ref double Top, ref double Left)
        {
            // Load the Window at the correct position
            try
            {
                string TopLeft = App.inifile.GetKeyValue(App.Ini_Setting.WindowSettings__AboutWindow_TopLeft, String.Empty);
                if (!String.IsNullOrEmpty(TopLeft))
                {
                    string[] tl = TopLeft.Split(';');
                    Top = double.Parse(tl[0]);
                    Left = double.Parse(tl[1]);

                    // Check Screen ! * To Do * Check Screen Dimensions *
                }
            }
            catch (Exception ex) { App.log.Error("Error Loading Settings Window Settings", ex); }
        }

        /// <summary>
        /// Set the Top N' Left for the Settings Window
        /// </summary>
        /// <param name="Top">Top Double</param>
        /// <param name="Left">Left Double</param>
        internal void SetWindowSetting_TopLeft_AboutWindow(double Top, double Left)
        {
            if (!Double.IsNaN(Top) && !Double.IsNaN(Left))
                App.inifile.SetKeyValue(App.Ini_Setting.WindowSettings__AboutWindow_TopLeft, (Top.ToString() + ";" + Left.ToString()));
        }

        #endregion

        #region Special Circumstances

        /// <summary>
        /// HL7Messaging puts itself in the Windows\Run to start-up automatically,
        /// If we monitor HL7Messaging then we should remove that key * Always * if we are to
        /// start with windows. ~This function get's called in those instances where we should
        /// check exactly that, and deal with exactly that issue.
        /// </summary>
        internal void DeleteOffendingHL7KeyIfApplicable()
        {
            bool bStartWithWindowsIsSet = App.inifile.GetKeyValue(App.Ini_Setting.MonitorSettings__Start_With_Windows, true);
            if (bStartWithWindowsIsSet)
            {
                WatchdogConfiguration config = App.xmlfile.ReadData(false);
                bool bHasHL7MessagingConfigured = (config.MonitoredProcesses.GetProcessExesByProcessExeFileNameWithoutExtension("HL7Messaging").Length > 0);
                if (bHasHL7MessagingConfigured) // Make sure the Key is Deleted
                {
                    if (!RegKey.DeleteKey("Microsoft\\Windows\\CurrentVersion\\Run", "HL7Messenger"))
                        App.log.Error("Failed to write to Microsoft\\Windows\\CurrentVersion\\Run. Check Permissions.");
                }
            }
        }

        #endregion
    }
}
