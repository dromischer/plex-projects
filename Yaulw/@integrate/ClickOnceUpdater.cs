﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Deployment.Application;
using Forms = System.Windows.Forms;
using System.Windows;
using System.Diagnostics;
using Watchdog.WatchdogLib.WinForms;
using System.IO;

namespace Watchdog
{
    /// <summary>
    /// Responsible for Dynamically Checking if there is an update available via ClickOnce
    /// </summary>
    public static class ClickOnceUpdater
    {
        /// <summary>
        /// Construction
        /// </summary>
        static ClickOnceUpdater(){}

        /// <summary>
        /// Installs the Update and Restarts the Current Instance
        /// </summary>
        /// <returns>false if an error occured, if succesful * Will Restart the App *</returns>
        internal static bool InstallUpdateAndRestartIfSuccessful()
        {
            try
            {
                UpdateCheckInfo info = null;
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                if (ad != null)
                {
                    // Log this to make sure we are sane
                    App.log.Info("Success in retrieving Application Deployment Manifest. This is a ClickOnce App");

                    try
                    {
                        info = ad.CheckForDetailedUpdate();
                    }
                    catch (DeploymentDownloadException dde)
                    {
                        MsgBox.ShowInfo("The new version of the application cannot be downloaded at this time. \n\nPlease check your network connection, or try again later. Error: " + dde.Message, "Unable to Download", System.Windows.Forms.MessageBoxButtons.OK);
                        App.log.Info("The new version of the application cannot be downloaded at this time. \n\nPlease check your network connection, or try again later. Error: " + dde.Message);
                        return false;
                    }
                    catch (InvalidDeploymentException ide)
                    {
                        MsgBox.ShowError("Cannot check for a new version of the application. The ClickOnce deployment is corrupt. Please redeploy the application and try again. Error: " + ide.Message, "Invalid Deployment", System.Windows.Forms.MessageBoxButtons.OK);
                        App.log.Error("Cannot check for a new version of the application. The ClickOnce deployment is corrupt. Please redeploy the application and try again. Error: " + ide.Message);
                        return false;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        MsgBox.ShowError("This application cannot be updated. It is likely not a ClickOnce application. Error: " + ioe.Message, "Invalid Operation", System.Windows.Forms.MessageBoxButtons.OK);
                        App.log.Error("This application cannot be updated. It is likely not a ClickOnce application. Error: " + ioe.Message);
                        return false;
                    }

                    if (info.UpdateAvailable)
                    {
                        Boolean doUpdate = true;

                        if (!info.IsUpdateRequired)
                        {
                            Forms.DialogResult dr = MsgBox.ShowInfo("An update is available. Would you like to update\nthe application now?", "Update Available", Forms.MessageBoxButtons.OKCancel);
                            if (!(Forms.DialogResult.OK == dr))
                                doUpdate = false;
                        }
                        else
                        {
                            // Display a message that the app MUST reboot. Display the minimum required version.
                            MsgBox.ShowInfo("This application has detected a mandatory update from your\ncurrent " +
                                "version to version " + info.MinimumRequiredVersion.ToString() +
                                ".\nThe application will now install\nthe update and restart.",
                                "Update Available", System.Windows.Forms.MessageBoxButtons.OK);
                            App.log.Info("This application has detected a mandatory update from your current " +
                                "version to version " + info.MinimumRequiredVersion.ToString() +
                                ". The application will now install the update and restart.");
                        }

                        if (doUpdate)
                        {
                            try
                            {
                                ad.Update();
                                App.log.Info("The application has been upgraded,and will now restart.");

                                // Restart the Application * Imp! Auto-Delay the Start of the new Watchdog Instance *
                                if(File.Exists(App.APPLICATION_CLICKONCE_STARTMENU_LINK))
                                   Process.Start(App.APPLICATION_CLICKONCE_STARTMENU_LINK, "15");

                                // Auto-Update * No Need to Close Applications *
                                App.State_SpecialCircum_DontCloseApplicationsOnExit();
                                Application.Current.Shutdown();
                                return true;
                            }
                            catch (DeploymentDownloadException dde)
                            {
                                MsgBox.ShowError("Cannot install the latest version of the application. \n\nPlease check your network connection, or try again later. Error: " + dde, "Update Error", System.Windows.Forms.MessageBoxButtons.OK);
                                App.log.Error("Cannot install the latest version of the application. \n\nPlease check your network connection, or try again later. Error: " + dde);
                                return false;
                            }
                        }
                    }
                    else
                    {
                        App.log.Info("Newer Version not available at this time.");
                        MsgBox.ShowInfo("A newer version of the application\nis not available at this time. \n\nPlease try again later.", "Newer Version not available", System.Windows.Forms.MessageBoxButtons.OK);
                    }
                }

            }
            catch (Exception) { /* ignore */ }
            return false;
        }

    }
}
