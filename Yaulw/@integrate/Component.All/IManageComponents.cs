﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Component.All
{
    /// <summary>
    /// Used by the Binary and Setup Component Manager,
    /// could possible be reused later for a new Component Manager
    /// </summary>
    public interface IManageComponents
    {
        /// <summary>
        /// Quick Check to see if newer Components are available
        /// </summary>        
        /// <returns>true, if newer components are available, false otherwise</returns>
        bool AreNewerComponentsAvailable();

        /// <summary>
        /// Retrieves the list of newer components that need to be installed
        /// </summary>
        /// <param name="setupEventObj">Setup Event Object is passed, for component manager to pass down to it's components</param>
        /// <returns>a list of newer components to install, null otherwise</returns>
        IInstallComponent[] GetNewerComponents(ref SetupEvents setupEventObj);

        /// <summary>
        /// Retrieves the list of all installed components, in order to uninstall
        /// </summary>
        /// <param name="ComponentsSeperatedbySemiColonOrBlankForAll">Specify Component Filter</param>
        /// <param name="setupEventObj">Setup Event Object is passed, for component manager to pass down to it's components</param>
        /// <returns>a list of all installed components, null otherwise</returns>
        IInstallComponent[] GetAllInstalledComponents(string ComponentsSeperatedbySemiColonOrBlankForAll, ref SetupEvents setupEventObj);

        /// <summary>
        /// Retrieves the list of all installed components
        /// </summary>
        /// <returns>a list of all installed components, null otherwise</returns>
        ComponentConfig.Component[] GetAllInstalledComponents();

        /// <summary>
        /// Retrieves the list of all installed componentsW
        /// </summary>
        /// <returns>a list of all installed componentsW, null otherwise</returns>
        ComponentConfig.ComponentW GetAllInstalledComponentsW();

        /// <summary>
        /// Retrieves the list of all Embedded components
        /// </summary>
        /// <returns>a list of all embedded components, null otherwise</returns>
        ComponentConfig.Component[] GetAllEmbeddedComponents();        
    }
}
