﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Component.All
{
    /// <summary>
    /// All Components that install themselves must support this interface
    /// </summary>
    public interface IInstallComponent
    {
        /// <summary>
        /// Get the Corresponding Component for this IInstallComponent
        /// </summary>
        /// <returns>the component for this IInstallCompoent</returns>
        ComponentConfig.Component GetComponent();
       
        /// <summary>
        /// Used to determine if the component is needed to install
        /// </summary>
        /// <returns>true to continue to Install, false otherwise</returns>
        bool BEFORE_INSTALLING_COMPONENT();
        
        /// <summary>
        /// Used to install a component
        /// </summary>
        /// <returns>true, if install was successful, false otherwise</returns>
        bool INSTALL_COMPONENT();
        
        /// <summary>
        /// Used to do any validation after install occured
        /// </summary>
        /// <returns>true, if validation was successful, false otherwise</returns>
        bool AFTER_INSTALLING_COMPONENT();

        /// <summary>
        /// Used to determine if the component supports uninstalling
        /// </summary>
        /// <returns>true, if component supports uninstalling, false otherwise</returns>
        bool SUPPORTS_UNINSTALL();        

        /// <summary>
        /// Used to determine if the component is needed to uninstall
        /// </summary>
        /// <returns>true to continue to uninstall, false otherwise</returns>
        bool BEFORE_UNINSTALLING_COMPONENT();

        /// <summary>
        /// Used to uninstall a component
        /// </summary>
        /// <returns>true, if uninstall was successful, false otherwise</returns>
        bool UNINSTALL_COMPONENT();

        /// <summary>
        /// Used to do any validation after uninstall occured
        /// </summary>
        /// <returns>true, if validation was successful, false otherwise</returns>
        bool AFTER_UNINSTALLING_COMPONENT();
    }
}
