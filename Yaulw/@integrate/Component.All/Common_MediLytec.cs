﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Component.All
{
    /// <summary>
    /// Install Configuration
    /// </summary>
    public enum InstallConfig
    {
        LytecMD,
        MedisoftClinical,
    }

    /// <summary>
    /// Common Functions and Helpers Useful for Lytec, Medisoft Installing activities
    /// </summary>
    public static class Common_MediLytec
    {
        /// <summary>
        /// Common/Ofted used #Defs/Definitions associated with Lytec/Medisoft
        /// </summary>
        public static class MediLytecPoundDef
        {
            public const string BRIDGE_SERVICE_TITLE = "BridgeService";
            public const string BRIDGE_SERVICE_ASSEMBLY = "BridgeService.exe";
            public const string BRIDGE_SERVICE_NAME = "McKesson Bridge Service";
            public const string MIRTH_SERVICE_NAME = "Mirth Connect Service";
            public const string POSTGRE_SERVICE_NAME = "Mirth_Connect_PostgreSQL_Server";
            public const string POSTGRE_SERVER_PORT = "5432";
        }

        /// <summary>
        /// Retrieve the Configuration (Lytec/Medisoft) from the Registry
        /// </summary>
        /// <returns>Lytec/Medisoft</returns>
        public static InstallConfig RetrieveInstallConfigFromRegistry()
        {
            bool bIsLytecInstalled = false;
            try 
            { 
                RegistryKey reg = Registry.LocalMachine.OpenSubKey("Software\\Lytec", false);
                bIsLytecInstalled = (reg != null);
                if (!bIsLytecInstalled) // also check Wow64
                {
                    reg = Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\Lytec", false);
                    bIsLytecInstalled = (reg != null);
                }

            }
            catch (Exception) { /* ignore */ }

            if (bIsLytecInstalled)
                return InstallConfig.LytecMD;
            else
                return InstallConfig.MedisoftClinical;
        }
    }
}
