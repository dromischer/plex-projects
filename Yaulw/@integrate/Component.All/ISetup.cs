﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Component.All
{
    /// <summary>
    /// Class is passed into ISetup ComponentLoaded() giving each component
    /// a chance to subscribe to it
    /// </summary>
    public class SetupEvents
    {
        #region Events

        /// <summary>
        /// Delegate used to handle Setup Events
        /// </summary>        
        public delegate void SetupEvent();
        
        /// <summary>
        /// Called after the entire Setup Application ends
        /// </summary>
        public event SetupEvent Ending_Setup;

        /// <summary>
        /// Called before the entire Install Process Begins
        /// </summary>
        public event SetupEvent Before_Install;

        /// <summary>
        /// Called after the entire Install Process ends
        /// </summary>
        public event SetupEvent After_Install;

        /// <summary>
        /// Called before the entire Uninstall Process Begins
        /// </summary>
        public event SetupEvent Before_Uninstall;

        /// <summary>
        /// Called after the entire Uninstall Process ends
        /// </summary>
        public event SetupEvent After_Uninstall;

        #endregion

        # region Event Raisers

        /// <summary>
        /// Raise the Ending Setup Event
        /// </summary>
        public void Raise_Ending_Setup_Event()
        {
            if (Ending_Setup != null)
                Ending_Setup();
        }

        /// <summary>
        /// Raise the Before Install Event
        /// </summary>
        public void Raise_Before_Install_Event()
        {
            if (Before_Install != null)
                Before_Install();
        }

        /// <summary>
        /// Raise the After Install Event
        /// </summary>
        public void Raise_After_Install_Event()
        {
            if (After_Install != null)
                After_Install();
        }

        /// <summary>
        /// Raise the Before Uninstall Event
        /// </summary>
        public void Raise_Before_Uninstall_Event()
        {
            if (Before_Uninstall != null)
                Before_Uninstall();
        }

        /// <summary>
        /// Raise the After Uninstall Event
        /// </summary>
        public void Raise_After_Uninstall_Event()
        {
            if (After_Uninstall != null)
                After_Uninstall();
        }

        #endregion
    }

    /// <summary>
    /// All Components that install themselves will implement this interface, if they are interested to 
    /// listen to / Handle Setup Events
    /// </summary>
    public interface ISetup
    {
        /// <summary>
        /// Called when the Component is loaded. Components are only loaded
        /// when an Install on them is iminent. The component has a chance to
        /// listen to/handle events in the setup process.
        /// </summary>
        void ComponentLoaded(ref SetupEvents subscribeToDesiredEvents);
    }

}
