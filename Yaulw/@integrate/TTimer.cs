﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Threading;
using WinThread = System.Threading;

namespace WatchdogLib.Thread
{
    /// <summary>
    /// Wrapper class around Timer Objects
    /// </summary>
    public class TTimer : IDisposable
    {
        // private Members
        private Timer _Timer = new Timer();
        private bool _disposed = false;
        private Dispatcher _Dispatcher = null;
        private ElapsedEventHandler _DispatchedElapsedEvent = null;

        /// <summary>
        /// Creates a new Multi-threaded System.Timer
        /// </summary>
        /// <param name="ElapsedHandler">Event Handler for Timer</param>
        /// <param name="IntervalMiliseconds">Interval in Miliseconds</param>
        /// <param name="StartEnabled">True to start the timer upon creation, false otherwise</param>
        /// <returns>A Timer Object, which should be Disposed by Caller</returns>
        //public TTimer(ElapsedEventHandler ElapsedHandler, int IntervalMiliseconds = 1000, bool StartEnabled = false, bool bUseDispatcher = true)
        public TTimer(ElapsedEventHandler ElapsedHandler, int IntervalMiliseconds, bool StartEnabled, bool bUseDispatcher)
        {
            if (ElapsedHandler != null)
            {                
                _Timer = new System.Timers.Timer();

                // The Primary Dispatcher thread is the thread that called us
                if (bUseDispatcher)
                {
                    _Dispatcher = Dispatcher.CurrentDispatcher;
                    _DispatchedElapsedEvent = ElapsedHandler;
                    _Timer.Elapsed += new ElapsedEventHandler(_Timer_Elapsed);                    
                }
                else
                {
                    _Timer.Elapsed += ElapsedHandler;
                }
                
                // Set the Interval / start
                _Timer.Interval = IntervalMiliseconds;
                _Timer.Enabled = StartEnabled;
                if (StartEnabled)
                    _Timer.Start();

                // Keep the timer alive 
                GC.KeepAlive(_Timer);
            }
        }

        /// <summary>
        ///  For Dispatching the Event to the Primary Dispatcher Thread
        /// </summary>        
        void _Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            object[] param_s = new object[] { sender, e };
            _Dispatcher.Invoke((ElapsedEventHandler)_DispatchedElapsedEvent, param_s);
        }

        /// <summary>
        /// Manually Start the Timer
        /// </summary>
        public void Start()
        {
            Stop(); // First Stop(), an existing Timer
            _Timer.Enabled = true;
            _Timer.Start();
        }

        /// <summary>
        /// Manually Start the Timer at a new Interval
        /// </summary>
        /// <param name="IntervalMiliseconds">Interval in Miliseconds</param>
        public void Start(uint IntervalMiliseconds)
        {
            Stop(); // First Stop(), an existing Timer
            _Timer.Interval = IntervalMiliseconds;
            _Timer.Enabled = true;
            _Timer.Start(); 
        }

        /// <summary>
        /// Manually Start the Timer at a new Interval
        /// </summary>
        /// <param name="tsInterval">Interval as a TimeSpan</param>
        public void Start(TimeSpan tsInterval)
        {
            Stop(); // First Stop(), an existing Timer
            _Timer.Interval = tsInterval.TotalMilliseconds;
            _Timer.Enabled = true;
            _Timer.Start();
        }

        /// <summary>
        /// Manually Stop the Timer
        /// </summary>
        public void Stop()
        {
            _Timer.Enabled = false;
            _Timer.Stop();
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass
            // of this type implements a finalizer
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_Timer != null)
                        _Timer.Dispose();
                }

                // Indicate that the instance has been disposed.
                _Timer = null;
                _disposed = true;
            }
        }

        #endregion
    }
}
