﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using WatchdogLib;
using Watchdog.WatchdogLib.WinForms;
using Watchdog.WatchdogLib.Win32;

namespace Watchdog
{
    /// <summary>
    /// Handles CommandLine Logic
    /// </summary>
    public static class CMDlineHandler
    {
        // Our WCF Client object stub
        private static IWatchdog _proxyObj = null;

        /// <summary>
        /// Shows the Command Line Help either as a Windows Form or
        /// to the Console * Depending if a Console Window is Attached *
        /// </summary>
        private static void ShowCommandLineHelp()
        {
            bool bToConsole = App.State_SpecialCircum_IsConsoleWindowIsAttached();
            string cmdHelpText = "";

            cmdHelpText += "\n";
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "           Make changes to the .xml Configuration.\n";
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "-Add_Process \"Application Exe;Prms;WorkingDir\"\n";
            cmdHelpText += "-Remove_Process \"Application Exe;Prms\"\n";           
            cmdHelpText += "-Add_Service \"Service Name\"\n";            
            cmdHelpText += "-Remove_Service \"Service Name\"\n";            
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "                 View the .xml Configuration.\n";
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "-Show_Processes\n";            
            cmdHelpText += "-Show_Services\n";            
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "            Manipulate a Running Monitor Instance.\n";
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "-Start\n";            
            cmdHelpText += "-Pause\n";            
            cmdHelpText += "-Restart\n";                        
            cmdHelpText += "-Restart_All\n";           
            cmdHelpText += "-Stop\n";            
            cmdHelpText += "-Stop_All\n";            
            cmdHelpText += "**************************************************************\n";
            cmdHelpText += "-? Show Help.\n\n";

            // Show Help to the User (Either to the console or a Message Window)
            if (bToConsole)
                Console.Write(cmdHelpText);
            else
                MsgBox.ShowInfo(cmdHelpText, (App.APPLICATION_NAME_SHORT + " Command Line Help"), System.Windows.Forms.MessageBoxButtons.OK);
        }

        /// <summary>
        /// Either writes out to the console or shows a message box, if we want to alert the user
        /// </summary>
        /// <param name="str">string to write out</param>
        private static void WriteOut(string str)
        {
            bool bToConsole = App.State_SpecialCircum_IsConsoleWindowIsAttached();
            if (bToConsole)
                Console.WriteLine("\n" + str);
            else
                MsgBox.ShowInfo(str, (App.APPLICATION_NAME_SHORT + " Command Line Alert"), System.Windows.Forms.MessageBoxButtons.OK);
        }

        /// <summary>
        /// Main Entry Point for CommandLine Mode * When Application is being called with
        /// CommandLine Parameters *
        /// </summary>
        static public void EnterCommandLineMode()
        {
            // Show Help?
            if (App.cmdline.ShowHelp || !App.cmdline.ParamsValid)
            {
                ShowCommandLineHelp();
                return;
            }

            // Check now using WCF...
            // Now we need to check communication with the other instance,            
            // if there isn't another process, then start/stop/restart/etc,.. won't work
            _proxyObj = WCFHost.GetWatchDogClientInterface();
            if (_proxyObj != null)
                App.State_SpecialCircum_MainMonitorInstance_CommSuccees();
            else
                App.log.Info("No other Instances found, Command-line parameter functionallity limited to making configuration changes only");

            // Now let's go thru all the CommandLine_Flags * WCF *
            DealWithApplicationStateWCFCalls_CommandLineFlags();

            // Now let's go thru all the CommandLine_Flags * Non-WCF *
            DealWithDisplayConfigurationNonWCFCalls_CommandLineFlags();

            // Now let's go thru all the CommandLine_Options * Non-WCF *
            DealWithConfigurationChanges_CommandLineOptions();

            // Done Here
            _proxyObj = null;
        }

        /// <summary>
        /// Deals with all the State Changes for the Main Application 
        /// * Via WCF * calls into main app and makes those changes
        /// </summary>
        private static void DealWithApplicationStateWCFCalls_CommandLineFlags()
        {
            // Now let's go thru all the CommandLine_Flags * WCF *
            if (App.State_SpecialCircum_DidMainMonitorInstanceCommSucceed())
            {
                try
                {
                    if (App.cmdline.GetFlagValue(App.CommandLine_Flag.START))
                    {
                        _proxyObj.StartMonitoring();
                    }
                    else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.STOP))
                    {
                        _proxyObj.StopMonitoring();
                    }
                    else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.STOP_ALL))
                    {
                        _proxyObj.StopAllMonitoring();
                    }
                    else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.RESTART))
                    {
                        _proxyObj.RestartMonitoring();
                    }
                    else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.RESTART_ALL))
                    {
                        _proxyObj.RestartAllMonitoring();
                    }
                    else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.PAUSE))
                    {
                        _proxyObj.PauseMonitoring();
                    }
                }
                catch (Exception ex)
                {
                    App.log.Error("Error Occured processing CommandLine_Flag via WCF", ex);

                    // Alert the user
                    WriteOut(ex.Message);
                }
            }
        }

        /// <summary>
        /// Deals with all the Flags that deal with displaying configuration. does this locally * in this process *
        /// View Configuration. Also handles LockWorkstation (Feature).
        /// </summary>
        private static void DealWithDisplayConfigurationNonWCFCalls_CommandLineFlags()
        {
            if (App.cmdline.GetFlagValue(App.CommandLine_Flag.LOCK))
            {
                // Hidden Feature, allows us to specify in windows start-up to lock the workstation
                if (!User32.LockWorkStation())
                {
                    App.log.Error("LockWorkstation() Failed");
                    WriteOut("LockWorkstation Failed");
                }
            }
            else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.SHOW_PROCESSES))
            {
                // write out each process
                string[] processes = WCFHost.ShowProcesses();
                StringBuilder sb = new StringBuilder();
                if (processes != null && processes.Length != 0)
                {
                    foreach (string p in processes)
                    {
                        sb.Append(p);
                        sb.Append("\n");                        
                    }
                }
                if (sb.Length > 0)
                    WriteOut(sb.ToString());
            }
            else if (App.cmdline.GetFlagValue(App.CommandLine_Flag.SHOW_SERVICES))
            {
                // write out each service
                string[] services = WCFHost.ShowServices();
                StringBuilder sb = new StringBuilder();
                if (services != null && services.Length != 0)
                {
                    foreach (string s in services)
                    {
                        sb.Append(s);
                        sb.Append("\n");
                    }
                }
                if (sb.Length > 0)
                    WriteOut(sb.ToString());
            }
        }

        /// <summary>
        /// Deals with making configuration changes. does this locally * in this process *
        /// Add/Remove Configuration
        /// </summary>
        private static void DealWithConfigurationChanges_CommandLineOptions()
        {            
            string ProcessAdd = App.cmdline.GetOptionValue<string>(App.CommandLine_Option.ADD_PROCESS, String.Empty);
            if (String.IsNullOrEmpty(ProcessAdd))
            {
                string ProcessRemove = App.cmdline.GetOptionValue<string>(App.CommandLine_Option.REMOVE_PROCESS, String.Empty);
                if (String.IsNullOrEmpty(ProcessRemove))
                {
                    string ServiceAdd = App.cmdline.GetOptionValue<string>(App.CommandLine_Option.ADD_SERVICE, String.Empty);
                    if (String.IsNullOrEmpty(ServiceAdd))
                    {
                        string ServiceRemove = App.cmdline.GetOptionValue<string>(App.CommandLine_Option.REMOVE_SERVICE, String.Empty);
                        if (!String.IsNullOrEmpty(ServiceRemove))
                        {
                            bool bRemoved = WCFHost.RemoveService(ServiceRemove);
                            if (bRemoved)
                            {
                                WriteOut(String.Format("Service '{0}' removed successfully from Configuration", ServiceRemove));
                                App.log.Info(String.Format("Service '{0}' removed successfully from Configuration", ServiceRemove));

                                // * Reload on Main Instance, if possible *
                                if (App.State_SpecialCircum_DidMainMonitorInstanceCommSucceed()) _proxyObj.ReloadConfigurationNextInterval();
                                App.xmlfile.ForceRefreshOnNext_ReadData = true;
                            }
                            else
                            {
                                WriteOut(String.Format("Service '{0}' failed to be removed from Configuration", ServiceRemove));
                                App.log.Error(String.Format("Service '{0}' failed to be removed from Configuration", ServiceRemove));
                            }
                        }
                    }
                    else
                    {
                        bool bAdded = WCFHost.AddService(ServiceAdd);
                        if (bAdded)
                        {
                            WriteOut(String.Format("Service '{0}' added successfully to Configuration", ServiceAdd));
                            App.log.Info(String.Format("Service '{0}' added successfully to Configuration", ServiceAdd));

                            // * Reload on Main Instance, if possible *
                            if (App.State_SpecialCircum_DidMainMonitorInstanceCommSucceed()) _proxyObj.ReloadConfigurationNextInterval();
                            App.xmlfile.ForceRefreshOnNext_ReadData = true;
                        }
                        else
                        {
                            WriteOut(String.Format("Service '{0}' failed to be added to Configuration", ServiceAdd));
                            App.log.Error(String.Format("Service '{0}' failed to be added to Configuration", ServiceAdd));
                        }
                    }
                }
                else
                {
                    string[] pNc = ProcessRemove.Split(';');
                    bool bRemoved = false;
                    if (pNc.Length == 1)
                        bRemoved = WCFHost.RemoveProcess(pNc[0], String.Empty);
                    else if (pNc.Length == 2)
                        bRemoved = WCFHost.RemoveProcess(pNc[0], pNc[1]);
                    if (bRemoved)
                    {
                        WriteOut(String.Format("Application '{0}' with CommandLinePrms '{1}' removed successfully from Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));
                        App.log.Info(String.Format("Application '{0}' with CommandLinePrms '{1}' removed successfully from Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));

                        // * Reload on Main Instance, if possible *
                        if (App.State_SpecialCircum_DidMainMonitorInstanceCommSucceed()) _proxyObj.ReloadConfigurationNextInterval();
                        App.xmlfile.ForceRefreshOnNext_ReadData = true;
                    }
                    else
                    {
                        WriteOut(String.Format("Application '{0}' with CommandLinePrms '{1}' failed to be removed from Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));
                        App.log.Error(String.Format("Application '{0}' with CommandLinePrms '{1}' failed to be removed from Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));
                    }
                }
            }
            else
            {
                string[] pNc = ProcessAdd.Split(';');
                bool bAdded = false;
                if(pNc.Length == 1)
                   bAdded = WCFHost.AddProcess(pNc[0], String.Empty, String.Empty);
                else if(pNc.Length == 2)
                   bAdded = WCFHost.AddProcess(pNc[0], pNc[1], String.Empty);
                else if (pNc.Length == 3)
                   bAdded = WCFHost.AddProcess(pNc[0], pNc[1], pNc[2]);
                if (bAdded)
                {
                    WriteOut(String.Format("Application '{0}' with CommandLinePrms '{1}' added successfully to Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));
                    App.log.Info(String.Format("Application '{0}' with CommandLinePrms '{1}' added successfully to Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));

                    // * Reload on Main Instance, if possible *
                    if (App.State_SpecialCircum_DidMainMonitorInstanceCommSucceed()) _proxyObj.ReloadConfigurationNextInterval();
                    App.xmlfile.ForceRefreshOnNext_ReadData = true;
                }
                else
                {
                    WriteOut(String.Format("Application '{0}' with CommandLinePrms '{1}' failed to be added to Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));
                    App.log.Error(String.Format("Application '{0}' with CommandLinePrms '{1}' failed to be added to Configuration", pNc[0], (pNc.Length > 1) ? pNc[1] : ""));
                }
            }
        }

    }
}
