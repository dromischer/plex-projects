﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Component.All;
using Microsoft.Win32;

namespace Component.Binary.Java
{
    public class InstallComponent : IInstallComponent , ISetup
    {
        #region IInstallComponent Members

        /// <summary>
        /// Get the Component bound to this IInstallComponent
        /// </summary>
        /// <returns></returns>
        public ComponentConfig.Component GetComponent()
        {
            return Common.EmbeddedConfig.BinaryComponents.GetComponent("Java");
        }

        /// <summary>
        /// We only want to install Java if an earlier version is installed
        /// </summary>
        /// <returns>true, if an earlier version or none is installed, false otherwise</returns>
        public bool BEFORE_INSTALLING_COMPONENT()
        {
            // If we get here, just in case always just install the java version
            // to make sure everything works as expected

            //string Version = GetLatestJavaVersionFromTheRegistry();
            //if (!String.IsNullOrEmpty(Version))
            //{
            //    int nCompare = String.Compare(Version, GetComponent().Version);
            //    if (nCompare < 0)
            //        return true;
            //    else
            //        return false;
            //}
            return true;
        }

        /// <summary>
        /// Installs Java on the system
        /// </summary>
        /// <returns>Returns true if the Component installs, false otherwise</returns>
        public bool INSTALL_COMPONENT()
        {
            Common.PSetupSpwan(GetComponent().TempFileNamesNPath[0], "/s /v \"/qn ADDLOCAL=ALL IEXPLORER=1 REBOOT=Supress\"");
            return true;
        }

        /// <summary>
        /// Make sure that the version in the registry matches the component
        /// </summary>
        /// <returns>true, if the version in the registry matches what we installed, false otherwise</returns>
        public bool AFTER_INSTALLING_COMPONENT()
        {
            string Version = GetLatestJavaVersionFromTheRegistry();
            if (!String.IsNullOrEmpty(Version))
            {
                int nCompare = String.Compare(GetComponent().Version, Version);
                if (nCompare != 0)
                    return false;
                else
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Check to see, if this component supports uninstalling
        /// </summary>
        /// <returns>true, if uninstall is supported, false otherwise</returns>
        public bool SUPPORTS_UNINSTALL()
        {
            return true;
        }

        /// <summary>
        /// Check if we can uninstall the java version
        /// </summary>
        /// <returns>true, if version is installed, false otherwise</returns>
        public bool BEFORE_UNINSTALLING_COMPONENT()
        {            
            string[] Versions = GetAllJavaVersionFromTheRegistry();
            if (Versions != null && Versions.Length > 0)
            {
                if (Versions.Contains(GetComponent().Version))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Uninstalls Java on the system
        /// </summary>
        /// <returns>Returns true if the Component installs, false otherwise</returns>
        public bool UNINSTALL_COMPONENT()
        {
            Common.PSetupSpwan(GetComponent().TempFileNamesNPath[0], "/s /v \"/qn REBOOT=Supress\" /x");
            return true;
        }

        /// <summary>
        /// Check to make sure that the version we uninstalled doesn't exist anymore
        /// </summary>
        /// <returns>true if version was removed, false otherwise</returns>
        public bool AFTER_UNINSTALLING_COMPONENT()
        {
            string[] Versions = GetAllJavaVersionFromTheRegistry();
            if (Versions != null && Versions.Length > 0)
            {
                if (Versions.Contains(GetComponent().Version))
                    return false;
            }
            return true;
        }

        #endregion

        #region ISetup Members

        public void ComponentLoaded(ref SetupEvents subscribeToDesiredEvents)
        {
            subscribeToDesiredEvents.Before_Install += new SetupEvents.SetupEvent(subscribeToDesiredEvents_Before_Install);
            subscribeToDesiredEvents.After_Install += new SetupEvents.SetupEvent(subscribeToDesiredEvents_After_Install);            
        }

        void subscribeToDesiredEvents_Before_Install()
        {         
        }

        void subscribeToDesiredEvents_After_Install()
        {         
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// Use this function to check the java version in the registry
        /// </summary>
        /// <returns>The Java Version found in the registry or String.Empty, if not found</returns>
        string GetLatestJavaVersionFromTheRegistry()
        {
            string[] Versions = GetAllJavaVersionFromTheRegistry();
            if (Versions != null && Versions.Length > 0)
            {
                string strVersion = Versions[Versions.Length - 1];
                if (!String.IsNullOrEmpty(strVersion))
                    return strVersion;
            }            
            return String.Empty;
        }

        /// <summary>
        /// Use this function to check all the java versions in the registry
        /// </summary>
        /// <returns>The Java Versions found in the registry or null, if none found</returns>
        string[] GetAllJavaVersionFromTheRegistry()
        {
            bool bIsJavaInstalled = false;
            try
            {
                RegistryKey reg = Registry.LocalMachine.OpenSubKey("Software\\JavaSoft\\Java Plug-in", false);
                bIsJavaInstalled = (reg != null);
                if(reg == null)
                    reg = Registry.LocalMachine.OpenSubKey("Software\\Wow6432Node\\JavaSoft\\Java Plug-in", false);
                bIsJavaInstalled = (reg != null);
                if (bIsJavaInstalled)
                {
                    string[] SubKeys = reg.GetSubKeyNames();
                    if (SubKeys != null && SubKeys.Length > 0)
                        return SubKeys;
                }
            }
            catch (Exception) { /* ignore */ }
            return null;
        }

        #endregion
    }
}
