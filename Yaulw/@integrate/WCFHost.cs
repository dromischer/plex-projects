﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Watchdog.WatchdogLib.Monitor;
using Watchdog.WatchdogLib.WinForms;

namespace Watchdog
{
    /// <summary>
    /// WCF Operations Contract in order to allow the command-line process
    /// to communicate with the running Application's instance
    /// </summary>
    [ServiceContract]
    public interface IWatchdog
    {  
        #region Application State Changes

        /// <summary>
        /// Starts Monitoring
        /// </summary>
        [OperationContract]
        void StartMonitoring();

        /// <summary>
        /// Pauses Monitoring
        /// </summary>
        [OperationContract]
        void PauseMonitoring();

        /// <summary>
        /// Restarts Monitoring
        /// </summary>
        [OperationContract]
        void RestartMonitoring();

        /// <summary>
        /// Restarts All Monitoring
        /// </summary>
        [OperationContract]
        void RestartAllMonitoring();

        /// <summary>
        /// Stops Monitoring
        /// </summary>
        [OperationContract]
        void StopMonitoring();

        /// <summary>
        /// Stops All Monitoring
        /// </summary>
        [OperationContract]
        void StopAllMonitoring();

        /// <summary>
        /// Tell the Running Application to Reload the Configuration
        /// </summary>
        [OperationContract]
        void ReloadConfigurationNextInterval();

        #endregion

        #region Ping
        
        /// <summary>
        /// To Check if we can communicate with the other side
        /// </summary>
        /// <returns>PING_RETURN_VALUE</returns>
        [OperationContract]
        int Ping();        

        #endregion
    }

    /// <summary>
    /// Responsible for the WCF Callbacks
    /// </summary>
    public class WCFHost : IWatchdog
    {        
        #region IWatchdog Members

        /// <summary>
        /// Starts Monitoring
        /// </summary>
        public void StartMonitoring()
        {
            HiddenMainWindow.StartMonitoring(true, true);
        }

        /// <summary>
        /// Pauses Monitoring
        /// </summary>
        public void PauseMonitoring()
        {
            HiddenMainWindow.PauseMonitoring(true);
        }

        /// <summary>
        /// Restarts Monitoring
        /// </summary>
        public void RestartMonitoring()
        {
            HiddenMainWindow.RestartMonitoring(4, true);
        }

        /// <summary>
        /// Restarts All Monitoring
        /// </summary>
        public void RestartAllMonitoring()
        {
            HiddenMainWindow.RestartAllMonitoring(4, true);
        }

        /// <summary>
        /// Stops Monitoring
        /// </summary>
        public void StopMonitoring()
        {
            HiddenMainWindow.StopMonitoring(true, false);
        }

        /// <summary>
        /// Stops All Monitoring
        /// </summary>
        public void StopAllMonitoring()
        {
            HiddenMainWindow.StopAllMonitoring(true);
        }

        /// <summary>
        /// Tell the Running Application to Reload the Configuration
        /// </summary>
        public void ReloadConfigurationNextInterval()
        {
            App.xmlfile.ForceRefreshOnNext_ReadData = true;
        }

        /// <summary>
        /// Const Value to return on Ping
        /// </summary>
        public const int PING_RETURN_VALUE = 10;
        /// <summary>
        /// To Check if we can communicate with the other side
        /// </summary>
        /// <returns>PING_RETURN_VALUE</returns>
        public int Ping()
        {
            return PING_RETURN_VALUE;
        }

        #endregion

        #region Configuration Changes / Display Configuration Statics

        /// <summary>
        /// Adds a Process from the Configuration and saves the xml
        /// </summary>
        /// <param name="ProcessExeFileNameNPath">Process Exe File name</param>
        /// <param name="CommandlinePrms">Command Line Prms for Process</param>
        /// <param name="WorkingDir">Working Directory</param>
        /// <returns>true if successfull, false otherwise</returns>
        public static bool AddProcess(string ProcessExeFileNameNPath, string CommandlinePrms, string WorkingDir)
        {
            WatchdogConfiguration config = App.xmlfile.ReadData(false);
            bool bSuccess = config.MonitoredProcesses.AddProcessExe(ProcessExeFileNameNPath, CommandlinePrms, WorkingDir);
            if (bSuccess)
                App.xmlfile.SaveData();
            return bSuccess;
        }

        /// <summary>
        /// Removes a Process from the Configuration and saves the xml
        /// </summary>
        /// <param name="ProcessExeFileNameNPath">Process Exe File name</param>
        /// <param name="CommandlinePrms">Command Line Prms for Process</param>
        /// <returns>true if successfully deleted a process with matching ProcessExeFileNameNPath and CommandlinePrms</returns>
        public static bool RemoveProcess(string ProcessExeFileNameNPath, string CommandlinePrms)
        {
            WatchdogConfiguration config = App.xmlfile.ReadData(false);
            bool bSuccess = config.MonitoredProcesses.RemoveProcessExe(ProcessExeFileNameNPath, CommandlinePrms);
            if (bSuccess)
                App.xmlfile.SaveData();
            return bSuccess;
        }

        /// <summary>
        /// Adds a Service from the configuration and saves the xml
        /// </summary>
        /// <param name="ServiceName">Name of Service</param>
        /// <returns>true if sucessfull, false otherwise</returns>
        public static bool AddService(string ServiceName)
        {
            WatchdogConfiguration config = App.xmlfile.ReadData(false);
            bool bSuccess = config.MonitoredServices.AddServiceExe(ServiceName);
            if (bSuccess)
                App.xmlfile.SaveData();
            return bSuccess;
        }

        /// <summary>
        /// Removes a Service from the configuration and saves the xml
        /// </summary>
        /// <param name="ServiceName">Name of Service</param>
        /// <returns>true if sucessfull, false otherwise</returns>
        public static bool RemoveService(string ServiceName)
        {
            WatchdogConfiguration config = App.xmlfile.ReadData(false);
            bool bSuccess = config.MonitoredServices.RemoveServiceExe(ServiceName);
            if (bSuccess)
                App.xmlfile.SaveData();
            return bSuccess;
        }

        /// <summary>
        /// Returns an array of all Processes and their configurations (in a ';' seperated string)
        /// </summary>
        /// <returns>Process List with configuration</returns>
        public static string[] ShowProcesses()
        {
            List<string> ProcessList = new List<String>();
            WatchdogConfiguration config = App.xmlfile.ReadData(false);
            string s = String.Empty;
            foreach (WatchdogConfiguration.ProcessExe p in config.MonitoredProcesses.ProcessExes)
            {
                if (String.IsNullOrEmpty(p.CommandLinePrms) && String.IsNullOrEmpty(p.WorkingDirectory))
                    s = p.ProcessExeFileNameNPath;
                else if (!String.IsNullOrEmpty(p.CommandLinePrms) && String.IsNullOrEmpty(p.WorkingDirectory))
                    s = p.ProcessExeFileNameNPath + ";" + p.CommandLinePrms;
                else if (!String.IsNullOrEmpty(p.CommandLinePrms) && !String.IsNullOrEmpty(p.WorkingDirectory))
                    s = p.ProcessExeFileNameNPath + ";" + p.CommandLinePrms + ";" + p.WorkingDirectory;
                else
                    continue;
                ProcessList.Add(s);
            }
            return ProcessList.ToArray();
        }

        /// <summary>
        /// Returns an array of all Services
        /// </summary>
        /// <returns>Services List</returns>
        public static string[] ShowServices()
        {
            List<string> ProcessList = new List<String>();
            WatchdogConfiguration config = App.xmlfile.ReadData(false);
            foreach (WatchdogConfiguration.ServiceExe s in config.MonitoredServices.ServiceExes)
                     ProcessList.Add(s.Name);
            return ProcessList.ToArray();
        }

        #endregion

        #region Internal Statics

        private static ServiceHost _host = null;

        /// <summary>
        /// Start the WCF Pipe Host (Watchdog)
        /// </summary>
        internal static void StartHost()
        {
            // Make sure it is closed
            StopHost();
            _host = new ServiceHost(typeof(WCFHost), new Uri[] { new Uri("net.pipe://localhost") });
            _host.AddServiceEndpoint(typeof(IWatchdog), new NetNamedPipeBinding(), AppResx.GetString("APPLICATION_NAME_SHORT"));

            try
            {
                _host.Open();
            }
            catch (Exception e)
            {
                App.log.Fatal("Unable to start WCF Pipe Host", e);
                MsgBox.ShowFatalError("Unable to start WCF Pipe Host", "Pipe Host Failure", System.Windows.Forms.MessageBoxButtons.OK);
                App.Current.Shutdown();
            }
        }

        /// <summary>
        /// Stop the WCF Pipe Host
        /// </summary>
        internal static void StopHost()
        {
            if (_host != null)
            {
                _host.Close();
                _host = null;
            }
        }

        /// <summary>
        /// Called by Client to communicate with the server
        /// </summary>
        /// <returns>a valid IWatchDog Object or Null if error occured</returns>
        internal static IWatchdog GetWatchDogClientInterface()
        {                        
            // We only should be calling this in CommandLine Mode.
            if (App.State_SpecialMode_IsCommandLineSet())
            {
                try
                {
                    IWatchdog watchdoginterface = null;
                    ChannelFactory<IWatchdog> iwatchdogFactory = new ChannelFactory<IWatchdog>(new NetNamedPipeBinding(), new EndpointAddress(("net.pipe://localhost/" + AppResx.GetString("APPLICATION_NAME_SHORT"))));
                    watchdoginterface = iwatchdogFactory.CreateChannel();

                    // Try to ping the object! ~if it fails, target object doesn't exist
                    if (watchdoginterface.Ping() == WCFHost.PING_RETURN_VALUE)
                        return watchdoginterface;
                }
                catch (Exception)
                {
                    // * Ignore this error we don't want to log this*
                    //App.log.Error("Failed to obtain WatchDogClientInterface", e);
                }
            }
            return null;
        }

        #endregion
    }
}
