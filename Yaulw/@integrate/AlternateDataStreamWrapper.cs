﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PhoneHome.Lib.Assembly;

// Downloaded from
// http://www.codeproject.com/Articles/2670/Accessing-alternative-data-streams-of-files-on-an
using Trinet.Core.IO.Ntfs;
using System.IO;

namespace PhoneHome
{
    /// <summary>
    /// Wrapper class arround Alternate Data Streams
    /// * IMP * Registrate 'Later' Medisoft Demo Registration Work-arround
    /// Medisoft has a 30 day trial where they don't enter a serial number. However, this can 
    /// be bypassed, since registration is terrible at what it does. So we must make sure that 
    /// the 30 days haven't passed, and we deal with it here with PhoneHome via AlternateStreams
    /// </summary>
    internal static class AlternateDataStreamWrapper
    {
        /// <summary>
        /// Read the Timestamp found in the Alternative Stream
        /// </summary>
        /// <returns>Dt found or DT.Min (if none found)</returns>
        internal static DateTime ReadDateTimeStamp()
        {
            try
            {
                string s_curDir = Path.GetDirectoryName(AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Executing));
                SafeNativeMethods.Win32StreamInfo s_streamInfo = new SafeNativeMethods.Win32StreamInfo() { StreamAttributes = FileStreamAttributes.None, StreamName = "phdt", StreamSize = 20, StreamType = FileStreamType.Data };

                AlternateDataStreamInfo adataStream = new AlternateDataStreamInfo(s_curDir, s_streamInfo);
                using (FileStream fs = adataStream.OpenRead())
                using (StreamReader sr = new StreamReader(fs))
                {
                    string strLine = sr.ReadLine();
                    DateTime dtFound = DateTime.Parse(strLine);
                    return dtFound;
                }
            }
            catch (Exception e) { string Message = e.Message; }
            return DateTime.MinValue;
        }

        /// <summary>
        /// Write the passed in Timestamp to the Alternative Stream
        /// </summary>
        /// <param name="dtStamp">Timestamp to write</param>
        internal static void WriteDateTimeStamp(DateTime dtStamp)
        {
            try
            {
                string s_curDir = Path.GetDirectoryName(AssemblyW.SpecializedAssemblyInfo.GetAssemblyFileNameNPath(AssemblyW.AssemblyST.Executing));
                SafeNativeMethods.Win32StreamInfo s_streamInfo = new SafeNativeMethods.Win32StreamInfo() { StreamAttributes = FileStreamAttributes.None, StreamName = "phdt", StreamSize = 20, StreamType = FileStreamType.Data };

                AlternateDataStreamInfo adataStream = new AlternateDataStreamInfo(s_curDir, s_streamInfo);
                using (FileStream fs = adataStream.OpenWrite())
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(dtStamp.ToShortDateString());
                }
            }
            catch (Exception e) { string Message = e.Message; }
        }

    }

}
