﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Reflection;
using System.IO;
using Watchdog.WatchdogLib.Assembly;

namespace Watchdog
{
    // ResxTypes
    public enum Resx
    {
        AppResx,
    }

    #region ResxHelper Wrapper Classes

    /// <summary>
    /// AppResx Wrapper String Resource Value Functions
    /// </summary>
    public static class AppResx
    {
        public static string GetString(string name)
        {
            return ResxHelper.GetStringValue(Resx.AppResx, name);
        }
        public static string GetStringFormated(string name, params object[] args)
        {
            return ResxHelper.GetStringValueFormated(Resx.AppResx, name, args);
        }
    }

    #endregion

    /// <summary>
    /// Allows External Callers To Quickly gain access to the Resources Contained in this Assembly
    /// </summary>
    public static class ResxHelper
    {
        /// <summary>
        /// Private static Dictionary Map of Resource Managers
        /// </summary>
        private static Dictionary<Resx, ResourceManager> _ResourceMap = new Dictionary<Resx, ResourceManager>();

        /// <summary>
        /// Static Constructor, iterates through the enumerations and loads resourceManagers internally
        /// </summary>
        static ResxHelper()
        {
            //string[] resources = Assembly.GetExecutingAssembly().GetManifestResourceNames();
            //~we shouldn't be doing this on filename (filename could change)
            //string curAsmName = Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().ManifestModule.Name);
            string curAsmName = AssemblyW.GetAssemblyName(AssemblyW.AssemblyST.Entry);

            // Create a ResourceManager for each Resource Type in this Assembly
            foreach (string ResxName in Enum.GetNames(typeof(Resx)))
                     _ResourceMap.Add((Resx)Enum.Parse(typeof(Resx), ResxName), new ResourceManager((curAsmName + "." + ResxName), Assembly.GetExecutingAssembly()));
        }

        #region String Resource Value Functions

        /// <summary>
        /// Returns the Value of a String Resource via GetString()
        /// </summary>
        /// <param name="resx">Reource Type to Access</param>
        /// <param name="Name">Name of String Resource to get</param>
        /// <returns>the value of the resource, or "" if not found/Error Occured</returns>        
        public static string GetStringValue(Resx resx, string Name)
        {
            try
            {
                if (!String.IsNullOrEmpty(Name))
                {
                    string Value = PreserverFormating(_ResourceMap[resx].GetString(Name));
                    return Value;
                }
            }
            catch (Exception) { /* ignore */ }
            return String.Empty;
        }

        /// <summary>
        /// Returns a Formated String Value of a String Resource via GetString()
        /// </summary>
        /// <param name="resx">Reource Type to Access</param>
        /// <param name="Name">Name of String Resource to get</param>
        /// <param name="args">Arguments to pass into String.Format()</param>
        /// <returns>the value of the resource, or "" if not found/Error Occured</returns>  
        public static string GetStringValueFormated(Resx resx, string Name, params object[] args)
        {
            String retVal = GetStringValue(resx, Name);
            if (!String.IsNullOrEmpty(retVal))
                return String.Format(retVal, args);
            else
                return String.Empty;
        }

        /// <summary>
        /// we want to preserver formating using '\' characters that are in the resource
        /// </summary>
        /// <param name="Name">a string value retrieved from the resource</param>
        /// <returns>a string that preserves formatting</returns>
        private static string PreserverFormating(string Value)
        {
            if (!String.IsNullOrEmpty(Value))
            {
                Value = Value.Replace("\\N", "\n");
                Value = Value.Replace("\\n", "\n");
                Value = Value.Replace("\\T", "\t");
                Value = Value.Replace("\\t", "\t");
                return Value;
            }
            return String.Empty;
        }

        #endregion
    }
}
