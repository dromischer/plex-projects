﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Watchdog.WatchdogLib.Other;

namespace Watchdog
{
    /// <summary>
    /// Keeps Track of RunTime State Variables/Objects
    /// </summary>
    public static class AppState
    {
        private static object s_LockObject = new Object();

        #region Internal State Check Functions

        /// <returns>true if the Process Monitor is running, false otherwise</returns>
        internal static bool IsMonitorRunning()
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.ProcessMonitorStarted_bool, false);
            }
        }

        /// <returns>true if the Scheduler is running, false otherwise</returns>
        internal static bool IsSchedulerRunning()
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.SchedulerIsRunning_bool, false);
            }
        }

        /// <returns>true if the Process Monitor encountered errors, false otherwise</returns>
        internal static bool DidMonitorEncounterErrors()
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.ProcessMonitorEncounteredErrors_bool, false);
            }
        }

        /// <returns>true if the Application is called with Command-Line Parameters, false otherwise</returns>
        internal static bool IsInCommandLinePrmsMode()
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.IsInCommandLineMode_bool, false);
            }
        }

        /// <returns>true if the Application is attached to a Console Window For Output, false otherwise</returns>
        internal static bool IsAttachedToConsoleWindow()
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.IsAttachedToConsoleWindow_bool, false);
            }
        }

        /// <returns>When the application is in commandline mode, we want to make sure that a 'real' monitor instance is running,
        /// in order to communicate with it, this keeps track if that other instance is available</returns>
        internal static bool InCommandLineMode_IsMonitorInstanceAvailable()
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.InCommandLineMode_IsMonitorInstanceAvailable_bool, false);
            }
        }

        #endregion

        #region Internal State Set Functions

        /// <summary>
        /// Use this to set the Monitor is Running state to true/false
        /// </summary>
        internal static bool MonitorIsRunning(bool bIsRunning)
        {
            lock (s_LockObject)
            {
                return AppState.State.SetStateValue<bool>(AppState.StateKey.ProcessMonitorStarted_bool, bIsRunning);
            }
        }

        /// <summary>
        /// Use this to set the Scheduler is Running state to true/false
        /// </summary>
        internal static bool SchedulerIsRunning(bool bIsRunning)
        {
            lock (s_LockObject)
            {
                return AppState.State.SetStateValue<bool>(AppState.StateKey.SchedulerIsRunning_bool, bIsRunning);
            }
        }

        /// <summary>
        /// Use this to set the Monitor's Error State
        /// </summary>
        internal static bool MonitorEncounterErrors(bool bErrorsOccured)
        {
            lock (s_LockObject)
            {
                return AppState.State.SetStateValue<bool>(AppState.StateKey.ProcessMonitorEncounteredErrors_bool, bErrorsOccured);
            }
        }

        /// <summary>
        /// Use this to set that the Application is called with Command-Line Parameters
        /// </summary>
        internal static bool CommandLinePrmsMode(bool bIsInCommandLinePrmsMode)
        {
            lock (s_LockObject)
            {
                return AppState.State.SetStateValue<bool>(AppState.StateKey.IsInCommandLineMode_bool, bIsInCommandLinePrmsMode);
            }
        }

        /// <summary>
        /// Use this to set that the Application is attached to a Console Window for Output
        /// </summary>
        internal static bool AttachedToConsoleWindow(bool bItIsAttached)
        {
            lock (s_LockObject)
            {
                return AppState.State.SetStateValue<bool>(AppState.StateKey.IsAttachedToConsoleWindow_bool, bItIsAttached);
            }
        }

        /// <summary>
        /// Use this to set that the Application can communicate with the 'real' monitor instance that is running.
        /// </summary>
        internal static bool CommandLineMode_IsMonitorInstanceAvailable(bool bIsAvailable)
        {
            lock (s_LockObject)
            {
                return AppState.State.GetStateValue<bool>(AppState.StateKey.InCommandLineMode_IsMonitorInstanceAvailable_bool, bIsAvailable);
            }
        }

        #endregion

        /// <summary>
        /// Various Keys that we can use to save/get the GUI State
        /// </summary>
        internal enum StateKey
        {
            ProcessMonitorStarted_bool,
            ProcessMonitorEncounteredErrors_bool,
            SchedulerIsRunning_bool,
            IsInCommandLineMode_bool,
            IsAttachedToConsoleWindow_bool,
            InCommandLineMode_IsMonitorInstanceAvailable_bool,
        }

        // Initialize BkgdState
        internal static readonly StateM State = new StateM(typeof(StateKey));
    }
}
