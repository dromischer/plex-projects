﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using Yaulw.Tools;

namespace Yaulw.Process
{
    /// <remarks>
    /// Helper Class to create ProcessStartInfo Objects
    /// </remarks>
    public static class PStartInfo
    {
        /// <summary>
        /// Creates a simple ProcessStartInfo Object
        /// </summary>
        /// <param name="ExeFileNPath">Exe File and Path to execute</param>
        /// <param name="CmdLine">CmdLine Params to process, optional</param>
        /// <param name="bUseRunAs">If true, will set "RunAs" For the Process, to Run as Administrator</param>
        /// <param name="WindowStyle">the windows style to initialize main window with</param>
        /// <param name="bUseShellExecute">True to use Explorer.exe shellexecute, false otherise</param>
        /// <returns>a ProcessStartInfo Object or null if error occured</returns>
        public static ProcessStartInfo CreateProcess(string ExeFileNPath, string CmdLine = "", string WorkingDir = "", bool bUseRunAs = false, ProcessWindowStyle WindowStyle = ProcessWindowStyle.Normal, bool bUseShellExecute = true)
        {
            if (!String.IsNullOrEmpty(ExeFileNPath) && System.IO.File.Exists(ExeFileNPath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(ExeFileNPath, CmdLine);
                startInfo.WindowStyle = WindowStyle;
                startInfo.UseShellExecute = bUseShellExecute;
                if (bUseRunAs)
                    startInfo.Verb = "runas";

                // Set up Working Directory, if one is found
                if(!String.IsNullOrEmpty(WorkingDir))
                    startInfo.WorkingDirectory = PathNaming.PathEndsWithSlash(WorkingDir);
                else
                    startInfo.WorkingDirectory = Path.GetDirectoryName(ExeFileNPath);                                   
                return startInfo;
            }
            return null;
        }

        /// <summary>
        /// Creates an url ProcessStartInfo Object
        /// </summary>
        /// <param name="url">url to launche</param>        
        /// <returns>a ProcessStartInfo Object or null if error occured</returns>
        public static ProcessStartInfo LaunchUrl(string url)
        {
            if (!String.IsNullOrEmpty(url))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo(url);                
                startInfo.UseShellExecute = true;
                return startInfo;
            }
            return null;
        }

        /// <summary>
        /// Creates a CMD.Exe CommandLine Executable ProcessStartInfo Object
        /// </summary>
        /// <param name="scriptFileNPath">Full FileName and Path to script file to execute via CMD.exe</param>
        /// <param name="bUseRunAs">If true, will set "RunAs" For the Process, to Run as Administrator</param>
        /// <returns>a ProcessStartInfo Object or null if error occured</returns>
        public static ProcessStartInfo CreateCMDScriptProcess(string scriptFileNPath, bool bUseRunAs = false)
        {
            if (!String.IsNullOrEmpty(scriptFileNPath) && System.IO.File.Exists(scriptFileNPath))
            {
                //The "/C" Tells Windows to Run The Command then Terminate 
                string strCmdLine = "/C " + '\"' + scriptFileNPath + '\"';
                string WindowsSystem32Folder = System.Environment.GetFolderPath(Environment.SpecialFolder.System);
                ProcessStartInfo startInfo = new ProcessStartInfo((WindowsSystem32Folder + "\\" + "CMD.exe"), strCmdLine);
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                if (bUseRunAs)
                    startInfo.Verb = "runas";
                startInfo.WorkingDirectory = Path.GetDirectoryName(scriptFileNPath);
                return startInfo;
            }
            return null;
        }

        /// <summary>
        /// Creates a CMD.Exe CommandLine Executable ProcessStartInfo Object
        /// </summary>
        /// <param name="DosCommand">Dos Command to Execute</param>
        /// <param name="bUseRunAs">If true, will set "RunAs" For the Process, to Run as Administrator</param>
        /// <returns>a ProcessStartInfo Object or null if error occured</returns>
        public static ProcessStartInfo CreateCMDDosCommandProcess(string DosCommand, bool bUseRunAs = false)
        {
            if (!String.IsNullOrEmpty(DosCommand))
            {
                //The "/C" Tells Windows to Run The Command then Terminate 
                string strCmdLine = "/C " + '\"' + DosCommand + '\"';
                string WindowsSystem32Folder = System.Environment.GetFolderPath(Environment.SpecialFolder.System);
                ProcessStartInfo startInfo = new ProcessStartInfo((WindowsSystem32Folder + "\\" + "CMD.exe"), strCmdLine);
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                if (bUseRunAs)
                    startInfo.Verb = "runas";
                return startInfo;
            }
            return null;
        }
    }
}
