﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities.GenericUtilities.Structs
{
    /// <summary>
    /// String Builder Wrapper struct in order to be able to use +=
    /// </summary>
    public struct sbstring
    {
        // StringBuilder to wrapp
        private StringBuilder _sb;


        public static string operator+ (sbstring op1, string op2)
        {
            op1.SBInit();
            op1._sb.Append(op2);
            return op1._sb.ToString();
        }

        //public static sbstring operator +(sbstring op1, string op2)
        //{
        //    op1._sb.Append(op2);
        //    return op1;
        //}       

        /// <summary>
        /// Call this function to initialize the StringBuilder * must be called by all ops first *
        /// </summary>
        private void SBInit() { if (_sb == null) _sb = new StringBuilder(); }
    }
}
