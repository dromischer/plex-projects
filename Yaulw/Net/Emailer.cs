﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using webmail = System.Web.Mail;
using Yaulw.Other;

// Disable System.Web.Mail is depreciated Warning
#pragma warning disable 0618

namespace Yaulw.Net
{
    /// <remarks>
    /// Emailer Helps you send Emails using System.Net or System.Web.Mail.
    /// System.Web.Mail is depreciated, but currently is the only way to send emails via
    /// SSH/SMTP on port 465.
    /// </remarks>
    public static class Emailer
    {
        #region Public Statics

        /// <summary>
        /// Generic Function to handle Emails
        /// </summary>
        /// <param name="SmtpServer">specifiy a SMTP Server to use * Required *</param>
        /// <param name="SenderEmail">Email to use to send with * Required *</param>
        /// <param name="SenderDisplayName">Display name for Sender * Required *</param>
        /// <param name="ReceiverEmails">';' seperated list of emails to send to * Required *</param>        
        /// <param name="MessageSubject">Email Message Subject * Required *</param>
        /// <param name="MessageText">Email Message Text * Required *</param>
        /// <param name="nPort">Smtp Port to use</param>
        /// <param name="ReceiverCCEmails">';' seperated list of emails to cc to</param>
        /// <param name="bUseSmtpAuth">true to use SmtpAuthentication, false otherwise</param>
        /// <param name="SmtpUser">Smtp Authentication Username</param>
        /// <param name="SmtpPassword">Smtp Authentication Password</param>        
        /// <param name="RequiresSSL">true to use ssl, false otherwise</param>
        /// <param name="nTimeOutInSeconds">Defaults to 100 Seconds</param>
        /// <param name="exceptionFunc">If an exception occurs this delegate will get called (can be null)</param>
        /// <returns>true if the Message Send Successfully, false otherwise</returns>
        public static bool SendEmail(string SmtpServer, string SenderEmail, string SenderDisplayName, string ReceiverEmails, string MessageSubject, string MessageText, uint nPort, string ReceiverCCEmails, bool bUseSmtpAuth, string SmtpUser, string SmtpPassword, bool RequiresSSL, int nTimeOutInSeconds, DelegateCollection.Void_Param1_Exception_Func exceptionFunc)
        {
            // SSL on Port 25 is Explicit SSL, which is supported by System.Net
            if (RequiresSSL && (nPort != 25))
                return SendEmailUsingWebMail(SmtpServer, SenderEmail, SenderDisplayName, ReceiverEmails, MessageSubject, MessageText, nPort, ReceiverCCEmails, bUseSmtpAuth, SmtpUser, SmtpPassword, RequiresSSL, nTimeOutInSeconds, exceptionFunc);
            else
                return SendEmailUsingNetMail(SmtpServer, SenderEmail, SenderDisplayName, ReceiverEmails, MessageSubject, MessageText, nPort, ReceiverCCEmails, bUseSmtpAuth, SmtpUser, SmtpPassword, RequiresSSL, nTimeOutInSeconds, exceptionFunc);
        }

        #endregion

        #region Private Statics

        /// <summary>
        /// Send an Email * Doesn't Support Implicit SSL (port 465) - ONLY Explicit SSL on 25
        /// </summary>
        /// <param name="SmtpServer">specifiy a SMTP Server to use * Required *</param>
        /// <param name="SenderEmail">Email to use to send with * Required *</param>
        /// <param name="SenderDisplayName">Display name for Sender * Required *</param>
        /// <param name="ReceiverEmails">';' seperated list of emails to send to * Required *</param>
        /// <param name="MessageSubject">Email Message Subject * Required *</param>
        /// <param name="MessageText">Email Message Text * Required *</param>
        /// <param name="nPort">Smtp Port to use</param>
        /// <param name="ReceiverCCEmails">';' seperated list of emails to cc to</param>
        /// <param name="bUseSmtpAuth">true to use SmtpAuthentication, false otherwise</param>
        /// <param name="SmtpUser">Smtp Authentication Username</param>
        /// <param name="SmtpPassword">Smtp Authentication Password</param>
        /// <param name="RequiresSSL">true to use ssl, false otherwise</param>
        /// <param name="nTimeOutInSeconds">Defaults to 100 Seconds</param>
        /// <param name="exceptionFunc">If an exception occurs, this delegate gets called (can be null)</param>
        /// <returns>true if the Message Send Successfully, false otherwise</returns>
        private static bool SendEmailUsingNetMail(string SmtpServer, string SenderEmail, string SenderDisplayName, string ReceiverEmails, string MessageSubject, string MessageText, uint nPort, string ReceiverCCEmails, bool bUseSmtpAuth, string SmtpUser, string SmtpPassword, bool RequiresSSL, int nTimeOutInSeconds, DelegateCollection.Void_Param1_Exception_Func exceptionFunc)
        {
            try
            {
                if (String.IsNullOrEmpty(SmtpServer) || String.IsNullOrEmpty(SenderEmail) || String.IsNullOrEmpty(SenderDisplayName) || String.IsNullOrEmpty(ReceiverEmails) || String.IsNullOrEmpty(MessageSubject) || String.IsNullOrEmpty(MessageText))
                    return false;

                SmtpClient sMail = new SmtpClient(SmtpServer);
                sMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                if (bUseSmtpAuth && !String.IsNullOrEmpty(SmtpUser) && !String.IsNullOrEmpty(SmtpPassword))
                {
                    sMail.UseDefaultCredentials = false;
                    sMail.Credentials = new NetworkCredential(SmtpUser, SmtpPassword);
                }

                // Mail Message
                MailMessage msg = new MailMessage();
                msg.Subject = MessageSubject;
                // From
                msg.From = new MailAddress(SenderEmail, SenderDisplayName);
                // To
                foreach (string strToEmail in ReceiverEmails.Split(';'))
                    msg.To.Add(new MailAddress(strToEmail));
                // Body
                msg.Body = MessageText;
                // CC
                if (!String.IsNullOrEmpty(ReceiverCCEmails))
                {
                    foreach (string strToEmail in ReceiverCCEmails.Split(';'))
                        msg.CC.Add(new MailAddress(strToEmail));
                }

                // Send it
                if (nTimeOutInSeconds < 100)
                    nTimeOutInSeconds = 100;
                sMail.Timeout = (int)TimeSpan.FromSeconds(nTimeOutInSeconds).TotalMilliseconds;
                sMail.Port = ((nPort > 0) && (nPort < 65536)) ? (int)nPort : 25;
                sMail.EnableSsl = RequiresSSL;
                sMail.Send(msg);
                return true;
            }
            catch (Exception e) { if (exceptionFunc != null) exceptionFunc(e); }
            return false;
        }

        /// <summary>
        /// Send an Email * Does Support SSL (port 465) via Hacks - System.Web.Mail Space however is depreciated
        /// </summary>
        /// <seealso cref="http://www.aspcode.net/Send-mail-from-ASPNET-using-your-gmail-account.aspx"/>
        /// <param name="SmtpServer">specifiy a SMTP Server to use * Required *</param>
        /// <param name="SenderEmail">Email to use to send with * Required *</param>
        /// <param name="SenderDisplayName">Display name for Sender * Required *</param>
        /// <param name="ReceiverEmails">';' seperated list of emails to send to * Required *</param>
        /// <param name="MessageSubject">Email Message Subject * Required *</param>
        /// <param name="MessageText">Email Message Text * Required *</param>
        /// <param name="nPort">Smtp Port to use</param>
        /// <param name="ReceiverCCEmails">';' seperated list of emails to cc to</param>
        /// <param name="bUseSmtpAuth">true to use SmtpAuthentication, false otherwise</param>
        /// <param name="SmtpUser">Smtp Authentication Username</param>
        /// <param name="SmtpPassword">Smtp Authentication Password</param>
        /// <param name="RequiresSSL">true to use ssl, false otherwise</param>
        /// <param name="nTimeOutInSeconds">Defaults to 100 Seconds</param>
        /// <param name="exceptionFunc">If an exception occurs, this delegate gets called (can be null)</param>
        /// <returns>true if the Message Send Successfully, false otherwise</returns>
        private static bool SendEmailUsingWebMail(string SmtpServer, string SenderEmail, string SenderDisplayName, string ReceiverEmails, string MessageSubject, string MessageText, uint nPort, string ReceiverCCEmails, bool bUseSmtpAuth, string SmtpUser, string SmtpPassword, bool RequiresSSL, int nTimeOutInSeconds, DelegateCollection.Void_Param1_Exception_Func exceptionFunc)
        {
            try
            {
                if (String.IsNullOrEmpty(SmtpServer) || String.IsNullOrEmpty(SenderEmail) || String.IsNullOrEmpty(SenderDisplayName) || String.IsNullOrEmpty(ReceiverEmails) || String.IsNullOrEmpty(MessageSubject) || String.IsNullOrEmpty(MessageText))
                    return false;

                string[] Receivers = ReceiverEmails.Split(';');
                foreach (string Receiver in Receivers)
                {
                    webmail.MailMessage Mail = new System.Web.Mail.MailMessage();
                    Mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"] = SmtpServer;
                    Mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2;
                    Mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = nPort.ToString();
                    if (RequiresSSL)
                        Mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"] = "true";
                    if (bUseSmtpAuth)
                    {
                        Mail.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
                        Mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = SmtpUser;
                        Mail.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = SmtpPassword;
                    }

                    // Send Message
                    Mail.To = Receiver;
                    Mail.From = SenderEmail;
                    Mail.Subject = MessageSubject;
                    Mail.Body = MessageText;
                    webmail.SmtpMail.SmtpServer = SmtpServer;
                    webmail.SmtpMail.Send(Mail);
                }

                return true;
            }
            catch (Exception e) { if (exceptionFunc != null) exceptionFunc(e); }
            return false;
        }

        #endregion
    }
}
