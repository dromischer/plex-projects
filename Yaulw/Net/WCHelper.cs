﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Yaulw.Net
{
    /// <summary>
    /// WebClient Helper class to get commen stuff done
    /// </summary>
    public static class WCHelper
    {
        public const string USER_AGENT_IE8 = "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; GTB7.4; InfoPath.2; SV1; .Net CLR 3.3.69573; .NET CLR 1.0.3705; WOW64; rv:12.0; en-US) Gecko/20100101 Firefox/12.0";
        public const string USER_AGENT_GENERIC = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0";

        /// <summary>
        /// Download the specified URL file to the local path
        /// </summary>
        /// <param name="URLFileNameNPath"></param>
        /// <param name="LocalFileNameNPath"></param>
        /// <param name="bOverwriteExisting"></param>
        /// <returns></returns>
        public static bool DownloadFileFromURL(string URLFileNameNPath, string LocalFileNameNPath, bool bOverwriteExisting)
        {
            if(!String.IsNullOrEmpty(URLFileNameNPath) && !String.IsNullOrEmpty(LocalFileNameNPath))
            {
                try 
                {
                    using (WebClient fileReader = new WebClient())
                    {
                        //string filename = URLFileNameNPath.Substring(URLFileNameNPath.LastIndexOf("/"), URLFileNameNPath.Length);
                        if (!System.IO.File.Exists(LocalFileNameNPath) || bOverwriteExisting)
                            fileReader.DownloadFile(URLFileNameNPath, LocalFileNameNPath);
                    }
                    return true;
                } 
                catch (Exception) { /* ignore */ }
            }
            return false;
        }

        /// <summary>
        /// ScreenScrape the Text from the URL
        /// </summary>
        /// <param name="URL"></param>
        /// <returns></returns>
        public static string ScreenScrapeFromURL(string URL)
        {
            if (!String.IsNullOrEmpty(URL))
            {
                try 
                {
                    using (WebClient fileReader = new WebClient())
                    {
                        fileReader.Headers.Add("user-agent", USER_AGENT_GENERIC);
                        using (Stream data = fileReader.OpenRead(URL))
                        using (StreamReader sr = new StreamReader(data))
                        {
                            string str = sr.ReadToEnd();
                            return str;
                        }
                    }
                }
                catch (Exception) { /* ignore */ }
            }
            return "";
        }


    }
}
