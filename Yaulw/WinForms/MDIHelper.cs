﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Yaulw.WinForms
{
    /// <remarks>
    /// Helper functions for WinForms MDI Layout
    /// </remarks>
    public static class MDIHelper
    {
        /// <summary>
        /// Use this function to retrive the actual MDIClient Object from
        /// an MDIContainer Form
        /// </summary>
        /// <param name="mdiFormContainer">Window form that is a valid MDIContainter</param>
        /// <returns>Valid MdiClient object if found, null otherwise</returns>
        public static MdiClient GetMDIClientObj(Form mdiFormContainer)
        {
            if (mdiFormContainer.IsMdiContainer)
            {
                for (int i = 0; i < mdiFormContainer.Controls.Count; ++i)
                {
                    MdiClient mdiClient = mdiFormContainer.Controls[i] as MdiClient;
                    if (mdiClient != null)
                        return mdiClient;
                }
            }
            return null;
        }

        /// <summary>
        /// Closes all MDI Children for the specified MDI Form Container
        /// </summary>
        /// <param name="mdiFormContainer">Window form that is a valid MDIContainter</param>
        public static void CloseAllChildWindows(Form mdiFormContainer)
        {
            if (mdiFormContainer.IsMdiContainer)
            {
                if (mdiFormContainer.MdiChildren.Length > 0)
                {
                    Form[] form = mdiFormContainer.MdiChildren;
                    foreach (Form f in form)
                    {
                        f.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Changes the Child MDI Window Layout for the specified MDI Form Containter
        /// </summary>
        /// <param name="mdiFormContainter">Window form that is a valid MDIContainter</param>
        /// <param name="pLayout">Specify the MdiLaoyt to use</param>
        public static void HandleChildWindowsLayout(Form mdiFormContainer, MdiLayout pLayout)
        {
            if (!mdiFormContainer.IsMdiContainer)
                throw new InvalidOperationException("mdiFormContainter not and MDI Container");

            // We don't handle Vertical * we are done *
            if (pLayout == MdiLayout.TileVertical)
                return;

            // Arrange Icon works fine with MDI
            if (pLayout == MdiLayout.ArrangeIcons)
            {
                mdiFormContainer.LayoutMdi(pLayout);
                return;
            }

            ////
            // For Some Reason *YTBD* Cascade and Horizontal MDI Layout stopped working,
            // So now we just do the same logic manually and don't use the build in functions
            // provided by the MDI
            ////
            if (pLayout == MdiLayout.Cascade)
            {
                // Incr. Consts
                const int N_INCR_TOP = 30;
                const int N_INCR_LEFT = 18;

                Form ActiveMDI = mdiFormContainer.ActiveMdiChild;
                if (ActiveMDI != null)
                {
                    int nTop = -1 * N_INCR_TOP;
                    int nLeft = -1 * N_INCR_LEFT;

                    // By Default, Cascade windows increases the size of the Form
                    // to a percentage of available with on the screen. any screen size > 800
                    // width we'll use the calculated width
                    MdiClient client = GetMDIClientObj(mdiFormContainer);
                    int nCalculatedWidth = (client.Width / 100) * 80;
                    int nCalculatedHeight = (client.Height / 100) * 60;

                    foreach (Form child in mdiFormContainer.MdiChildren)
                    {
                        if (child != ActiveMDI)
                        {
                            // Incr. nTop N' nLeft
                            nTop = nTop + N_INCR_TOP;
                            nLeft = nLeft + N_INCR_LEFT;

                            // Position Childe
                            child.Top = nTop;
                            child.Left = nLeft;
                            child.Height = (nCalculatedHeight > child.MinimumSize.Height) ? nCalculatedHeight : child.MinimumSize.Height;
                            child.Width = (nCalculatedWidth > child.MinimumSize.Width) ? nCalculatedWidth : child.MinimumSize.Width;
                            child.Activate();
                        }
                    }

                    // Position Active MDI
                    ActiveMDI.Top = nTop + N_INCR_TOP;
                    ActiveMDI.Left = nLeft + N_INCR_LEFT;
                    ActiveMDI.Height = (nCalculatedHeight > ActiveMDI.MinimumSize.Height) ? nCalculatedHeight : ActiveMDI.MinimumSize.Height;
                    ActiveMDI.Width = (nCalculatedWidth > ActiveMDI.MinimumSize.Width) ? nCalculatedWidth : ActiveMDI.MinimumSize.Width;
                    ActiveMDI.Activate();
                }
            }
            else if (pLayout == MdiLayout.TileHorizontal)
            {
                Form ActiveMDI = mdiFormContainer.ActiveMdiChild;
                if (ActiveMDI != null && mdiFormContainer.MdiChildren.Length > 1)
                {
                    MdiClient client = GetMDIClientObj(mdiFormContainer);
                    int nMaxHeight = client.Height;
                    int nMaxWidth = client.Width;

                    // MDI Window Counts
                    int nCount = mdiFormContainer.MdiChildren.Length;
                    int nHalf = (nCount / 2);

                    // Position the first half of the Windows
                    int nTop = 0;
                    int nCalculatedHeight = nMaxHeight / nHalf;
                    int nCalculatedWidth = (nMaxWidth / 2);
                    for (int i = 0; i < nHalf; ++i)
                    {
                        Form child = mdiFormContainer.MdiChildren[i];

                        // Title the Window
                        child.Top = nTop;
                        child.Left = 0;

                        // Always set to Mins
                        child.Height = (child.MinimumSize.Height < nCalculatedHeight) ? nCalculatedHeight : child.MinimumSize.Height;
                        child.Width = (child.MinimumSize.Width < nCalculatedWidth) ? nCalculatedWidth : child.MinimumSize.Width;

                        // incr. nTop
                        nTop = nTop + nCalculatedHeight;
                    }

                    // Position the remaining half of the Windows
                    nTop = 0;
                    nCalculatedHeight = (nMaxHeight / (nCount - nHalf));
                    for (int i = nHalf; i < nCount; ++i)
                    {
                        Form child = mdiFormContainer.MdiChildren[i];

                        // Title the Window
                        child.Top = nTop;
                        child.Left = nCalculatedWidth;

                        // Always set to Mins
                        child.Height = (child.MinimumSize.Height < nCalculatedHeight) ? nCalculatedHeight : child.MinimumSize.Height;
                        child.Width = (child.MinimumSize.Width < (nCalculatedWidth - 20)) ? (nCalculatedWidth - 20) : child.MinimumSize.Width;

                        // incr. nTop
                        nTop = nTop + nCalculatedHeight;
                    }
                }
                else if (ActiveMDI != null && mdiFormContainer.MdiChildren.Length == 1)
                {
                    ActiveMDI.Top = 0;
                    ActiveMDI.Left = 0;
                    ActiveMDI.Height = ActiveMDI.MinimumSize.Height;
                    ActiveMDI.Width = ActiveMDI.MinimumSize.Width;
                }
            }

        }

    }
}
