﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yaulw.WPF;

namespace KeeCrypt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class HiddenMainWindow : Window
    {
        private KeepHidden _HiddenWindow = null;

        public HiddenMainWindow()
        {
            // * Force this window to remain hidden indefinitly*
            _HiddenWindow = new KeepHidden(this);
            
            InitializeComponent();
        }
    }
}
