﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyTiddlyWikis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string _publicTiddlyPath = "";
        string _privateTiddlyPath = "";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FileNameNPath"></param>
        /// <returns></returns>
        private StringBuilder ReadFileToSB(string FileNameNPath)
        {
            StringBuilder sb = new StringBuilder("");
            if (!File.Exists(FileNameNPath))
                return sb;

            try
            {
                using (var fs = new FileStream(FileNameNPath, FileMode.Open))
                using (var sr = new StreamReader(fs))
                {
                    return new StringBuilder(sr.ReadToEnd());
                }
            }
            catch (Exception) { /* ignore */ }
            return sb;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            //StringBuilder sb = ReadFileToSB(@"E:\Reinvent99\SkyDrive\Documents\TiddlyPublic\LatestTech\index.html");
            //sb.Insert(0, "<!-- saved from url=(0014)about:internet -->\n\r");            
            //webBrowser.Navigate("http://yahoo.com/");
            //webBrowser.NavigateToString(sb.ToString());

            // Get SkyDrive Path
            string dirSkyDriveDocs = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\SkyDrive\\Documents";
            if(!Directory.Exists(dirSkyDriveDocs))
            {
                MessageBox.Show("SkyDrive Documenents directory not in user profile - Application will exit");
                Application.Current.Shutdown();
            }

            // Get Tiddly Paths
            _publicTiddlyPath = dirSkyDriveDocs + "\\TiddlyPublic";
            _privateTiddlyPath = dirSkyDriveDocs + "\\TiddlyPersonal";
            if (!Directory.Exists(_publicTiddlyPath) || !Directory.Exists(_privateTiddlyPath))
            {
                MessageBox.Show("Either TiddlyPublic or TiddlyPrivate doesn't exist - Application will exit");
                Application.Current.Shutdown();
            }

            // Load the Tiddlies
            LoadPublicAndPrivateTiddlies();

            //webBrowser
            //webBrowser.Navigate("file:///E:/Reinvent99/SkyDrive/Documents/TiddlyPublic/LatestTech/index.html");
            //webBrowser.Navigate("file://127.0.0.1/e$/Reinvent99/SkyDrive/Documents/TiddlyPublic/LatestTech/index.html");
        }

        /// <summary>
        /// Load and Public and Private Tiddlies
        /// </summary>
        void LoadPublicAndPrivateTiddlies()
        {
            // Get Public Tiddlies
            string[] dirs = Directory.GetDirectories(_publicTiddlyPath);
            List<ListBoxItem> public_tiddlies = new List<ListBoxItem>();            
            foreach (string d in dirs)
            {
                if (RetrieveLastDirectoryNameInPath(d) != "_Empty")
                    public_tiddlies.Add(new ListBoxItem() { Content = RetrieveLastDirectoryNameInPath(d) });
            }
            publicTiddlies.ItemsSource = public_tiddlies;
            publicTiddlies.SelectionChanged += Tiddlies_SelectionChanged;
                      
            // Get Private Tiddlies
            dirs = Directory.GetDirectories(_privateTiddlyPath);
            List<ListBoxItem> private_tiddlies = new List<ListBoxItem>();
            foreach (string d in dirs)
            {
                if (RetrieveLastDirectoryNameInPath(d )!= "_Empty")
                    private_tiddlies.Add(new ListBoxItem() { Content = RetrieveLastDirectoryNameInPath(d) });
            }
            privateTiddlies.SelectionChanged += Tiddlies_SelectionChanged;
            privateTiddlies.ItemsSource = private_tiddlies;
        }

        void Tiddlies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string pathToUse = "";
            ComboBox cb = (ComboBox)sender;
            if (cb.Name == "publicTiddlies")
                pathToUse = _publicTiddlyPath;
            else
                pathToUse = _privateTiddlyPath;

            ListBoxItem item = (ListBoxItem)cb.SelectedItem;
            if (item != null)
            {
                string dirName = item.Content.ToString();
                // Let's try to navigate 
                string Index = pathToUse + "\\" + dirName + "\\" + "index.html";
                if (File.Exists(Index))
                {
                    Index = Index.Replace("\\", "/");
                    //webBrowser.Navigate("file:///E:/Reinvent99/SkyDrive/Documents/TiddlyPublic/LatestTech/index.html");
                    webBrowser.Navigate("file:///" + Index);
                }
                else
                {
                    MessageBox.Show(Index + " doesn't exist! ~shouldn't happen");
                }
            }       
        }

        private void reloadTiddlies_Click_1(object sender, RoutedEventArgs e)
        {
            LoadPublicAndPrivateTiddlies();
        }

        /// <summary>
        /// Retrieve the last directory name from a path
        /// </summary>
        /// <param name="DirectoryPath"></param>
        /// <returns>the name of the last directory in the path</returns>
        public static string RetrieveLastDirectoryNameInPath(string DirectoryPath)
        {
            if (!String.IsNullOrEmpty(DirectoryPath))
            {
                // Find the last folder and return that information
                // This is what this function was created for                
                string dp = DirectoryPath;
                int nLast = dp.LastIndexOf('\\');
                if (nLast != -1 && dp.Length > 3)
                {
                    dp = dp.Substring(nLast + 1);
                    return dp;
                }
            }
            return String.Empty;
        }
    }
}
